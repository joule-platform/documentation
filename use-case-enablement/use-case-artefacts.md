---
hidden: true
---

# Use case artefacts



this page will describe how joules architecture pieces are and the organisation of the files on where to place the use cases file when creating a new one



## Deployment artefact

Now we bring together each deployment artefact (source, use case and sinks) to form the desired use case. A use case is formed by a single app.env file which references these files. This method of deployment enables you to simply switch out the source and sinks based upon your needs i.e. development, testing and production deployments

<figure><img src="../.gitbook/assets/uc-deployment.png" alt=""><figcaption><p>use case file brings each descriptor file to form a single unit of deployment</p></figcaption></figure>

### Binding file used to run a use case

```yaml
use case:
  name: basic_twindow_analytics
  constraints:
    valid from: "2024-01-01T08:00:00.000Z"
    valid to: "2030-01-01T23:59:00.000Z" 
  sources:
  - nasdaq_quotes_stream
  stream name: basic_tumbling_window_pipeline
  sinks:
  - kafka_analytics_view
```

