---
description: Build impactful use cases
---

# Use case building framework

## **What will we learn in this article?**

In this guide, we will learn how to **create effective use cases** that bridge the gap between user needs and system functionality. We will walk through the steps of building a use case, provide a real-world example and offer a simple user story template to help you get started.

By the end, we will have the tools and confidence to create use cases that drive real business impact.

With this use case in our hands, we will be focused when transferring it to the Joule DSL and have a **successful project delivery**.

## **What steps do we take for an impactful use case?**

### **Understand the system**

To build a strong use case, we start by defining the system we are working with. Whether it’s software, a product or a service:

1. What does it do for our users?
2. What goals does it help to achieve?
3. What are the key features and functions?

{% hint style="info" %}
Represent everything visually using a graphing tool, on paper or a board
{% endhint %}

### **Identify the actors**

Actors are anyone or anything interacting with our system. This can include:

1. External stakeholders such as end users (i.e., customers, employees).
2. Secondary systems or tools with dependencies to the target improvement area.
3. Internal stakeholders, your colleagues.

We should clearly define their behaviours and roles in the process.

### **Define actor goals**

Next, we focus on what the actors want to achieve. This step shifts the perspective from what the system can do to why users need it. Listing each goal ensures we understand the breadth of user interactions and outcomes.

### **Map the scenario**

Now, we outline the step-by-step flow of interaction between the actor and the system:

1. <mark style="color:green;">**Basic flow**</mark>\
   What happens when everything works as intended.
2. <mark style="color:green;">**Alternate flows**</mark>\
   What happens if there are errors, misuse or exceptions.

These flows give us a clear picture of how the system operates in real-world situations.

### **Validate and refine**

We compile all the flows to create a full use case. If we’re working with complex systems, we focus on the most common interactions or critical scenarios. Testing these scenarios can help us refine the use case and improve its usability.

## **Example: Online appointment scheduling**

### **Actors**

1. Customer.
2. Scheduling System.

### **Basic flow**

1. The customer selects a service and checks availability.
2. The system displays available slots.
3. The customer chooses a time and provides contact details.
4. The system confirms the booking and sends a notification.

### **Alternate flow**

If the customer enters incomplete contact information, the system prompts them to complete the form before proceeding.

### **Outcome**

The customer successfully schedules an appointment or receives actionable guidance to correct errors.

<figure><img src="../.gitbook/assets/image (1) (2) (1).png" alt=""><figcaption><p>Use case diagram</p></figcaption></figure>

## **Template: User story**

We can use this simple structure to write user stories as part of our use case to add more context and tell the story:

```
“As a [persona], I [want to], [so that].”
```

### **Example Stories**

1. As a customer, I want to view available appointment times, so that I can book a slot that fits my schedule.
2. As a system admin, I want to track booking patterns, so that I can optimize resource allocation.

## Additional resources

1. [Use case framework](https://www.fractalworks.io/post/evaluating-and-developing-data-analytics-capabilities#viewer-9ovgc).
2. [Double diamond ](https://www.designcouncil.org.uk/our-resources/the-double-diamond/)design process.
3. Visualisation of [use cases using the C4 model](https://c4model.com/).
