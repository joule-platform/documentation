---
icon: square-info
description: >-
  Joule is a low-code use case platform designed for modern streaming analytical
  processing
layout:
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: false
---

# What is Joule?

## Overview

Joule is a low-code platform for rapid use case development, featuring out-of-the-box templated implementations for **data sources and core processors**. Its declarative language simplifies defining processing pipelines and integration with data platforms, letting you **focus on building solutions** instead of rebuilding assets.

This is achieved by Joules declarative language that provides users with the **ability to define** processing pipelines and integrating to data platforms, along with utilising **reusable** of prebuilt and custom processors, metrics, analytics and data integrations.

Joule is meant to be small, module and powerful the moment it is deployed.&#x20;

### The Joule platform ecosystem

<figure><img src="../.gitbook/assets/joule_graph_transparent.png" alt=""><figcaption><p>Joule works alongside your existing data and visualisation platforms so <br>you can focus delivering value add use cases.</p></figcaption></figure>

## Learn more about the Joule platform

{% content-ref url="key-features.md" %}
[key-features.md](key-features.md)
{% endcontent-ref %}

{% content-ref url="joules-architecture.md" %}
[joules-architecture.md](joules-architecture.md)
{% endcontent-ref %}
