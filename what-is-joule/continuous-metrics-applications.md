---
icon: tally-4
description: Real-time metrics provide the ability to drive advance insights and use cases
---

# Continuous metrics

## What will we learn on this article?

This article explains **continuous metrics** and how they enhance advanced [use cases](use-case-enablement.md) by integrating live metrics within stream processing.

You will learn how Joule uses DuckDB to generate, store and manage these metrics, with examples like business dashboards and dynamic customer promotions.

## What are continuous metrics?

Combining live metrics within stream processing context provides a powerful tool **to build advance use cases**.

Functions such as:

* ML model predictions
* Analytic calculations
* Complex rule alerting logic

[Filtering](../components/processors/filters/) can leverage calculated metrics within the stream processing context.

Continuous metrics are generated **on a scheduled cycle** using captured streaming events. These are stored in-memory for any stream process to use.

{% hint style="info" %}
Stream metrics should not be confused with streaming analytics.\
Streaming analytics will perform insight creation, pattern detection, generate next best action or alert based upon a condition.
{% endhint %}

## How does Joule provide this feature?

Joule embeds an in-memory high performance analytics database, [DuckDB](https://duckdb.org/), to provide the core analytical function.

The necessary supporting capabilities are provided with the following additional functions:

* Scheduled metric generation
* Table management
* Metric priming and export
* Data access API's
* Event capture

## Use case examples

The following use cases apply metrics in slightly different methods.

1. **Business dashboards** calculates and distributes whereas
2. **Dynamic customer promotions** adjusts customer purchase promotions based upon general buyer activity and live market conditions.&#x20;

### **Business dashboards**

Understanding intraday business activity is essential for various functions in today's competitive market.

By leveraging analytical insights and live metrics, businesses can **shift from being reactive to applying proactive measures focused** on the next best action.

[Metrics](../components/processors/enrichment/metrics.md) play a crucial role in making this shift possible.

### **Dynamic customer promotions**

Dynamically adjusting promotions **based on market conditions**, inventory levels and customer buying behaviour can significantly impact product pricing.

By combining [in-motion metrics](../components/processors/enrichment/dynamic-contextual-data/) with customer purchase history and intent, tailored promotions can be offered to customers.

Real-time metrics create a consistent approach for these scenarios by collecting and calculating key variables within a specific time frame.
