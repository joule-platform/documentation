---
description: Learn what use cases can be built and deployed using Joule
icon: unity
---

# Use case enablement

## What will we learn on this article?

This article highlights what use cases mean and which can be supported through Joule.

Let's first get on one page together what a use case is:

> A use case is a concept in software development and product design that outlines how users interact with a system to achieve specific goals.\
> It details success and failure scenarios, as well as important variations or exceptions.\
> Use cases can be documented in writing or illustrated in a diagram or graph using a use case modeling tool.

We will showcase some example business applications that can be accomplished through the use of Joule such as:

1. Customer consent management
2. Geospatial intelligence
3. Proactive inventory management
4. and analytics-driven use cases:
   1. Real-time metrics
   2. Predictive analytics

We will also examine technical use cases such as event stream deduplication, the time-consuming task of data wrangling and data orchestration.

{% hint style="info" %}
Drive real business impact and learn how to build a use case from scratch with your stakeholders, [follow this guide](../use-case-enablement/use-case-building-framework.md)
{% endhint %}

## What are use cases in Joule?

<mark style="color:green;">**In short**</mark>

Joule provides out-of-the-box, ready-to-use code that enables quick deployment and immediate business impact.

<mark style="color:green;">**In detail**</mark>

Joule empowers business developers to create both business and technical use cases through pre-built and custom data integrations, event processors and analytics.

Its flexibility supports a variety of use cases, including data encryption, real-time metrics, custom analytics and machine learning predictions.

Now, let's explore some potential use cases that can be build with Joule.

## Use case examples&#x20;

We will look at 3 different spaces of use cases:

1. Business use cases
2. Analytical use cases
3. Data orchestration use cases

### Business use cases

This category of use cases focuses on applying customer-facing insights, generating alerts and determining the next best actions.

1. <mark style="color:green;">**Customer consent management**</mark>\
   Apply static or dynamic opt-In / opt-out lists to drive [event filtering](../components/processors/filters/).
2. <mark style="color:green;">**Geospatial intelligence**</mark>\
   Develop geospatial applications that track entities in specific locations, like shopping malls or stadiums, over time.
3. <mark style="color:green;">**Proactive inventory management**</mark>\
   Capture real-time inventory levels to manage and forecast stock by location, helping to optimise inventory and boost sales revenue.
4. <mark style="color:green;">**Sensitive customer data management**</mark>\
   [Remove or mask sensitive customer information](../components/processors/transformation/) (i.e. credit card number) before distributing to consuming systems.

### Analytics driven use cases

The extensive use of analytics in use cases is increasingly essential for differentiating customer-facing offerings.

1. <mark style="color:green;">**Analytic driven alerting**</mark>\
   Combine [real-time metrics](../components/analytics/analytic-tools/) and live events to proactively alert when a system is nearing failure in a manufacturing environment.
2. <mark style="color:green;">**Stream based predictive analytics**</mark>\
   Leverage [machine learning models](../components/analytics/ml-inferencing/) within a stream to predict in real-time.\
   i.e. Likelihood to convert to being a valued customer.
3. <mark style="color:green;">**Real-time metrics**</mark>\
   Define and monitor custom metrics to drive proactive customer support and drive higher brand satisfaction and stickiness.\
   i.e. Improve customer experience for mobile phone usage
4. <mark style="color:green;">**Contextual analytics**</mark>\
   Join events with slow moving customer profile data to determine next best action.\
   i.e. Customer website dynamic promotions.

### Data orchestration use cases

This category of use cases play primarily a supportive role to the above mentioned use cases and therefore considered more technical in nature.

1. <mark style="color:green;">**Once-only processing (stream deduplication)**</mark>\
   Process events **just once** through event deduplication.\
   i.e. Improve customer event processing by focusing only on critical events and reducing the risk of errors.
2. <mark style="color:green;">**Data wrangling**</mark>\
   Prepare data for subsequent pipeline processing by transforming event attributes into a desired format.\
   i.e. Machine learning predictions may require input data to be engineered into suitable target variables using techniques like normalisation.
