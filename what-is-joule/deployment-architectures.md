---
hidden: true
---

# Deployment architectures



within hybrid environments and the Internet of Things



Deployment architectures

* Single unit of processing
* Horizontal multiple unit of processing &#x20;
* Hierarchical&#x20;
* Bare metal deployment



## IoT deployment

IoT (Internet of Things) deployment models can be considered as a catch up architecture for remote single node deployments that interact with a central master node/s.&#x20;



\[DIAGRAM]



Remote deployment targets can be considered as&#x20;

* Raspberry Pi
* Client machine
* Server
* WASM deployment ?







## Scaled deployment



## DUMPING GROUNF FOR TEXT

Joule is a Low Code Stream Analytics Platform designed to fast track use case development. Straight out of the box, Joule offers standard implementations for data sources and sinks, along with essential core processors, allowing you to kickstart your use case development journey with ease.

## Low Code DSL

Joule DSL provides an easy method to build use cases with reusable assets while delivering business impact at pace. The DSL is extendable using standard Json annotation when building custom components using the Joule SDK

```yaml
pipeline:
  - filter:
      expression: "symbol == 'MSFT'"
  - time window:
      emitting type: tumblingQuoteAnalytics
      aggregate functions:
        MIN: [ask, bid]
        MAX: [ask, bid]
      policy:
        type: tumblingTime
        window size: 5000
```





