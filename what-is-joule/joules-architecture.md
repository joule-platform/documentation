---
icon: cubes
description: High level overview of technologies used within the Joule platform
---

# The tech stack

## Overview

Joule's architecture principle is to always keep things simple but powerful.

The SDK provided, aids developers to customise and leverage key mature technologies that are able to support a wide array of use cases.

## Development principles

When developing Joule, the desire was to keep the solution as small as possible to enable deployment on to any JVM supported hardware. This meant smaller independent established frameworks needed to be considered to provide the key features Joule needed.

Therefore, frameworks such as Spring were not consider even though the main developer has many years development experience using the Spring ecosystem. The result is, the use case deployment artifact size is reduced and can be managed to the components needed.

Selection consideration used:

1. Has a strong community.
2. Modern and proven in the wild.
3. Lightweight with minimal dependencies.
4. Best of breed within category.
5. Standards based to enable solution switch if needed.

### Component based architecture

Joule follows a component driven architecture approach to bring new features to the platform incrementally while leveraging existing assets.

<figure><img src="../.gitbook/assets/usecase-enablement (1).png" alt=""><figcaption><p>Joule works alongside your existing data and visualisation platforms so <br>you can focus delivering value add use cases.</p></figcaption></figure>

## GraalVM

<div align="left"><figure><img src="../.gitbook/assets/graalvm.png" alt="" width="197"><figcaption></figcaption></figure></div>

[GraalVM](https://www.graalvm.org/) was primarily chosen for its ability to support Python and Javascript within the Java processing environment.

However, further benefits such as fast process startup time, lean runtime and low resource usage has made this a no-regrets decision.&#x20;

Key features used:

* Compile and Runtime JRE
* Dynamic scripting support for Python, Javascript and Node.js

## In-memory analytics database

<div align="left"><figure><img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fduckdb.org%2Fimages%2Flogo-dl%2FDuckDB_Logo.png&#x26;f=1&#x26;nofb=1&#x26;ipt=4aa584899795e971cd93502474a84ca24211ab3fd01a832f02297dfc6c646159&#x26;ipo=images" alt="" width="188"><figcaption></figcaption></figure></div>

Joule uses [DuckDB](https://duckdb.org/) as an internal analytics database solution.

This is a lightweight, open-source, portable and performant modern database solution that is gaining significant traction since v1.0.0.

Key features used:

* Event capture and storage
* Metrics engine
* Enrichment processing
* Data export
* Low-latency data access

## Rest API

Open access to a Joule nodes is through Rest APIs.

The [Javalin](https://javalin.io/) web framework library is used due to its lightweight, simple and flexible approach to development.

Key features used:

* Jetty Http server
* OpenAPI and Swagger Docs
* Web sockets for event ingestion and distribution
* SSL support

## Connectors

All developed connector integrations could have easily been provided using the Sprint Integration framework at an overhead cost which was not acceptable. This has meant a focused approach on the key OOTB solutions provided.

All connector integrations are developed against client libraries and updated every six months.&#x20;
