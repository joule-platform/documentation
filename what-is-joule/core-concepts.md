---
icon: line-height
description: >-
  This section explores high-level concepts about Joule, it will help us
  understand how to build stream processing pipelines that subscribe to source
  data and publish results
---

# Core concepts

## What will we learn on this article?

This article introduces high-level concepts about Joule, providing a foundational understanding of key topics and how they connect.

You will learn how Joule enables the creation of stream processing pipelines that subscribe to source data and publish results.

This article covers:

* <mark style="color:green;">**Joule core concepts**</mark>\
  An overview of essential concepts necessary for using Joule effectively.
* <mark style="color:green;">**Event sources**</mark>\
  Discover how data sources supply Joule with the information needed to execute use cases, including implementations like Kafka, RabbitMQ and others.
* <mark style="color:green;">**Stream processors**</mark>\
  Understand the role of processors in Joule, which perform unique functions that, when combined, create business use cases. Examples include enrichers, transformations and online predictions.
* <mark style="color:green;">**Data sinks**</mark>\
  Learn how data sinks allow Joule to connect with downstream systems, adding value to business cases through implementations like SQL databases and InfluxDB.
* <mark style="color:green;">**Joule Domain Specific Language (DSL)**</mark>\
  Explore how Joule's DSL facilitates defining use cases with human-readable YAML syntax.
* <mark style="color:green;">**Internal event representation**</mark>\
  Understand the key internal event structure to develop use cases and extend the platform.&#x20;

By the end of this article, you will have a comprehensive understanding of Joule's capabilities and components.

{% hint style="info" %}
Learn more about the [core data types of Joule on this pages](data-types/)
{% endhint %}

## Core Joule architecture

<figure><img src="../.gitbook/assets/concepts.png" alt=""><figcaption><p>Core concept architecture</p></figcaption></figure>

{% hint style="info" %}
Stream & batch processed data can be combined with Joule.
{% endhint %}

## Event sources

Data sources provide Joule with the required data to execute use cases.

Fractalworks has provided a number of ready to use implementations to enable you to start building use cases.

{% content-ref url="../components/connectors/sources/" %}
[sources](../components/connectors/sources/)
{% endcontent-ref %}

## Stream processors

Processors form the core of the Joule platform.

A processor performs a distinct unique function when chained with others form a business use case.

{% content-ref url="../components/processors/" %}
[processors](../components/processors/)
{% endcontent-ref %}

## Data sinks

Data sinks enable Joule to integrate to downstream systems which provide further value to additional business cases

{% content-ref url="../components/connectors/sinks/" %}
[sinks](../components/connectors/sinks/)
{% endcontent-ref %}

## Joule Domain Specific Language

The Joule platform provides a Domain Specific Language (DSL) to define use cases using human-readable yaml syntax.&#x20;

{% content-ref url="../concepts/low-code-development.md" %}
[low-code-development.md](../concepts/low-code-development.md)
{% endcontent-ref %}

## Internal event representation

When data is processed within Joule external data is converted in to an internal format before being presented to consumers in to a suitable format.

{% content-ref url="data-types/streamevent.md" %}
[streamevent.md](data-types/streamevent.md)
{% endcontent-ref %}
