---
description: Geospatial data structure for location-based spatial entity analysis
---

# GeoNode

## Learn what it is

{% content-ref url="../../concepts/key-joule-data-types/geonode.md" %}
[geonode.md](../../concepts/key-joule-data-types/geonode.md)
{% endcontent-ref %}

## **Package location**

```java
com.fractalworks.streams.processors.geospatial.structures
```

## Class Definition

```java
class GeoNode<T extends ReferenceData> implements Coordinates, ReferenceData
```

### Constructors

```java
// Default constructor
public GeoNode()

// Create a node with location details and add entity later
public GeoNode(double latitude, double longitude)

// Create a node with location details and a reference data type
public GeoNode(double latitude, double longitude, T entity)
```

## Key Methods

### Get coordinates

```java
Tuple<Double, Double> getCoordinates();
```

### Get entity unique key

```java
Object getKey() 
```

### Get entity&#x20;

```java
T getValue()
```

## Example

The example demonstrates using the `GeoNode` class to store and query geospatial data.

This example demonstrates how to associate spatial data with entities and efficiently query nearby locations.

1. <mark style="color:green;">**Friend class**</mark>\
   A `Friend` object stores details (ID, name) of a person.
2. <mark style="color:green;">**GeoNode instances**</mark>\
   Each `GeoNode<Friend>` links a `Friend` to specific geographical coordinates (latitude, longitude).
3. <mark style="color:green;">**QuadTree**</mark>\
   The `GeoNode` instances are added to a `QuadTree`, which indexes the locations for efficient querying.
4. <mark style="color:green;">**GeoFence**</mark>\
   A `GeoFence` is created around a specific location with a radius (2 units).
5. <mark style="color:green;">**Querying**</mark>\
   The `QuadTree` is queried for `GeoNode` objects within the `GeoFence`, returning nearby friends.
6. <mark style="color:green;">**Display results**</mark>\
   The nearby friends are printed to the console.

```java
class Friend implements ReferenceData {
        Integer id;
        String name;
    // Implementation
}

Friend bart = new Friend(1, "bart");
Friend billy = new Friend(2, "billy");
Friend cumin = new Friend(3, "cumin");
Friend john = new Friend(4, "john");
Friend stacey = new Friend(5, "stacey");
Friend bambi = new Friend(6, "bambi");
Friend gazza = new Friend(7, "gazza");
Friend kevin = new Friend(8, "kevin");
Friend lora = new Friend(9, "lora");
Friend rodney = new Friend(10, "rodney");
Friend arkan = new Friend(11, "arkan");
    
GeoNode<Friend>[] friendlocations = new GeoNode[]{
    new GeoNode<>(51.456821f, -0.164746f, bart),
    new GeoNode<>(51.456821f, -0.164746f, billy),
    new GeoNode<>(51.457018f, -0.1622072f, cumin),
    new GeoNode<>(51.457018f, -0.1622072f, john),
    new GeoNode<>(51.467766f, -0.2987533f, stacey),
    new GeoNode<>(51.467766f, -0.2987533f, bambi),
    new GeoNode<>(51.467766f, -0.2987533f, gazza),
    new GeoNode<>(51.457018f, -0.1622072f, kevin),
    new GeoNode<>(51.457018f, -0.1622072f, lora),
    new GeoNode<>(51.467766f, -0.2987533f, rodney),
    new GeoNode<>(51.456821f, -0.164746f, arkan)
};

// Add friend locations to search tree
QuadTree<GeoNode<Friend>> friendsTree = new QuadTree<>(-180f, -90f, 360, 64800);
Arrays.stream(peoplePoints).forEach(friendsTree::insert);

// Perform a search of entites within a geofence
GeoFence currentLocation = new GeoFence(51.456821f, -0.164746f, 2);
Collection<GeoNode<Friend>> friendsNearby = friendsTree.query(currentLocation);
friendsNearby.forEach(System.out::println);
```
