---
description: ReferenceDataObject stores, queries, and manages contextual reference data
---

# ReferenceDataObject

## Learn what it is

{% content-ref url="../../concepts/key-joule-data-types/referencedataobject.md" %}
[referencedataobject.md](../../concepts/key-joule-data-types/referencedataobject.md)
{% endcontent-ref %}

## **Package location**

```java
com.fractalworks.streams.sdk.referencedata
```

## DSL attributes

<table><thead><tr><th width="135">Attribute</th><th width="428">Description</th><th>Type</th></tr></thead><tbody><tr><td>key</td><td>Unique reference data key used when performing query lookups</td><td>Object</td></tr><tr><td>data</td><td>Custom data to be stored outside of the Map structure</td><td>Object</td></tr></tbody></table>

## Key methods

### Key

These methods allow you to set and retrieve a unique lookup key for the contextual data. If a `null` value is passed, it will be converted to a String `null`.

```java
// Set lookup key
public void setKey(Object key);

// Get lookup key
public Object getKey();
```

### Data

These methods enable you to associate and retrieve custom data with the contextual data object. Again, if a `null` value is passed, it is converted to a String `null`.

```java
// Set custom data object
public void setData(Object data);

// Get the custom data object
public Object getData();
```
