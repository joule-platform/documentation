---
description: Key data types available within the Joule platform
---

# Data types

## Learn more about the key data types

{% content-ref url="../../concepts/key-joule-data-types/" %}
[key-joule-data-types](../../concepts/key-joule-data-types/)
{% endcontent-ref %}

## Available data types

<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>StreamEvent</strong></mark></td><td>StreamEvent enables flexible, efficient event-driven data processing in Joule</td><td></td><td><a href="streamevent.md">streamevent.md</a></td></tr><tr><td><mark style="color:orange;"><strong>ReferenceDataObject</strong></mark></td><td>ReferenceDataObject stores, queries, and manages contextual reference data</td><td></td><td><a href="referencedataobject.md">referencedataobject.md</a></td></tr><tr><td><mark style="color:orange;"><strong>GeoNode</strong></mark></td><td>Geospatial data structure for location-based spatial entity analysis</td><td></td><td><a href="geonode.md">geonode.md</a></td></tr></tbody></table>
