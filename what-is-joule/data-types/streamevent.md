---
description: StreamEvent enables flexible, efficient event-driven data processing in Joule
---

# StreamEvent

## Learn what it is

{% content-ref url="../../concepts/key-joule-data-types/streamevent-object.md" %}
[streamevent-object.md](../../concepts/key-joule-data-types/streamevent-object.md)
{% endcontent-ref %}

## **Package location**

`com.fractalworks.streams.core.data.streams`.

## DSL attributes

<table><thead><tr><th width="148">Attribute</th><th width="410.3333333333333">Description</th><th>Type</th></tr></thead><tbody><tr><td>uuid</td><td>Unique identifier assigned by the platform to enable tracking</td><td>UUID</td></tr><tr><td>type</td><td>Parent type of the event e.g. <code>market_data</code></td><td>String</td></tr><tr><td>subType</td><td>Finer grain type of the event e.g. <code>aim</code> </td><td>Object</td></tr><tr><td>eventTme</td><td>Actual time the event occurred within the source system. This is set by the user</td><td>Long</td></tr><tr><td>IngestTime</td><td>Actual time the event was ingested by Joule process. This is automatically assigned at point of object creation</td><td>Long</td></tr><tr><td>dictionary</td><td>Store of all key event attributes</td><td>Map&#x3C;String,Object></td></tr><tr><td>callChain</td><td>Tracking store of every change made to the event as it passes through the processing pipeline. This is managed by the Joule platform</td><td>Map&#x3C;Tuple&#x3C;UUID, String>, Long></td></tr></tbody></table>

## Key methods

### Adding

Two methods are provided to add new attributes to an event. If a `null` value is passed, it will be converted to a String `null` value.

```java
// Add field and value using the default UUID
public void addValue(String field, Object value);

// Add field and value using the passed UUID
public void addValue(UUID srcUUID, String field, Object value);
```

### Updating

Attributes can be updated in place. If a `null` value is passed, it will be converted to a String `null` value.

```java
// Update field and value using the passed UUID
public void replaceValue(UUID srcUUID, String field, Object value);
```

### Get value

Retrieve either a single attribute or the entire set of attribute values.

```java
// Get value for field
public Object getValue(String field);

// Get a shallow copy of the field value dictionary
public Map<String, Object> getDictionary();
```

### Remove

Two methods are available to remove fields from the event.

```java
// Remove a single field from the event
public void removeValue(UUID srcUUID, String field);

// Remove all stored field data
public void removeAll();
```

### Cloning

A deep copy method is provided to create a new `StreamEvent` from the current one, preserving data isolation.

```java
// Get a deep copy of this eventfav
public StreamEvent deepCopy();
```

### Get call chain

Retrieve the history of changes applied to the event, providing insight into its modification history.

```java
// Get the history of changes applied to the event
public Map<Triple<UUID, String,Object>, Long> getCallChain();
```
