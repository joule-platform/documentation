---
icon: scribble
---

# Public Roadmap

## 2024 Roadmap and vision

Improve user experience for analytical and enrichment features. Analytic processes will be able to take advantage of static and pre-computed metric data for online ML predictions and insights

### Ambition

To deliver a conversation driven use case definition UI that reduces the burden on users to define use cases. Standard pre-configured data connectors and processors will be leveraged to drive the process.\


<figure><img src="../.gitbook/assets/Joule - roadmap.jpg" alt=""><figcaption><p>Subject to change</p></figcaption></figure>
