# Running Joule on CLI

***

Joule ships with a Command Line Interface (CLI) for running your Joule project. To use the CLI your workflow generally looks like this:

1. Define your use case using the Joule DSL
2. Develop custom components within an IDE and test them locally, if required
3. Define the required data sources and sinks using Joule DSL
4. **Run** your use case from the command line

#### Example

```
java -cp "./lib/*" com.fractalworks.streams.joule.JouleCLI \
         -s eventSource.yaml \
         -e usecase.yaml \
         -p publishSinks.yaml
```

### Command line arguments

The CLI has a few command line arguments to run the Joule process.

_Required_

* `-s or --source` for data source file
* `-u or --usecase` for stream processing use case file
* `-p or --publish` for event sinks file

_Optional_

* `-c or --config` for custom Joule process configuration file
* `-r or --refdata` for reference data sources file

## Alternatively&#x20;

Use the provided scripts within the example projects copy them to your project environment. See use cases [running examples documentation](../use-cases/running-examples/).

### Start Joule

Start up the Joule process. Joule will use the `conf/app.env` configuration as its executing use case.

```bash
./bin/startJoule.sh
```

### Start Quote generator <a href="#start-quote-generator" id="start-quote-generator"></a>

By starting the quote simulator events will be published to Kafka ready for Joule to consume and process

```bash
./bin/startQuotePublisher.sh
```

### Shutting down processes

To stop the running processes just execute the below under the directory of which you started the processes

```bash
/bin/stopQuotePublisher.sh
/bin/stopJoule.sh
```

### Clean up environment

Once you have run the example and viewed the results you can cleanup the environment by running the below script.

```bash
./bin/cleanup.sh
```

### Prerequisites

* OpenJDK 19+ to run the platform
* To develop components read the [setting up environment](../joule-sdk/setting-up-the-environment.md) document for detailed requirements.

Within the example repository, we have provided a bash script for additional functionality. Check the [README.MD](https://gitlab.com/joule-platform/fractalworks-joule-examples/-/blob/main/README.md) for further information
