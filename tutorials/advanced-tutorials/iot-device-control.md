# IoT device control

## Objective

In the realm of Internet of Things (IoT), proactive adaptive control has emerged as the norm for managing remote devices. This case study presents a practical application of IoT in addressing low growth yields. It employs streaming sensor measurements, analytics, and control logic to devise a solution.

The scenario presents a mushroom grower encountering difficulties in optimising their production environment for mushroom cultivation. This is primarily attributed to the unpredictable fluctuations in temperature, humidity, and carbon dioxide levels, which severely disrupt optimal conditions for mushroom growth.

The objective is to automate the control of airflow valves to an optimal growing environment for each connected growing module.

Two key functions are required

1. Increase production output by optimising an independent growing module
2. Ensure heat, humidity and carbon dioxide levels remain optimal in each growing module

### Prerequisites

To get started building a custom processor ensure you have your development environment configured. Read the [environment setup](../../developer-guides/builder-sdk/environment-and-setup/environment-setup.md) documentation to get your environment ready to build.&#x20;

## Processing Overview <a href="#explaining-each-step" id="explaining-each-step"></a>

For a very simple processing overview sensor measurements are generated and published onto the `sensors/data` MQTT topic, processed and then published on to the `management_units` topic ready for valve controller to switch on or off the humidity ventilation unit.

```mermaid
 flowchart TD
    A[Sensor measurements] -->|Consume| B(filter)
    B --> C(window analytics)
    C --> D(control logic)
    D --> |Transform| E{Publish}
    E -->|File| F[CVS]
    E -->|MQTT| G[Valve controller]
```

## Development steps <a href="#explaining-each-step" id="explaining-each-step"></a>

{% stepper %}
{% step %}
### Setup environment

Within the “quickstart” folder, all the necessary files to execute the demonstration have been deployed. The configuration, parser, and transformer files are located in the “iot-demo” directory.

1. Install tools
2. Start Joule container
3. Setup the sensor simulator environment
4. Validate MQTT is running

### Install tools

**Python -** Python is used for the sensor simulator. [Official python downloads](https://www.python.org/downloads/)

**curl -** curl is used to execute Rest Joule API commands for transport registration, use case deployment etc. [Example install directions](https://help.ubidots.com/en/articles/2165289-learn-how-to-install-run-curl-on-windows-macosx-linux)

**Optional - Developer environment** To extend the demo you will need to install the [supported developer environment.](https://docs.fractalworks.io/joule/developer-guides/setting-up-developer-environment/environment-setup)

### Start Joule container

Run the below command to start up the Joule environment.

```bash
./quickstart/startupJoule
```

### Setup the sensor simulator environment <a href="#setup-the-sensor-simulator-environment" id="setup-the-sensor-simulator-environment"></a>

Sensor measurement events are generated using a python simulator. Sensors events are published on to the MQTT `sensors/data` topic as a JSON payload which is then parsed in to the Joule StreamEvent object ready for processing.

**Sensor Event**

The sensor measurement schema:

```
- eventTime
- sensorId
- temperature
- humidity
- co2
- mqValue
```

#### Example

```json
{
  "sensorId": 1001,
  "eventTime": 1735767588478,
  "temperature": 33.0,
  "humidity": 12.3,
  "co2": 12.2,
  "mqValue": 25
}
```

To use the simulator, the executing environment must be prepared for its intended use.

```bash
cd ./quickstart/bin/iot/sensor_simulator

python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install -r requirements.txt
```

#### Validate MQTT is running <a href="#validate-mqtt-is-running" id="validate-mqtt-is-running"></a>

Now validate messages can be sent and received using the running mqtt container.

**Subscriber**

```bash
mosquitto_sub -h localhost -t sensors/data
```

**Publisher**

```bash
mosquitto_pub -h localhost -t sensors/data -m "{\"sensorId\":1001,\"eventTime\":1735767588478,\"temperature\":33.0,\"humidity\":12.3,\"co2\":12.2}"
```

If all of this is working you are now ready to deploy a use case.
{% endstep %}

{% step %}
### Deploy use case

To deploy the use case, simply execute the following commands on the command line.

#### &#x20;Valve Control&#x20;

This forms the base functionality of the use case.

```
./quickstart/bin/iot/deployValveControlUsecase
```

#### &#x20;Audited Valve Control

This use case adds auditing to the processing stream. Joule records both the incoming outgoing events within the embedded in-memory database.

```bash
./quickstart/bin/iot/deployAuditedUsecase
```
{% endstep %}

{% step %}
### Monitor output

Subscribe to the Joule MQTT output topic

```bash
mosquitto_sub -h localhost -t management_units
```

#### Example output&#x20;

Output generation is triggered when the logic within the JavaScript function “turnONorOFF” is executed. At a minimum, events are generated every 30 seconds, as defined within the tumbling window function.

The output value of 1 corresponds to an 'on' valve signal, while 0 corresponds to an 'off' valve signal.

```json
{
  "eventTime": 1738709444907,
  "sensorId": 1001,
  "valve_signal": 1
}
```

\
Output events are also published to a local file.&#x20;

```bash
tail -F data/output/activatations/sensor_activations.csv
```
{% endstep %}

{% step %}
### Export audit data

The audit use case captures all raw and processed data. Joule records both the incoming outgoing events within the embedded in-memory database.

```bash
./quickstart/bin/iot/exportData --stream 'audited_sensor_analytics' --table 'raw' --from '2025-01-10 16:00:00' --to '2025-01-10 23:00:00' --filepath 'data/test.parquet' --format 'parquet'"
```
{% endstep %}

{% step %}
### Undeploy

To undeploy a use case and its artefacts a utility script has been provided which takes a use case name as an argument.

```bash
./quickstart/bin/iot/undeploy --usecase valve_control_uc
```
{% endstep %}
{% endstepper %}



