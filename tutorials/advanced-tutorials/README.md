---
description: Learning-oriented step-by-step guides. Ideal to begin your journey with Joule.
---

# Advanced tutorials

## Advanced tutorials

Progress beyond simple use cases by developing custom processors and analytics, use Joule's analytic capabilities and build ML prediction based pipelines.

{% stepper %}
{% step %}
[Custom missing value processor](build-a-processor.md)

Build, deploy and apply a custom **missing value processor** to provide consistent values over time.
{% endstep %}

{% step %}
[Stateless Bollinger band analytics](stateless-bollinger-band-analytics.md)

Learn how to create a custom **Bollinger band implementation** using the user defined function API  and apply it within a use case context.
{% endstep %}

{% step %}
[IoT device control](iot-device-control.md)

Apply proactive adaptive control to remote connected devices using a centralise control process.
{% endstep %}
{% endstepper %}
