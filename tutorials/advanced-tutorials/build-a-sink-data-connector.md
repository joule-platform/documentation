---
hidden: true
---

# Build a sink data connector

## Prerequisites

To get started building a custom processor ensure you have your development environment configured.&#x20;

Read the [environment setup](../../developer-guides/builder-sdk/environment-and-setup/environment-setup.md) documentation to get your environment ready to build.&#x20;

## Overview

These instructions cover how to define, build, deploy a use case on to the Joule Platform using the Getting started project provided [here](../../joules-tutorials/getting-started.md). These instructions should only be used to configure the Joule Platform for development or demonstration purposes.
