---
description: Levers driving actionable insights through real-time processing
---

# Joule capabilities

## Joule strengths

At the core of Joule lies its capability to connect to data, process it as streaming events regardless of type and structure, apply analytics and deliver insights on demand.

Ways to leverage Joule are:

1. Connect to diverse data sources and process them as real-time streaming events.
2. Apply analytics to generate continuous metrics for actionable insights.
3. Deliver real-time machine learning predictions to drive next-best actions.
4. Integrate contextual data to support complex business reasoning.
5. Leverage flexible processing mechanisms to streamline intricate use cases.

## Use cases

By applying a real-time event-driven stream processing architecture Joule can help towards solving complex problems fast:

<table data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Real-time customer offers</strong></mark></td><td>Adapt product offers while customers visit the e-commerce site</td><td></td></tr><tr><td><mark style="color:orange;"><strong>Geospatial intelligence</strong></mark></td><td>Use geospatial intel to perform remote asset monitoring </td><td></td></tr><tr><td><mark style="color:orange;"><strong>Predictive analytics</strong></mark></td><td>Apply real-time ML inferencing for proactive customer support</td><td></td></tr></tbody></table>
