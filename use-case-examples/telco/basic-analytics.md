---
description: >-
  SQL analytics is an accepted standard for all enterprise platforms. Joule has
  various analytics function point to address this requirement one being within
  the SELECT statement
---

# Basic analytics

***

This example demostrates a basic select aggregate ratio function based upon `IMSI` and `celltower` within the `select` statement.

## Use case configuration

_File: app-geospatialMarketingCampaign.env_

```bash
SOURCEFILE=conf/sources/mobileSimulatedStream.yaml
REFERENCEDATA=conf/sources/mobileReferenceData.yaml
ENGINEFILE=conf/usecases/analytics/mobileEventBasicAnalyticsFunctions.yaml
PUBLISHFILE=conf/publishers/enrichedEventsInfluxdb.yaml
```

Reference data stores can be used to prime the geofences. Change the required yaml file to test this feature in the app.env file.

```bash
ENGINEFILE=conf/usecases/analytics/mobileReferenceData.yaml
```

## Pipeline **configuration**

{% code overflow="wrap" %}
```yaml
processing unit:
  pipeline:
    - tokenizer enricher:
        tokenizers:
          imei : com.fractalworks.streams.examples.telco.enricher.IMEIDecoder
          latlng: com.fractalworks.streams.examples.telco.enricher.LatitudeLongitudeDecoder

    - obfuscation:
        name: piiMasking
        enabled: true
        cloneEvent: false
        fields:
          imsi:
            encryption:
              decrypt: false
              key location: ./keytmp
              aes bit length: 128
              rsa bit length: 2048
              rotate keys: true

    - enricher:
        enrich:
          deviceType:
            key: device
            using: deviceStore

          bundles:
            key: dataBundle
            using: dataBundleStore

        stores:
          dataBundleStore:
            storeName: bundles
            primaryKey: bundleid
            primaryKeyType: java.lang.Integer
            queryByKey: true

          deviceStore:
            storeName: mobiledevices
            primaryKey: tac
            primaryKeyType: java.lang.String

select:
  expression: "imsi, device.manufacturer, device.model, bytesUp, bytesDown, 'byteRatio' bytesUp / bytesDown, celltower, droppedCall, latitude, longitude"

group by:
  - imsi
  - celltower
```
{% endcode %}

## Select statement

```yaml
select:
    expression: "imsi, 
                 device.manufacturer, 
                 device.model, 
                 bytesUp, 
                 bytesDown, 
                 'byteRatio' bytesUp / bytesDown, 
                 celltower, 
                 droppedCall, 
                 latitude, 
                 longitude"
```
