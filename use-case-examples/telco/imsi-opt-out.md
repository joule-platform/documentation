---
description: >-
  Generally for customer driven insights data opt-out is a key function of the
  system. Joule provides this function using filters.
---

# IMSI Opt-Out

***

Within sensitive processing environments users may opt-out of personal data processing, for example real-time marketing or cross site tracking. To address this requirement event filtering is applied at the first stage of processing, known as opt-out.

Joule has an OOTB filtering processor which applies a custom Javascript expression to perform the filtering. This example simply filters a single IMSI from the processing pipeline.

## Use case configuration

_File: app-filtering.env_

```bash
SOURCEFILE=conf/sources/mobileSimulatedStream.yaml
ENGINEFILE=conf/usecases/filtering/mobileEventFiltering.yaml
PUBLISHFILE=conf/publishers/filteredMobileEventsFile.yaml
```

## Pipeline configuration <a href="#processor-configuration" id="processor-configuration"></a>

This pipeline will remove events that do not have values for the IMSI field as this can often occur in production system. The pipeline will also filter out and events with the IMSI set to `310120265624299`.&#x20;

```yaml
processing unit:
  pipeline:
    - filter:
        expression: "(imsi !== null ) ? true : false;"
    - filter:
        expression: "(imsi !== '310120265624299' ) ? true : false;"
```

## Note

Stay tuned as I will be putting together a static and dynamic solution that addresses a flexible approach to customer opt-out.
