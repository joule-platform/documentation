---
description: >-
  Through the use of geospatial analytics and tracking novel use cases can be
  imagined and realised.
---

# Location tracking

***

This use case demonstrates geospatial location alerting using mobile location data and user defined geofences. When a mobile enters a geofence zone a tracking trigger is generated and tracked until the user exits the geofence. Three tracking states are generated per entity; Entered, dwelling and exitted. Geofences are manually defined within the configuration file.

## Use case configuration

_File: app-geotrackedDevices.env_

```bash
SOURCEFILE=conf/sources/mobileSimulatedStream.yaml
ENGINEFILE=conf/usecases/analytics/mobileEventGeoTrackingAndAlertingStream.yaml
PUBLISHFILE=conf/publishers/anonymisedGeoTrackedMobileEventFile.yaml
```

## Pipeline **configuration**

This pipeline will add tracking geofence Ids, using defined geofences, to an event once a device as entered.&#x20;

```yaml
processing unit:
  pipeline:
    - filter:
        expression: "(imsi !== null ) ? true : false;"
    
    - entity geo tracker:
        name: imsiTracker
        entity key: imsi
        min dwelling time: 5
        timeUnit: SECONDS  
        geofences:
          -
            id: 1000
            coordinates: ["51.4623328","-0.1759467"]
            radius: 150
          -
            id: 2000
            coordinates: ["51.5136287","-0.1137969"]
            radius: 150
          -
            id: 3000
            coordinates: ["51.51359909390081","-0.10022502910787918"]
            radius: 150
            
          - id: 4000
            coordinates: [ "51.51376937553133","-0.10087411403509025" ]
            radius: 150
      
          - id: 5000
            coordinates: [ "51.5139730193047","-0.1035375532265023" ]
            radius: 150
            
          - id: 6000
            coordinates: [ "51.51436695471256","-0.10715183002670553" ]
            radius: 150
      
          - id: 7000
            coordinates: [ "51.51375264191751","-0.11159351772462783" ]
            radius: 150
```

### Output Event

The following fields are added to the processed StreamEvent object.

```
Key: geoTrackingInfo 
Value: GeoTrackingInfo
          - trackingTag
          - currentSpeedAndDirection
          - previousSpeedAndDirection
          - distanceTraveled
          - previousDistanceTraveled
          - previousLat
          - previousLng
          - currentLat
          - currentLng
          - geoFenceOccupancyState
          - previousUpdatedTimestamp
          - currentUpdatedTimestamp
```
