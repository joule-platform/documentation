---
hidden: true
---

# Running examples

***

## Getting the examples running locally <a href="#start-quote-generator" id="start-quote-generator"></a>

Once the infrastructure is running it's time to start running the use case. All the configurations have been provided so its simply the task of updating a single `app.env` file under the `conf` directory.  The initial use cases have been pre-configured and ready to run. All you will need to do is the following

* Start Joule processor/s
* Start event simulator

***

## Single Joule Processor

This method will fire up a single Joule processing unit using the base start and stop scripts.

### Start Joule

Start up the Joule process. Joule will use the `conf/app.env` configuration as its executing use case.

```bash
./bin/startJoule.sh
```

### Start Quote generator <a href="#start-quote-generator" id="start-quote-generator"></a>

By starting the quote simulator events will be published to Kafka ready for Joule to consume and process

```bash
./bin/startQuotePublisher.sh
```

### Shutting down processes

To stop the running processes just execute the below under the directory of which you started the processes

```bash
/bin/stopQuotePublisher.sh
/bin/stopJoule.sh
```

### Clean up environment

Once you have run the example and viewed the results you can cleanup the environment by running the below script.

```bash
./bin/cleanup.sh
```

***

## Joule Cluster

This method will fire up a set of Joule processing units using the base scripts along with wrapper scripts to generate, start and stop joule processes. Note this is a very loose definition of a cluster , its simply a set of processes executing within a local environment without all the frills of cluster management and tooling.&#x20;

### Setup Cluster

Create a set of joule process directories ready for futher configurations.

```bash
./bin/setupJouleCluster.sh -j 3
```

#### Notes

If you have enabled the rest api the port will need to be incremented for each process in the `joule.properties` file.

```
joule.rest.port=7070
```

### Start Cluster

This is sequentially start each process within there own local directory. Logging and data will be generated locally to the process directory.

```bash
./bin/startJouleCluster.sh
```

### Stop Cluster

This is sequentially stop each process within there own local directory.&#x20;

```bash
./bin/stopJouleCluster.sh
```
