---
description: >-
  To bring alive Joule to example projects based upon two industries, telco and
  banking, have been provided. Each project demonstrates various features Joule
  provides.
---

# Overview

## Telco

Based upon experience this example demonstrates various functions required for real-time mobile analytics. A mobile event simulator has been provided to drive the use cases.&#x20;

### Features demonstrated

* Geospatial analytics
* Basic analytics
* Anonymisation
* Filtering
* Enrichment

## Banking&#x20;

This example is inspired investment banking  example demonstrates various functions required for real-time market analytics. A market pricing simulator has been provided to drive the use cases.&#x20;

### Features demonstrated

* Sliding and tumbling window analytics
* Joule OOTB analytics aggregates
* Custom analytics
* Metrics engine
* Enrichment

***

## Support <a href="#support" id="support"></a>

Creating examples and a platform takes a significant amount of work. Joule is independently developed, funded and maintained by Fractalworks Ltd. If you would like to support please contact Fractalworks enquiries
