---
description: Groups events by attributes to drive optimised computation functions
---

# Group by

## Overview

To effectively implement window-based aggregate analytics in streaming applications, the `GROUP BY` clause is essential. Many streaming architectures utilise time windows to analyse data, whether defined by a specific time interval or a fixed number of events.

For instance, to determine the average bid price for each market symbol, we can group events by the symbol and compute the average within each time window. This approach not only condenses the volume of events published by the system but also enhances overall processing efficiency.&#x20;

By leveraging the `GROUP BY` functionality, we can optimise resource usage while maintaining the integrity of real-time analytics.

{% hint style="warning" %}
group by is required and must be provided with at least one valid attribute
{% endhint %}

## Example

In the following example we are grouping events by market and then by symbol.&#x20;

```yaml
stream:
  ...
  group by:
    - market
    - symbol
```
