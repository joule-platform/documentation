---
description: >-
  A use case binds together the required data and stream pipeline unit to
  perform the desired function
---

# Use case definition

## Overview

A use case definition brings together required data, stream processing and data consumers.

## Example

```yaml
use case:
  name: basic_twindow_analytics
  sources:
  - nasdaq_quotes_stream
  stream name: basic_tumbling_window_pipeline
  sinks:
  - kafka_analytics_view
```

## Attribute schema

Available configuration parameters

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Unique name of use case</td><td>String</td><td>true</td></tr><tr><td>sources</td><td><p></p><p>List of registered data sources deployed</p><p> </p></td><td>List&#x3C;String></td><td>true</td></tr><tr><td>stream name</td><td>Valid name of a register use case stream</td><td>String</td><td>true</td></tr><tr><td>sinks</td><td>List of registered data sinks deployed</td><td>List&#x3C;String></td><td>true</td></tr></tbody></table>

