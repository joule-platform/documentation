---
description: Audit and track events for validation and testing
---

# Telemetry auditing

## Overview

Joule provides a build in **telemetry auditing** feature, which records inbound and outbound events to support testing, model validation, retraining and other analyses.

The auditing process logs events into an in-memory SQL database at set intervals, allowing users to capture both `raw` incoming events and `processed` outgoing events.

{% hint style="info" %}
This feature is optional, it allows the users to configure it based on specific needs
{% endhint %}

## Example & DSL attributes

This example will write every inbound and outbound event to the in-memory database every 10 seconds using cloned events for inbound and the resulting event from the final processing step.&#x20;

Inbound events are defined as `raw` and outbound as `processed`.

```yaml
stream:
  ...
  telemetry auditing:
    raw:
      clone events: true
      frequency: 10
    processed:
      clone events: false
      frequency: 10
```

### Attributes schema

<table><thead><tr><th width="145">Attribute</th><th width="356">Description</th><th width="138">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>clone events</td><td>Flag to inform the platform to perform a deep copy of the event</td><td>Boolean<br>Default: false</td><td>false</td></tr><tr><td>frequency</td><td>Frequency in seconds to write events to the internal in-memory database. Setting must be greater or equal to 5 seconds</td><td>Integer<br>Default:  5 Secs</td><td>false</td></tr></tbody></table>
