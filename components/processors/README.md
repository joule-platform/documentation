---
icon: microchip
description: >-
  Processors are the core of the Joule platform, each performing a specific
  task. These create use case when linked together
---

# Processors

## Overview

Joule harnesses processors as the foundational units of its processing functionality, linking them together to form event stream pipelines. These pipelines are central to Joule's architecture, facilitating the implementation of various use cases with ease and efficiency.&#x20;

A pipeline can represent either an entire use case or a portion of one, offering flexible implementation options tailored to specific needs.

Pipelines consists of one or more interconnected processors, each designed to execute a distinct task. Events are processed sequentially, with the final output either routed to subsequent linked pipelines or delivered to designated data consumers.&#x20;

All processors maintain real-time observability metrics, accessible through any JMX monitoring tool. For more in-depth information, please refer to the observability [documentation](../observability/).

<figure><img src="../../.gitbook/assets/pipeline.png" alt=""><figcaption><p>A pipeline defines the key ingredient of a use case</p></figcaption></figure>

## Key features of Joule's processors capability include

1. <mark style="color:green;">**Modular design**</mark>\
   Processors can be combined flexibly into pipelines, allowing custom configurations for specific use cases.
2. <mark style="color:green;">**Real-time processing**</mark>\
   Events are processed sequentially and in real time, supporting high-speed, low-latency applications.
3. <mark style="color:green;">**Out-of-the-box processors**</mark>\
   Joule offers a wide range of ready-to-use processors for common tasks, accelerating deployment.
4. <mark style="color:green;">**Extensibility via SDK**</mark>\
   The Processor SDK allows developers to create custom processors, extending functionality to meet unique business needs.
5. <mark style="color:green;">**Built-in observability**</mark>\
   All processors provide real-time metrics accessible through JMX, enhancing monitoring and troubleshooting.

## Types of processors

Joule offers many out-of-the-box processor implementations that support the implementation of use cases.&#x20;

Processors are categorised by function, based on their role within the platform.

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Filters</strong></mark></td><td>Reduce stream processing overhead by filtering irrelevant events</td><td></td><td><a href="filters/">filters</a></td></tr><tr><td><mark style="color:orange;"><strong>Enrichment</strong></mark></td><td>Enrich events using linked reference data, metrics and analytics</td><td></td><td><a href="enrichment/">enrichment</a></td></tr><tr><td><mark style="color:orange;"><strong>Transformation</strong></mark></td><td>Transform event attribute values based upon a desired target state </td><td></td><td><a href="transformation/">transformation</a></td></tr><tr><td><mark style="color:orange;"><strong>Triggers</strong></mark></td><td>Apply rule based triggers for downstream business actions</td><td></td><td><a href="triggers/">triggers</a></td></tr><tr><td><mark style="color:orange;"><strong>Stream Join</strong></mark></td><td>Join independent stream events to trigger advance analytical insights and dynamic business rules</td><td></td><td><a href="stream-join/">stream-join</a></td></tr><tr><td><mark style="color:orange;"><strong>Event Tap</strong></mark></td><td>Tap events directly in to an in-memory database to enable on / off line processing</td><td></td><td><a href="event-tap/">event-tap</a></td></tr></tbody></table>

## Extending capabilities

Joule provides a Processors SDK to enable business developers to extend the processing capabilities.&#x20;

{% hint style="info" %}
See Processor API [documentation](../../developer-guides/builder-sdk/processor-api.md) for further information.
{% endhint %}
