---
description: Filter events based on specific types from the processing pipeline
---

# By type

## Objective

Filter events based on their type and subtype if declared.&#x20;

## Uses

There are various uses for this filter such as:

1. <mark style="color:green;">**In quality assurance**</mark>\
   Ensure a stream performs as expected before results distribution by introducing deliberate errors to validate its function.
2. <mark style="color:green;">**In data analytics**</mark>\
   When using Joule for metrics processing, remove events from the stream after logging them in the internal database to optimise resource use.
3. <mark style="color:green;">**In IT and memory management**</mark>\
   Reduce memory usage by filtering to process only specific event types within a broader parent category, minimising unnecessary overhead.

## Example & DSL attributes

This filter will exclude all events with type `crypto` and subtype `ftx` from the processing pipeline

```yaml
type filter:
  type: crypto
  subtype: ftx
  exclude: true
```

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>type</td><td>Event type</td><td>String</td><td>true</td></tr><tr><td>subtype</td><td>Event subtype</td><td>String</td><td>false</td></tr><tr><td>exclude</td><td>Flag to switch event exclusion</td><td><p>Boolean</p><p>Default: true </p></td><td>false</td></tr></tbody></table>
