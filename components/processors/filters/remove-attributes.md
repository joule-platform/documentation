---
description: Remove sensitive attributes from an event
---

# Remove attributes

## Objective

Remove sensitive attributes from the source event to meet data processing compliance requirements.&#x20;

## Uses

There are various uses for this filter such as:

1. <mark style="color:green;">**Financial Services**</mark>\
   Remove personally identifiable information (PII) from transaction data to comply with data privacy regulations.
2. <mark style="color:green;">**Telecommunications**</mark>\
   Optimise event payloads by excluding non-essential data, reducing memory usage and network load.
3. <mark style="color:green;">**In healthcare**</mark>\
   Filter out sensitive patient information before sharing data with third parties, ensuring compliance with health data protection laws.

## Example & DSL attributes

This filter will remove the fields `credit_card_number` and `address` from the source event.

```yaml
field filter:
  fields: 
    - credit_card_number
    - address
```

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>fields</td><td>List of fields to remove from the event</td><td>List&#x3C;String></td><td>true</td></tr></tbody></table>
