---
description: Use Javascript expressions to define custom filters
---

# By expression

## Objective

Enable developers the flexibility to define filtering logic using Javascript.&#x20;

This is ideal when there is a need to fine tune the filtering process to the needs of the use case.

## Uses

There are various uses for this filter such as:

1. <mark style="color:green;">**In software development**</mark>\
   Enable developers to create complex filtering logic by combining multiple conditions within a single filter process, simplifying the pipeline.
2. <mark style="color:green;">**In data science**</mark>\
   Use layered filters to refine datasets, allowing specific data points to pass through for targeted analysis.
3. <mark style="color:green;">**In cybersecurity**</mark>\
   Apply multi-layered filters to detect specific patterns or anomalies in real-time, improving threat detection accuracy.

## Example & DSL attributes

This will provide all `IBM` events to the next processors.

{% hint style="info" %}
Top level `StreamEvent` attributes are not available within the scripting processing context.
{% endhint %}

```yaml
filter:
  expression: "(typeof symbol !== 'undefined' && symbol === 'IBM') ? true : false;"
```

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>expression</td><td>A valid Javascript expression that returns a boolean value</td><td>String</td><td>true</td></tr></tbody></table>
