---
description: Drop all incoming events in the processor
---

# Drop all events

## Objective

This filter drops all events from a processing stream.

## Uses

There are various uses for this filter such as:

1. <mark style="color:green;">**In logistics**</mark>\
   Ensure that shipment tracking events are accurately recorded in the internal database, while dropping any erroneous or duplicate events from the stream.
2. <mark style="color:green;">**In e-commerce**</mark>\
   Validate that customer transaction events are processed correctly and remove any failed or invalid transactions to maintain accurate sales metrics.
3. <mark style="color:green;">**In smart manufacturing**</mark>\
   Validate event data is being received and processed. \
   Confirm that machine performance data is properly logged, while filtering out irrelevant data points to ensure accurate analytics and reporting.

## Example

```yaml
drop filter: {}
```
