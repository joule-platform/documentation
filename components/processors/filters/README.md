---
icon: filter
description: Reduce stream processing overhead by filtering irrelevant events
---

# Filters

## Overview

Filters are the foundational component to manage incoming events.

This is a technique that **removes irrelevant or sensitive data** from a stream by evaluating each element with **pre-set conditions**.&#x20;

Filters aim to **improve processing efficiency**, especially when only a subset of the data is needed for your use case.

These filters allow you a variety of options such as **filtering event based** using a configurable criteria or a custom expression.

## Use cases

Here are some use cases of how it can be applied in stream processing:

1. <mark style="color:green;">**Apply customer opt in / out policies**</mark>\
   Manage the inclusion of customer events within the processing stream using static lists.
2. <mark style="color:green;">**Addressing missing data elements**</mark>\
   Validate all required data is present before further stream processing by dropping events.
3. <mark style="color:green;">**Handling PII data**</mark>\
   Remove or mask PII field data within the event.&#x20;
4. <mark style="color:green;">**Handling out-of-range values**</mark>\
   Validate and process events that meet a within range criteria.&#x20;

## Available filters options

<table data-card-size="large" data-column-title-hidden data-view="cards" data-full-width="false"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>By type</strong></mark></td><td>Filter events based on specific types from the processing pipeline</td><td></td><td><a href="by-type.md">by-type.md</a></td></tr><tr><td><mark style="color:orange;"><strong>By expression</strong></mark></td><td>Use Javascript expressions to define custom filters.</td><td></td><td><a href="by-expression.md">by-expression.md</a></td></tr><tr><td><mark style="color:orange;"><strong>On delta</strong></mark></td><td>Stateful filter processor that filters previous seen events</td><td></td><td><a href="send-on-delta.md">send-on-delta.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Remove attributes</strong></mark></td><td>Remove sensitive attributes from an event</td><td></td><td><a href="remove-attributes.md">remove-attributes.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Drop all</strong></mark></td><td>Drop all incoming events in the processor</td><td></td><td><a href="drop-all-events.md">drop-all-events.md</a></td></tr></tbody></table>

