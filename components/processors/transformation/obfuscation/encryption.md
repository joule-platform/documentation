---
description: Apply RSA encryption techniques along with AES symmetric encryption
---

# Encryption

## Objective

Joule combines [RSA encryption](https://www.geeksforgeeks.org/rsa-algorithm-cryptography/) with [AES symmetric encryption](https://www.geeksforgeeks.org/advanced-encryption-standard-aes/) to achieve the security of RSA with the performance of AES.

Session based keys are generated on Joule process startup with the ability to rotate keys on subsequent process restarts.

## Example & DSL attributes

```yaml
obfuscation:
  name: addressEncryption
  fields:
    address:
      encryption:
        decrypt: false
        key location: /tmp
        aes bit length: 128
        rsa bit length: 2048
        rotate keys: true
```

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>aes bit length</td><td>Number of bits for the encryption key to be used. Supported bit sizes: 128, 192 and 256</td><td><p>Integer</p><p>Default: 128</p></td><td>false</td></tr><tr><td>RSA bit length</td><td>Number of bits for the encryption key to be used. Number of bits must be >= 2048</td><td><p>Integer</p><p>Default: 2048</p></td><td>false</td></tr><tr><td>decrypt</td><td>Boolean to inform process to either decrypt or encrypt field value</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>key location</td><td>Location to store encryption keys</td><td>String</td><td>false</td></tr><tr><td>rotate keys</td><td>Automatically refresh encryption keys on process restart</td><td><p>Boolean</p><p>Deault: true</p></td><td>false</td></tr></tbody></table>
