---
description: >-
  Joule provides an in-stream-obfruscation function to streaming events. This
  function is useful for Personal Identifiable Information de-identification.
---

# Obfuscation

## Objective

Joule provides an in stream obfuscation function to streaming events. This function is useful for Personal Identifiable Information **de-identification** through the application of encryption, masking or bucketing.

Field level obfuscation is achieved by assigning the required obfuscation type to the field along settable attributes.&#x20;

## Uses

Some of the various uses for this class of obfuscation transformations:

1. Encrypt and decrypt event fields using a performant RSA encryption with AES symmetric encryption process
2. Mask PII to meet compliance policies and GDPR requirements
3. Bucket PII information so that processing can occur without breaking any  compliance policies.
4. Redact, eliminate, sensitive event field data by either nullifying or replacing with a blank value.

## Available obfuscation options

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Encryption</strong></mark></td><td>Apply RSA encryption techniques along with AES symmetric encryption</td><td></td><td><a href="encryption.md">encryption.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Masking</strong></mark></td><td>Mask sensitive field values while retaining the formatting criteria</td><td></td><td><a href="masking.md">masking.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Bucketing</strong></mark></td><td>Apply individual variance tolerances to protect the identity of the originating data</td><td></td><td><a href="bucketing.md">bucketing.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Redaction</strong></mark></td><td>Eliminate sensitive event field data by either nullifying or replacing with a blank value</td><td></td><td><a href="redaction.md">redaction.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Custom implementations</strong></mark></td><td>To extend beyond the available obfuscation implementations an Obfuscation API is provided developers to build and deploy new types. </td><td></td><td><a href="../../../../developer-guides/builder-sdk/transformation-api/obfuscation-api.md">obfuscation-api.md</a></td></tr></tbody></table>

