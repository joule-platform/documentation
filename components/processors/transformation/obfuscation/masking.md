---
description: Mask sensitive field values while retaining the formatting criteria
---

# Masking

## Objective

A technique commonly used by dynamic data masking within the [RDBMS](https://www.javatpoint.com/what-is-rdbms).

On-the-fly data obfuscation rules are executed on the source data event to enable PII data.

Certain fields are replaced with a mask character (such as an ‘XXX’). This removes the actual content while preserving the same formatting.

## Example & DSL attributes

This code defines an **obfuscation** strategy named `numberMasking`. It applies to the `creditcard` field and uses the following masking:

1. <mark style="color:green;">**pattern**</mark>\
   The credit card number is displayed as `XXXX XXXX XXXX 1234`, where only the last four digits are visible.
2. <mark style="color:green;">**mask**</mark>\
   The `*` character is used to mask the other digits.

This ensures that the credit card number is partially hidden, showing only the last four digits.

```yaml
obfuscation:
  name: numberMasking
  fields:
    creditcard:
      masking:
        pattern: XXXX XXXX XXXX 1234
        mask: "*"
```

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>mask</td><td>Replacement character to be applied</td><td><p>Char</p><p>Default: *</p></td><td>false</td></tr><tr><td>pattern</td><td>Masking pattern to be applied to source string value</td><td>String</td><td>false</td></tr><tr><td>apply to all</td><td>Boolean to inform if all characters need to be replace using the mask value (true) or to apply pattern (false). </td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr></tbody></table>
