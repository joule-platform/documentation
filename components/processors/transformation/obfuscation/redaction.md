---
description: >-
  Eliminate sensitive event field data by either nullifying or replacing with a
  blank value
---

# Redaction

## Objective

Effective method of eliminating sensitive event field data by either nullifying or replacing with a blank value.

## Example & DSL attributes

This DSL will replace the contents of the field with blank field.

The **obfuscation** strategy called `redactPII`, which is applied to two fields: `address` and `firstlast_name`.

1. <mark style="color:green;">**address**</mark>\
   The value for the `address` field is redacted and replaced with `null` (i.e., the address is completely removed).
2. <mark style="color:green;">**firstlast\_name**</mark>\
   The value for the `firstlast_name` field is not redacted and remains unchanged (`as null: false` means no obfuscation).

This strategy helps protect personally identifiable information (PII) by redacting sensitive data while retaining non-sensitive data.

```yaml
obfuscation:
  name: redactPII
  fields:
    address: 
      redact:
        as null: true
    firstlast_name:
      redact:
        as null: false
```

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>as null</td><td>If set to true replace value with a null otherwise as a empty string</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr></tbody></table>
