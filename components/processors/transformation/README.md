---
icon: money-bill-transfer
description: Reshape origin event data in to target event
---

# Transformation

## Overview

A streaming event transform converts a source event in to transformed event and event properties. Transformation are performed in real-time using a continuous process. &#x20;

Support includes derived attributes, transformed properties, custom and transformed events.&#x20;

## Use cases

Here are some use cases of how it can be applied in stream processing:

1. <mark style="color:green;">**Customer data**</mark>\
   Mask Personal Identifiable Information.
2. <mark style="color:green;">**GDPR compliance**</mark>\
   Tokenise a data value in to component parts and randomise sensitive components.
3. <mark style="color:green;">**Secure communication**</mark>\
   Encrypt sensitive data for endpoint consumption.
4. <mark style="color:green;">**Feature engineering**</mark>\
   Basic bucketing for statistical processing.
5. <mark style="color:green;">**Domain transformers**</mark>\
   Convert from source and to target domain data structures.

## Available transformation options

Customise events in real-time using Joule out-of-the-box transformation processors.&#x20;

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Field Tokeniser</strong></mark></td><td>Tokenise attribute values in to component parts</td><td></td><td><a href="field-tokeniser.md">field-tokeniser.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Obfuscation</strong></mark></td><td>Obfuscate streaming events to meet compliance policies</td><td></td><td><a href="obfuscation/">obfuscation</a></td></tr><tr><td><mark style="color:orange;"><strong>Feature engineering</strong></mark></td><td>Decorate a feature vector with enriched features specific to the deployed model</td><td></td><td><a href="../../analytics/ml-inferencing/feature-engineering/">feature-engineering</a></td></tr><tr><td><mark style="color:orange;"><strong>Domain Transformers</strong></mark></td><td>Parse incoming events and transform outgoing events</td><td></td><td><a href="../../connectors/serialisers/">serialisers</a></td></tr></tbody></table>
