---
description: Distribute delta records for key attribute changes
---

# Change Data Capture

## Objective

Change Data Capture (CDC) processor identifies and processes only data that has changed and then makes this changed data available for further use as a delta event.

## Uses

The CDC process determines change based upon the previous values held for the tracking fields. This presents various uses for this processor such as:

1. <mark style="color:green;">**In financial services**</mark>\
   Provide a configurable _inform on update_ consumer pattern.\
   Implement a system where traders receive notifications only for changes in stock prices or account balances, rather than the entire transaction details, allowing for quicker decision-making.
2. <mark style="color:green;">**In healthcare**</mark>\
   Optimised delta processing for event consumers and receive updates only on specific tracking fields vs the full event.\
   Enable healthcare providers to receive alerts about changes in patient data, such as medication dosages or vital signs, without needing the entire medical record, ensuring focused and timely responses.
3. <mark style="color:green;">**In retail**</mark>\
   Allow inventory management systems to inform staff only when stock levels change for specific items, rather than sending complete inventory reports, streamlining operations and improving response times.

## Example & DSL attributes

This will emit changed `StreamEvents` keyed on symbol when either, **OR function**, the `broker`, `bid` or `ask` values change.

```yaml
change data capture:
  key field: symbol
  tracking fields:
    - broker
    - bid
    - ask
  emit only changes: true
  drop duplicates: true
```

### Attributes schema

<table><thead><tr><th width="214">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>key field</td><td>Key field to </td><td>String</td><td>true</td></tr><tr><td>fields</td><td>List of fields to track and determine changes</td><td>List&#x3C;String></td><td>true</td></tr><tr><td>emit only changes</td><td>A flag to emit only a delta event for the changes seen on the record. If set to false  changes found will be will be added, as a map, to the <code>CDC_FIELD</code> event along with a <code>CDC</code> field being set to true to signify record availability</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>drop duplicates</td><td>Control flag which can be set to validate processor functionality at runtime. Use a JMX console to set</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr></tbody></table>
