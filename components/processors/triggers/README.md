---
icon: up-right-and-down-left-from-center
description: Minimise expensive computation while notifying consumers of events to action
---

# Triggers

## Overview

Real-time alerts and triggers is generally accepted a key feature of stream processing. Joule provides two key features to trigger **contextual and business rules based change**, CDC and declarative rules.&#x20;

## Use cases

Here are some use cases of how triggers it can be applied in stream processing:

1. <mark style="color:green;">**Consistent 360 degree customer profiles**</mark>\
   Provide timely key customer attributes update to trigger key processing streams.
2. <mark style="color:green;">**Optimised consumer processing**</mark>\
   Drive high-throughput processing streams using low memory delta records.&#x20;

## Available trigger options

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Change Data Capture</strong></mark></td><td>Distribute delta records for key attribute changes</td><td></td><td><a href="change-data-capture.md">change-data-capture.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Business rules</strong></mark></td><td>Leverage standard JSR-94 rules engine to perform advance business rules</td><td></td><td><a href="business-rules.md">business-rules.md</a></td></tr></tbody></table>
