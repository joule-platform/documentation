---
description: Leverage standard JSR-94 rules engine to perform advance business rules
---

# Business rules

## Objective

Enable businesses processes to leverage streaming data using **existing business rules** assets.

## Uses

Rules processing is well understood, mature technology which has many uses across industries.

Presented below are few uses within a streaming context:

1. <mark style="color:green;">**In financial services**</mark>\
   Enable a business to migrate towards a stream-based rules processing model, allowing for real-time risk assessment and compliance monitoring.
2. <mark style="color:green;">**In logistics**</mark>\
   Gain live customer insights using existing rule assets to improve delivery times and optimise route planning based on real-time data.
3. <mark style="color:green;">**In retail**</mark>\
   Implement a system for tracking customer purchasing behaviours, leveraging existing rule assets to provide tailored promotions and enhance customer satisfaction.

## Examples & DSL attributes

This snippet will load the Drools rules file, compile and build the internal rules context ready for event based rules processing.

```yaml
rules:
  name: anticipated high mobile usage
  rule file: /home/joule/rules/dataUsageRules.drl
```

### Attributes schema

<table><thead><tr><th width="214">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Name of processor</td><td>String</td><td>false</td></tr><tr><td>rule file</td><td>Drools rule file path</td><td>String</td><td>true</td></tr><tr><td>pool size</td><td>Non-functional setting to size the internal caching of Drools container </td><td><p>Integer</p><p>Default: 8</p></td><td>false</td></tr></tbody></table>

{% hint style="info" %}
Current implemented use **Redhat Drools Business Rules Management** solution [v8.44.0.Final](https://www.drools.org) which is a [JSR-94](https://www.oracle.com/technical-resources/articles/javase/javarule.html) standard implementation
{% endhint %}

### Example rules file

This example codifies the below data usage alerting business rules in to a set of Drools rules that are declared with a `drl` file.

#### Drools rules

1. <mark style="color:green;">**Low data usage**</mark>\
   Less than 50% data used and has more than equal to 15 days left.
2. <mark style="color:green;">**Medium data usage**</mark>\
   More than 50% and less than 65% data used and has less than 10 days left.
3. <mark style="color:green;">**High data usage**</mark>\
   More than 90% data used and has less than equal to 5 days left.

{% hint style="info" %}
To learn more about defining rules go to the Drools rules language [documentation](https://docs.drools.org/8.44.0.Final/drools-docs/drools/language-reference/index.html).
{% endhint %}

Whenever a rule condition is triggered the passed event has a `RuleResponse` object added, see below for further details.

```java
package com.fractalworks.streams.processors.rules;

import com.fractalworks.streams.core.data.streams.StreamEvent;

global com.fractalworks.streams.core.data.streams.Context ctx;
global com.fractalworks.streams.processors.rules.RuleResponse response;

dialect  "mvel"

rule "Low data usage"
    when
        StreamEvent( getValue("dataUsage") < 0.50,
                     getValue("daysleft") >= 15 )
    then
        response.setRuleId("Low data usage");
        response.put("desc","Low data usage");
end

rule "Medium data usage"
    when
        $t : StreamEvent( getValue("dataUsage") > 0.50,
                          getValue("dataUsage") < 0.65,
                          getValue("daysleft") < 10)
    then
        response.setRuleId("Medium data usage");
        response.put("desc","Medium data usage");
end

rule "High data usage"
    when
        $t : StreamEvent( getValue("dataUsage") > 0.90,
                          getValue("daysleft") <= 5)
    then
        response.setRuleId("High data usage");
        response.put("desc","High data usage");
end

```

### RuleResponse Object

This object contains custom rule responses that drive further processing within the use case. For the above example we simply added a description of what rule was fired.&#x20;

The `RuleResponse` extends from the [HashMap](https://www.geeksforgeeks.org/map-interface-java-examples/) class with an additional `ruleId` field. The `ruleId` is used to inform further processing which rule was triggered, this is a requirement.

Any number of response attributes can be added to the response object.

{% hint style="info" %}
The processor manages the creation of the RuleResponse object and the addition to the StreamEvent.
{% endhint %}
