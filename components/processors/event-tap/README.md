---
icon: arrows-to-circle
description: >-
  Tap events directly in to an in-memory database to enable on / offline
  processing
---

# Event tap

## Overview

Joule allows you to tap into an event stream and load it into an in-memory database, making the data accessible in SQL format.

## Uses

There are various uses for this filter such as:

1. <mark style="color:green;">**In financial services**</mark>\
   Enables offline analytics and when combined with transaction data, supports in-depth risk analysis and fraud detection.
2. <mark style="color:green;">**In logistics**</mark>\
   Build curated datasets by regularly exporting shipment and inventory data, allowing for historical analysis and performance tracking.
3. <mark style="color:green;">**In manufacturing**</mark>\
   Capture processed events to validate processing and provide lineage, ensuring compliance with industry regulations and improving quality control measures.

{% hint style="info" %}
See how the [Joule REST API](../../connectors/sources/rest-endpoints.md) can provide data exporting and query features.
{% endhint %}

## Accessing the Tap

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Anatomy of a Tap</strong></mark></td><td>Construction of a Tap</td><td></td><td><a href="anatomy-of-a-tap.md">anatomy-of-a-tap.md</a></td></tr><tr><td><mark style="color:orange;"><strong>SQL Queries</strong></mark></td><td>Use SQL queries to access the data in the store</td><td></td><td><a href="sql-queries.md">sql-queries.md</a></td></tr></tbody></table>
