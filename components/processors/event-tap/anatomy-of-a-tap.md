---
description: Construction of a Tap
---

# Anatomy of a Tap

## Overview

This article should give context about how a Tap is constructed.

## Example & DSL attributes

This code defines a **tap** configuration for streaming data to the `nasdaq_quotes` schema.

1. <mark style="color:green;">**target schema**</mark>\
   Specifies that the data will be sent to the `nasdaq_quotes` schema.
2. <mark style="color:green;">**queue capacity**</mark>\
   Sets the maximum capacity of the queue to 15,000 events.
3. <mark style="color:green;">**flush frequency**</mark>\
   Data will be flushed (written) every 5 events.
4. <mark style="color:green;">**index**</mark>\
   An index is created on the `symbol` field, but it is not marked as unique (`unique: false`).

This configuration controls how data is buffered and indexed before being sent to the target schema.

### Example

```yaml
tap:
  target schema: nasdaq_quotes
  queue capacity: 15000
  flush frequency: 5
  index:
    unique: false
    fields:
      - symbol
```

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>target schema</td><td>Target table name</td><td>String</td><td>true</td></tr><tr><td>queue capacity</td><td><p>Number of events to queue before flushing to database.</p><p></p><p>Must be greater than 99</p></td><td><p>Integer</p><p>Default :10000</p></td><td>false</td></tr><tr><td>flush frequency</td><td><p>Frequency the queue is flushed to database table. Either the queue capacity or this attribute triggers queue flush whatever comes first</p><p></p><p>Must me greater than zero</p></td><td><p>Long</p><p>Default: 5 Seconds</p></td><td>false</td></tr><tr><td>alias</td><td>Alias to use instead of the schema name</td><td>String</td><td>false</td></tr><tr><td>is insert</td><td>Update or insert to apply to table changes</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>update criteria fields</td><td>Array of fields used for the update criteria predicate. Only used if <code>is insert</code> set to false</td><td>String[]</td><td>false</td></tr><tr><td>index</td><td>Create a table index</td><td>See Index attributes</td><td>false</td></tr></tbody></table>

### Index Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>unique index</td><td>Create a unique index</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>index fields</td><td>Array of fields to create index</td><td>String[]</td><td>true</td></tr></tbody></table>
