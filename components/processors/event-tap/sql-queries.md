---
description: Use SQL queries to access the data in the store
---

# SQL Queries

## Overview

On processor initialisation, a set of queries are auto generated which are used to bind Rest API endpoints.

## SELECT

The following queries are generated:

1. Select data using pagination either with or without an `ingestTime` date range. &#x20;
2. Ability to export the table either by `CSV` or `Parquet` format.
3. Query to delete rows from the table between an `ingestTime` date range.&#x20;

### Example

The below example exports the quotes table within the `nasdaq_quotes` schema to a local parquet file.

Read the Data API query [page](../../../developer-guides/api-endpoints/data-access-api/query.md) more information on how to access the captured data.

```javascript
const response = await fetch('/joule/query/nasdaq_quotes/quotes}', {
    method: 'GET',
    headers: {},
});
const data = await response.json();
```
