---
description: Strict join between two event streams
---

# Inner stream joins

## Objective

To perform a **strict join between two event streams**, an inner join is used.

This join operator emits an event only when a **successful match occurs** between streams. Otherwise, it waits until the join criteria are met, as determined by the configured policy settings.

## Uses

Among the previously mentioned examples in the [stream join concept page](./), the application for inner joins can impact the following:

1. <mark style="color:green;">**Immediate feedback loop**</mark>\
   It enables businesses to receive immediate feedback on customer behaviour, allowing for rapid adjustments to marketing strategies.
2. <mark style="color:green;">**Enhanced customer experience**</mark>\
   By understanding which ads are clicked by which customers, businesses can personalise experiences and improve overall engagement.

## Example

This code defines a **join** operation for two streams (`sitevisits` and `webpage.adclicks`), based on a common `customerId`.

1. <mark style="color:green;">**expression**</mark>\
   The join condition is that `sitevisits.customerId` should match `webpage.adclicks.customerId`.
2. <mark style="color:green;">**merge events**</mark>\
   When the condition is met, the events from both streams are merged.
3. <mark style="color:green;">**left policy (time to live)**</mark>\
   The `sitevisits` stream events are kept for a maximum of 30 minutes before they expire.
4. <mark style="color:green;">**right policy (delete on join)**</mark>\
   Once a matching event from `webpage.adclicks` is joined with `sitevisits`, the event from `webpage.adclicks` is deleted.

This strategy merges data while ensuring that events are kept only within a specific timeframe and properly cleaned up once they are joined.

{% hint style="info" %}
Inner joins are set by `==`
{% endhint %}

```yaml
streams join:
  expression: "sitevisits.customerId == webpage.adclicks.customerId"
  merge events: true
  left policy:
    time to live: 30 minutes
  right policy:
    delete on join: true
```
