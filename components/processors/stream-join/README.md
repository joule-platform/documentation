---
icon: object-exclude
description: >-
  Join independent stream events to trigger advanced analytics and dynamic
  business rules
---

# Stream join

## Overview

The join operation in stream processing is a powerful function that **accelerates time-to-insight** by correlating multiple event streams into a cohesive, meaningful flow. This is essential for scenarios requiring **real-time data analysis**.

Unlike real-time data enrichment, which supplements data with computed or static data sets, stream joining focuses on combining live events for immediate insights and improve customer engagement.

<figure><img src="../../../.gitbook/assets/join-ads.png" alt="" width="375"><figcaption><p>Real-time customer promo example</p></figcaption></figure>

## Use cases

* <mark style="color:green;">**Ad Performance tracking in real-time**</mark>\
  Track ad clicks, impressions, and user actions in real-time to refine ad spend and gather customer insights.
* <mark style="color:green;">**Dynamic promotions**</mark>\
  Detect new leads on your site, track product interactions, and apply promotional rules to deliver tailored promotions.
* <mark style="color:green;">**Product recommendations**</mark>\
  Combine user sessions, viewed ads, and clicks to generate personalised product suggestions, with feedback loops for ongoing improvement.
* <mark style="color:green;">**Customer 360 proactive support**</mark>\
  Analyse multiple customer telemetry streams to provide proactive, personalised support and enhance the customer experience.
* <mark style="color:green;">**Platform anomaly detection**</mark>\
  Monitor and analyse telemetry from devices to detect unusual compute or network usage patterns, supporting operational efficiency and reliability.

## Available stream join options

On the Joule platform, there are two types of joins available.

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Inner stream joins</strong></mark></td><td>Strict join between two event streams</td><td></td><td><a href="inner-stream-joins.md">inner-stream-joins.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Outer stream joins</strong></mark></td><td>Immediately pass the first event received and initialised downstream processors</td><td></td><td><a href="outer-stream-joins.md">outer-stream-joins.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Join attributes &#x26; policy</strong></mark></td><td>Join attributes to control and optimise performance</td><td></td><td><a href="join-attributes-and-policy.md">join-attributes-and-policy.md</a></td></tr></tbody></table>
