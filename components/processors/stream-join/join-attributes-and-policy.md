---
description: Join attributes to control and optimise performance
---

# Join attributes & policy

## Stream join examples

{% tabs %}
{% tab title="Inner stream join" %}
```yaml
streams join:
  expression: "sitevisits.customerId == webpage.adclicks.customerId"
  merge events: true
  left policy:
    time to live: 30 minutes
  right policy:
    delete on join: true
```
{% endtab %}

{% tab title="Outer stream join" %}
```yaml
streams join:
  expression: "sitevisits.customerId *= webpage.adclicks.customerId"
  merge events: true
  left policy:
    time to live: 30 minutes
  right policy:
    delete on join: true
```
{% endtab %}
{% endtabs %}

These attributes define how the join is performed with respect to the expression, how to create the emitted event structure etc.

### Attributes schema

<table><thead><tr><th width="152">Attribute</th><th width="282">Description</th><th width="205">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>expression</td><td>Simple join expression that evaluates two streams </td><td>String</td><td>true</td></tr><tr><td>merge events</td><td>Flag that either merge event attributes into a flattened event structure or places each event </td><td>Boolean<br>Default: true</td><td>false</td></tr><tr><td>event type</td><td>User defined type of emitted event</td><td>String<br>Default: left type - right type (i.e. typeA-typeB)</td><td>false</td></tr><tr><td>left / right join</td><td>See Join attribute section</td><td>Policy attribute<br>Default</td><td>false</td></tr></tbody></table>

## Policy Attributes

Fine grained control can be applied to how joins are handled through the use of the policy attribute.

This becomes relevant for **large and fast event stream** with respect to memory and processing overhead.&#x20;

**Default configuration**

1. <mark style="color:green;">**delete on join**</mark>\
   is set to false and therefore will join events until the time to live setting is honoured.
2. <mark style="color:green;">**time to live**</mark>\
   is set for 60 minutes per stored event. This is refreshed every time a new event is received with the same join attribute value.

### Attributes schema

<table><thead><tr><th width="150">Attribute</th><th width="290">Description</th><th width="201">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>delete on join</td><td>Delete event from state management on a successful  join</td><td>Boolean<br>Default: false</td><td>false</td></tr><tr><td>time to live</td><td>Expire configuration for event stored within the state management system. Supported time units include nanoseconds, microseconds, milliseconds, seconds, hours and days</td><td>Formatted string<br>Default: 60 minutes</td><td>false</td></tr></tbody></table>

