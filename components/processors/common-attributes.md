---
description: Core processor attributes that provide out-of-the-box features
---

# Common attributes

## Overview

Every Joule OOTB processor will have these common attributes.

## Example & DSL attributes

The `mobile_device_info_enricher` enriches incoming events by adding `deviceManufacturer` and `model` information using the `tac` key from the `mobiledevices` store.

It triggers after a 50-second delay and clones the event with the enriched data.

```yaml
enricher:
  name: mobile_device_info_enricher
  clone event: true
  latency trigger: 50000
  fields:
    deviceManufacturer:
      with values: [deviceManufacturer, year_released]
      by key: tac
      using: deviceStore

    model:
      with values: [modelNumber]
      by key: tac
      using: deviceStore

  stores:
    deviceStore:
      store name: mobiledevices
```

### Attribute schema

<table><thead><tr><th width="171">Attribute</th><th width="349">Description</th><th width="327">Data Type</th></tr></thead><tbody><tr><td>name</td><td>Unique name of processor. </td><td><p>String</p><p>Default: Random UUID</p></td></tr><tr><td>enabled</td><td>Toggle to enable processor processing</td><td><p>Boolean</p><p>Default: true</p></td></tr><tr><td>properties</td><td>Processor specific properties</td><td>Map</td></tr><tr><td>clone event</td><td>Toggle to clone event for processing. Note this will cause significant memory overhead for high load use cases</td><td><p>Boolean</p><p>Default: false</p></td></tr><tr><td>stores</td><td>External data stores can be attached, useful for analytics, enrichment, filtering and transformation use cases</td><td>Map</td></tr><tr><td>latency trigger</td><td>Number of events processed to trigger the average processing calculation. A value less than 1 switches off the calculation</td><td><p>Integer</p><p>Default: 1000</p></td></tr></tbody></table>
