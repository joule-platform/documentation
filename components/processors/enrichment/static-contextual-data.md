---
description: >-
  Enrich events with essential slow changing data from Joule’s in-memory
  database.
---

# Static contextual data

## Objective

The enricher enables users to enhance events with multiple data elements through advanced mapping.

Slow-moving contextual data such as

* Customer information (i.e. subscriptions, product interactions)
* Contract pricing
* Product skews
* Daily prices
* Financial market data such as; FX rates, interest rates, start of day market index levels etc;.
* Complex engineered ML features&#x20;

are essential for **rule-based triggers** in business functions.

Joule supports this by using its internal database and allowing data import during the initialisation process (see [documentation](../../pipelines/data-priming/) for details).

## Uses

There are various uses for this filter such as:

1. <mark style="color:green;">**In retail**</mark>\
   Utilise static discount rules and seasonal pricing stored in the in-memory database to automatically apply the latest price adjustments to product events, supporting personalised promotions.
2. <mark style="color:green;">**In financial services**</mark>\
   Leverage exchange rate data by querying an in-memory database, enabling real-time currency conversion and accurate, timely financial calculations in customer-facing apps.
3. <mark style="color:green;">**In transportation**</mark>\
   Enrich vehicle routing events with fuel price data stored in-memory to enable efficient route adjustments based on the latest fuel costs, supporting cost-effective logistics.

## Architecture

Joule's architecture for handling static contextual data relies on its internal database, with data imported during the initialisation phase.

<figure><img src="../../../.gitbook/assets/static reference data.png" alt=""><figcaption></figcaption></figure>

## Example & DSL attributes

This code defines an enricher that enhances events with data from the `JouleDB` using SQL queries. There are three enrichment strategies:

1. <mark style="color:green;">**with\_values**</mark>\
   Retrieves the `exchange_rate` for a given `symbol` using a SQL query from the `fx_rates` table.
2. <mark style="color:green;">**as\_object**</mark>\
   Retrieves all details as an object for a given `symbol` from the `fx_rates` table.
3. <mark style="color:green;">**all\_attributes**</mark>\
   Retrieves all attributes for a given `symbol` from the `fx_rates` table.

Each enrichment uses a query with `symbol` as the key to fetch data from the `JouleDB` store.

{% hint style="danger" %}
Not supported:\
The [by key](key-concepts/anatomy-of-enrichment-dsl.md#by-key) attribute is not supported when using the Joule in-memory database.
{% endhint %}

```yaml
enricher:
  fields:
    with_values:
      by query:  "select * from fx_rates where symbol = ?"
      query fields: [symbol]
      with values: [exchange_rate]
      using: JouleDB

    as_object:
      by query: "select * from fx_rates where symbol = ?"
      query fields: [symbol]
      as object: true
      using: JouleDB

    all_attributes:
      by query: "select * from fx_rates where symbol = ?"
      query fields: [symbol]
      all attributes: true
      using: JouleDB
```

### Attributes schema

To direct the enricher to use Joule's in-memory database apply the setting to the `using` DSL attribute.

<table><thead><tr><th width="190">Attribute</th><th width="392">Description</th><th width="174">Data Type</th></tr></thead><tbody><tr><td>using: JouleDB</td><td>Directs processor to bind to the in-memory Joule database. No further storage configuration is required.</td><td>String</td></tr></tbody></table>
