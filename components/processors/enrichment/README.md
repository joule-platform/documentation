---
icon: object-union
description: Enrich events using connected contextual data, metrics and analytics
---

# Enrichment

## Overview

Enrichment is a common practice across industries and Joule makes it simpler.

In advanced streaming scenarios, it's often necessary to add extra contextual data to **improve processing** **accuracy** and **decision-making**.

By using **low-latency data stores**, streaming events can be quickly enriched with up-to-date information, ensuring **fast and responsive** data retrieval.

The key concepts and the anatomy of enrichment DSL will deep dive in the concept of enrichment.

{% content-ref url="key-concepts/" %}
[key-concepts](key-concepts/)
{% endcontent-ref %}

{% content-ref url="key-concepts/anatomy-of-enrichment-dsl.md" %}
[anatomy-of-enrichment-dsl.md](key-concepts/anatomy-of-enrichment-dsl.md)
{% endcontent-ref %}

## Use cases

Here are some use cases of how it can be applied in stream processing:

1.  <mark style="color:green;">**Customer data**</mark>

    Adding information such as data bundle plans or credit ratings.
2.  <mark style="color:green;">**Company metrics**</mark>

    Enriching streams with market ratings or previous day stock sensitivities.
3.  <mark style="color:green;">**Technical operations**</mark>

    Including data like cell tower signal coverage or machine utilisation limits.
4.  <mark style="color:green;">**Geofencing**</mark>

    Enriching streams with static geofences for places of interest.
5.  <mark style="color:green;">**Dynamic triggers**</mark>

    Incorporating real-time business triggers like SLAs or stock price levels.
6.  <mark style="color:green;">**Domain-specific metrics**</mark>

    Adding general KPIs or metrics for domain-level insights.

## Available enrichment options

Contextual data, usually stored in data marts or warehouses changes slowly. Integrating it into a high-throughput streaming platform **can cause processing delays**.

These processing delays are often due to the I/O overhead from the network and storage layers. To tackle these challenges, Joule offers a set of optimised solutions. Following enrichers are available.

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Metrics</strong></mark></td><td>Enrich events with pre-calculated metrics to facilitate further pipeline processing</td><td></td><td><a href="metrics.md">metrics.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Dynamic contextual data</strong></mark></td><td>Use fast, connected data stores to quickly enrich in-motion events with up-to-date context</td><td></td><td><a href="dynamic-contextual-data/">dynamic-contextual-data</a></td></tr><tr><td><mark style="color:orange;"><strong>Static contextual data</strong></mark></td><td>Enrich events with essential slow changing data from Joule’s in-memory database.</td><td></td><td><a href="static-contextual-data.md">static-contextual-data.md</a></td></tr></tbody></table>
