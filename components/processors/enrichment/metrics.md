---
description: >-
  Enrich events with pre-calculated metrics to facilitate further advanced
  processing
---

# Metrics

## Objective

Advanced use cases may need **pre-computed metrics** as inputs for calculations, event triggers, or additional context.

The enricher processor uses the [Metrics Engine](../../analytics/metrics-engine/) query interface to query and bind metrics to an event using additional DSL attributes

{% hint style="info" %}
The Metrics Engine is enabled by default using the following configuration in joule.properties:  `joule.internaldb.enable=true`
{% endhint %}

## Uses

There are various uses for this filter such as:

1. <mark style="color:green;">**In retail**</mark>\
   Utilise real-time sales data and competitor pricing metrics to adjust product prices dynamically, maximising profit margins and ensuring competitiveness.
2. <mark style="color:green;">**In manufacturing**</mark>\
   Use sensor data from machinery to predict failures before they occur, enabling proactive maintenance scheduling and reducing unplanned downtime.
3. <mark style="color:green;">**In e-commerce**</mark>\
   Combine user behaviour data with product performance metrics to generate personalised product recommendations, enhancing the customer shopping experience.

## Example & DSL attributes

This example defines three enrichment strategies:

1. <mark style="color:green;">**bid\_ask\_avg**</mark>\
   Retrieves `avg_ask` and `avg_bid` values by using the `AskBidMovingAverage` metric family and `symbol` as the key from the `MetricsDB`.
2. <mark style="color:green;">**market\_depth\_metrics**</mark>\
   Fetches all attributes related to `marketDepth` for a given `symbol` from the `MetricsDB`.
3. <mark style="color:green;">**trading\_triggers**</mark>\
   Retrieves `tradingTriggers` data as an object for a given `symbol` from the `MetricsDB`.

All enrichments use the `MetricsDB` for data retrieval.

```yaml
enricher:
  fields:
    bid_ask_avg:
      by metric family: AskBidMovingAverage
      by key: symbol
      with values: [avg_ask, avg_bid]
      using: MetricsDB

    market_depth_metrics:
      by metric family: marketDepth
      by key: symbol
      all attributes: true
      using: MetricsDB
    
    trading_triggers:
      by metric family: tradingTriggers
      by key: symbol
      as object: true
      using: MetricsDB
```

### Attributes schema

To direct the enricher to use the computed in-memory metrics database two attributes must be set.

<table><thead><tr><th width="190">Attribute</th><th width="392">Description</th><th width="174">Data Type</th></tr></thead><tbody><tr><td>by metric family</td><td>Metrics family to query against. This must be defined in the use case</td><td>String</td></tr><tr><td>using: MetricsDB</td><td>Directs the processor to bind to the metrics engine</td><td>String</td></tr></tbody></table>
