---
icon: eye
description: >-
  Joule uses JMX and Metrics API to make processes observable and to enable
  monitoring
---

# Observability

## Overview

Joule offers robust observability features designed for monitoring each component in real-time.

This system-wide observability is achieved through JMX (Java Management Extensions) and a Metrics API, enabling users to track events, processing status and data movement across processors, connectors and storage.

By leveraging a standard monitoring pattern, JMX beans offer visibility into the state of all active processes.

Below is an example configuration for enabling JMX beans, illustrating how Joule’s system architecture exposes these metrics.

<figure><img src="../../.gitbook/assets/monitoring.png" alt=""><figcaption><p>JMX beans UI</p></figcaption></figure>

## Key features of observability in Joule

1. <mark style="color:green;">**JMX integration**</mark>\
   Enables remote monitoring and management of processes.
2. <mark style="color:green;">**Metrics API**</mark>\
   Provides real-time counters for events processed, failed, discarded and more.
3. <mark style="color:green;">**Automated Monitoring**</mark>\
   Each component (data pipelines, processor, connectors, storage) has default metrics enabled, facilitating quick access to performance insights.
4. <mark style="color:green;">**Configurable**</mark>\
   Users can customize monitoring parameters for tailored observability.

### Metrics

The following metrics are enabled by default and are accessible via JMX beans:

* <mark style="color:green;">**Received**</mark>\
  Count of events received by the component.
* <mark style="color:green;">**Processed**</mark>\
  Number of events successfully processed.
* <mark style="color:green;">**Failed**</mark>\
  Events that failed during processing.
* <mark style="color:green;">**Discarded**</mark>\
  Events discarded based on specified rules.
* <mark style="color:green;">**Ignored**</mark>\
  Events ignored by the system.
* <mark style="color:green;">**Average processing latency**</mark>\
  Measures the average time taken to process events.

## Activate observability and monitoring

<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Enabling JMX for Joule</strong></mark></td><td>Activate JMX monitoring for Joule with configurable settings</td><td></td><td><a href="enabling-jmx-for-joule.md">enabling-jmx-for-joule.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Meters</strong></mark></td><td>Automated metrics monitor for Joule pipelines: data pipelines, processors, transport, storage</td><td></td><td><a href="meters.md">meters.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Metrics API</strong></mark></td><td>Tracks component events and data flow with counters</td><td></td><td><a href="metrics-api.md">metrics-api.md</a></td></tr></tbody></table>

## Additional information

* Learn about the [JMX technology](https://www.oracle.com/technical-resources/articles/javase/jmx.html)
* Learn about how to use the [monitoring and management using JMX technology](https://docs.oracle.com/javase/8/docs/technotes/guides/management/agent.html)
