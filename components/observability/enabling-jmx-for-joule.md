---
description: Activate JMX monitoring for Joule with configurable settings
---

# Enabling JMX for Joule

## Overview

This article explains how to enable JMX for monitoring the Joule process.

Various monitoring tools compatible with JMX are recommended at the end of the article for enhancing the user experience.

## Enabling JMX

**JMX is off by default** due to runtime overhead, but can be activated by **applying specific configurations** to the Joule process execution path.

The following snippet shows this configuration and will enable tools to directly connect to the Joule process:

```bash
-Dcom.sun.management.jmxremote.port=5000
-Dcom.sun.management.jmxremote.rmi.port=5000
-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5001
```

As a baseline example, the use case projects provide a Joule start script which contain JMX setting to enable process monitoring.

This can be found with the `bin/startJoule.sh` file.

```bash
JMX_CONFIG="-Dcom.sun.management.jmxremote.port=${JOULE_JMX_PORT} -Dcom.sun.management.jmxremote.rmi.port=${JOULE_JMX_PORT} -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=${JOULE_HOST_NAME} -Dcom.sun.management.jmxremote.local.only=false"
JVM_DEBUG="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:${JOULE_JMX_DEBUG_PORT}"

nohup java ${JVM_SWITCHES} ${JMX_CONFIG} ${JVM_DEBUG} -Dlog4j.configuration=file://${LOG4J_PATH} ${JOULE_MEMORY} -XX:+UseG1GC -Dpolyglot.engine.WarnInterpreterOnly=false -cp ${CLASSPATH}  com.fractalworks.streams.joule.JouleCLI -c ${JOULE_CONFIG_PATH} -s ${SOURCEFILE} -r ${REFERENCEDATA}  -u ${ENGINEFILE} -p ${PUBLISHFILE} &
```

{% hint style="info" %}
Learn about how to use the [monitoring and management using JMX technology](https://docs.oracle.com/javase/8/docs/technotes/guides/management/agent.html)
{% endhint %}

## Tool recommendation

Any monitor tools that supports JMX can be used. We recommend some of these tools:

* [JConsole](https://docs.oracle.com/javase/8/docs/technotes/guides/management/jconsole.html)
* [JDK Mission Control](https://docs.oracle.com/en/java/java-components/jdk-mission-control/index.html)
* [VisualVM](https://visualvm.github.io/)
* [dynatrace](https://www.dynatrace.com/)

{% hint style="info" %}
Further tools can be researched through [BetterStack](https://betterstack.com/community/comparisons/?q=observability)
{% endhint %}
