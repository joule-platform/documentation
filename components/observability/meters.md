---
description: >-
  Automated metrics monitor for Joule pipelines: data pipelines, processors,
  transport, storage
---

# Meters

## Overview

This page describes the automatic monitoring metrics available for different components within the Joule system, focusing on:

1. Data pipelines
2. Processors
3. Transport
4. Storage

Each component has specific metrics enabled by default, allowing users to **monitor performance and resource usage** effectively.

## Data pipelines

{% content-ref url="../pipelines/" %}
[pipelines](../pipelines/)
{% endcontent-ref %}

This monitor tracks the flow of events through the processing pipeline, providing insight into the volume of events processed. It helps ensure that **events are moving through the pipeline** efficiently and without delays.

<table><thead><tr><th width="183">Type</th><th>Description</th></tr></thead><tbody><tr><td>Queued</td><td>Number of events queued for processing</td></tr><tr><td>Processed</td><td>Number of events successfully processed</td></tr><tr><td>Failed</td><td>Number of failed events not processed by the consuming / publishing transport</td></tr></tbody></table>

## Processor&#x20;

{% content-ref url="../processors/" %}
[processors](../processors/)
{% endcontent-ref %}

Every processor has a set of metrics turned on by default. If a processor is assigned a symbolic name in the DSL configuration, a bean is created with that name for **easier identification and monitoring**.

<table><thead><tr><th width="183">Type</th><th>Description</th></tr></thead><tbody><tr><td>Received</td><td>Number of events received by the processor before being queued</td></tr><tr><td>Queued</td><td>Number of events queued ready for processing</td></tr><tr><td>Processed</td><td>Number of events successfully processed</td></tr><tr><td>Discarded</td><td>Number of events discarded, dropped, from the processor which is removed from the processing pipeline</td></tr><tr><td>Ignored</td><td>Number of events ignored but not dropped from the processing pipeline</td></tr><tr><td>Failed</td><td>Number of failed events due to a processing failure</td></tr></tbody></table>

## [Connector](../connectors/)

{% content-ref url="../connectors/" %}
[connectors](../connectors/)
{% endcontent-ref %}

Every transport has these metrics turned on by default. If a symbolic name is provided in the DSL configuration, a corresponding bean is created, allowing users to **monitor each transport** component distinctly.

<table><thead><tr><th width="183">Type</th><th>Description</th></tr></thead><tbody><tr><td>Queued</td><td>Number of events queued ready for processing</td></tr><tr><td>Processed</td><td>Number of events successfully processed</td></tr><tr><td>Failed</td><td>Number of failed events not processed by the consuming / publishing transport</td></tr></tbody></table>

## Storage

This set of metrics helps monitor the I/O (input/output) load on connected data storage systems, providing insights into **storage performance and resource usage**. Like other components, a symbolic name in the DSL configuration will create a bean for easier monitoring.

<table><thead><tr><th width="183">Type</th><th>Description</th></tr></thead><tbody><tr><td>Reads</td><td>Number of reads performed on the store</td></tr><tr><td>Writes</td><td>Number of writes performed against the store</td></tr></tbody></table>
