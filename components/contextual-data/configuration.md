---
description: Set up a contextual data-driven use case
---

# Configuration

## Overview

Joule’s contextual data management is streamlined through an **unified configuration file** that defines all relevant data sources, ensuring consistency and applicability across all deployed use cases.

The configuration specifies one or more contextual data sources, with current support for:

1. <mark style="color:green;">**Disk access**</mark>\
   [MinIO](minio-s3.md) for S3 object storage.
2. <mark style="color:green;">**Memory access**</mark>\
   [Apache Geode](apache-geode.md) as a high-performance cached data source.

{% hint style="info" %}
This file needs to be deployed before any use case deployments
{% endhint %}

## Example & DSL attributes

The following is an example configuration for integrating Apache Geode with Joule as an external data source. For detailed guidance on leveraging this robust enterprise feature, refer to the [Geode connector documentation](apache-geode.md).

This configuration specifies key data locations for `nasdaqIndexCompanies` and `holidays`. Although these data points may not change frequently, they may have high read demands, making them ideal candidates for hosting within **Joule’s in-memory space**.

```yaml
contextual data:
  name: banking market data 
  data sources:
    - geode stores:
        name: us markets
        connection:
          locator address: 192.168.86.39
          locator port: 41111
        stores:
          nasdaqIndexCompanies:
            region: nasdaq-companies
            keyClass : java.lang.String
            gii: true
          holidays:
            region: us-holidays
            keyClass : java.lang.Integer
```

### Stores explanation

A key element of the configuration’s DSL is the `stores` syntax. In essence, the `stores` definition binds a logical store name to a specific implementation, with properties tailored to the underlying technology. For instance, an S3 implementation will have different configuration properties compared to a distributed caching solution.

This can be better illustrated through an example where company information needs to be added to each event, and an ML model requires updates during the Joule process’s runtime. In this case, the configurations for `nasdaqIndexCompanies` and `predictors` differ, reflecting their distinct underlying technologies and usage patterns.&#x20;

```yaml
contextual data:
  name: banking market data 
  data sources:
    - geode stores:
        ...
        stores:
          nasdaqIndexCompanies:
            region: nasdaq-companies
            keyClass : java.lang.String
            gii: true
            
    - minio stores:
        ...
        stores:
          predictors:
            bucketId: models
            initial version Id: 12345
            download dir: /home/joule/nasdaq-companies-model/tmp
```

### Enrichment example

In Joule, contextual data is applied to events through the enricher processor, which attaches the relevant contextual data objects to each `StreamEvent`.

{% hint style="info" %}
Read the [enrichment](../processors/enrichment/) document for full feature information and further examples
{% endhint %}

The example below enriches the event's `companyInformation` attribute using the `nasdaqCompanies` contextual data, by performing a lookup based on the `symbol` key to retrieve relevant company data.

```yaml
enricher:
  fields:
    companyInformation:
      key: symbol
      using: nasdaqCompanies
      
    stores:
       nasdaqCompanies:
          store name: nasdaqIndexCompanies
```

### Attributes schema

<table><thead><tr><th width="171">Attribute</th><th width="280">Description</th><th width="188">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Contextual data store namespace</td><td>String</td><td>true</td></tr><tr><td>data sources </td><td>List of data sources to connect and bind in to the Joule processor</td><td>List of connector configurations</td><td>true</td></tr></tbody></table>
