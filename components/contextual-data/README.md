---
icon: head-side-virus
description: Enrich real-time streams with contextual data
---

# Contextual data

## Overview

For Joule applications, contextual (or reference) data is crucial for enabling advanced and insightful stream processing. By seamlessly **integrating contextual data with real-time events**, the system delivers enriched processing outcomes and better informed insights.

Within Joule, contextual data comprises both **static and dynamic data:**

1. <mark style="color:green;">**Static data**</mark>\
   **I**ncludes reference information, such as customer contracts, product SKUs, or start-of-day FX rates, machine learning predictive models etc.
2. <mark style="color:green;">**Dynamic data**</mark>\
   Consists of real-time metrics that drive KPI computations, machine learning (ML) engineered features, analytics, and other performance-based measures.

Some example applications include:&#x20;

1. <mark style="color:green;">**Geospatial location analytics**</mark>\
   Leveraging well-known areas of interest for real-time geospatial insights.
2. <mark style="color:green;">**Real-time customer promotions**</mark>\
   Tailored promotions based on historical buying profiles and real-time customer behaviour.
3. <mark style="color:green;">**Multi-currency dynamic pricing**</mark>\
   Adapting pricing strategies based on live exchange rates and market conditions.
4. <mark style="color:green;">**Real-time spending alerts**</mark>\
   Powered by pre-engineered ML features to track and alert users of unusual spending patterns.
5. <mark style="color:green;">**Predictive capacity demand forecasting**</mark>\
   Using live metrics to forecast future resource or capacity needs with high accuracy.

These capabilities enable Joule to not only process data in real time but also drive actionable insights that influence business decisions.

## Key features of Joule's contextual data capability

1. <mark style="color:green;">**Event enrichment**</mark>\
   Adds static information (i.e., geographic or device data) to live events for richer analysis.
2. <mark style="color:green;">**Transformation and validation**</mark>\
   Transforms and validates event attributes using predefined patterns or rules.
3. <mark style="color:green;">**Real-time predictive analytics**</mark>\
   Utilises engineered features for machine learning models.
4. <mark style="color:green;">**Machine Learning models**</mark>\
   Load and replace machine learning models&#x20;
5. <mark style="color:green;">**KPI computation**</mark>\
   Generates real-time performance indicators based on enriched event data.
6. <mark style="color:green;">**Local caching**</mark>\
   Contextual data is treated as "slow-moving" and often cached locally within processing nodes to optimise performance.

## Joule's contextual data in action

Joule **provides every processor with implementation interfaces** to access and apply contextual data within its processing scope.&#x20;

&#x20;The selection of a contextual data store depends on the specific access and usage requirements of each processor. For scenarios involving high-throughput event processing, an integrated, enterprise-grade, low-latency caching solution is available. For processors where models, rules, or mathematical constants require periodic updates, the MinIO S3-compatible solution is provided.

{% hint style="info" %}
Contextual data is treated differently to event driven data, it is considered slow-moving
{% endhint %}

For further information on how Joule integrates contextual data review the following documents:

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Architecture</strong></mark></td><td>Understand how contextual data integrates within Joule</td><td></td><td><a href="architecture.md">architecture.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Configuration</strong></mark></td><td>Set up a contextual data-driven use case</td><td></td><td><a href="configuration.md">configuration.md</a></td></tr></tbody></table>

## Types of source implementations

Joule integrates with various data sources to support contextual data within stream processing, these are:

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Minio S3</strong></mark></td><td>Multi-cloud S3-compatible interface for static contextual data (e.g., ML models)</td><td></td><td><a href="minio-s3.md">minio-s3.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Apache Geode</strong></mark></td><td>High-performance caching platform for static and slow-moving contextual data</td><td></td><td><a href="apache-geode.md">apache-geode.md</a></td></tr></tbody></table>
