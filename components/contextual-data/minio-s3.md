---
description: Multi-cloud S3-compatible interface for static contextual data and ML models
---

# MinIO S3

## Overview

The MinIO S3 driver in Joule provides a flexible storage solution for integrating static contextual data within a streaming context.

This storage type is ideal for **very slow-changing data**, which can be cached and used for enriching events without frequent updates.

With S3-compatible storage, developers can leverage existing data assets, making it easy to add value to real-time streaming applications.

{% hint style="info" %}
[Driver details io.minio:minio:8.5.4](https://min.io/)
{% endhint %}

## Use cases

The MinIO S3 storage is well-suited for various forms of static data, including industry standards, organisational contextual data and pre-computed models.

Typical use cases include:

1. <mark style="color:green;">**Geographic and regional data**</mark>\
   Postal codes, country codes (e.g., ISO-366).
2. <mark style="color:green;">**Device and market data**</mark>\
   Mobile manufacture models, market exchange codes.
3. <mark style="color:green;">**Currency data**</mark>\
   Currency codes for international transactions.
4. <mark style="color:green;">**Machine learning models**</mark>\
   Host and deploy pre-trained models in to Joule for real-time predictions.
5. <mark style="color:green;">**Static variables and codes**</mark>\
   Charge codes, car VINs, pre-computed data for analysis.
6. <mark style="color:green;">**Pattern matching**</mark>\
   Regex patterns for validating inputs, such as phone numbers.

## Example & DSL attributes

The following example is a sample configuration that uses the MinIO S3 connector to **load a machine learning model** on startup.

This setup leverages specific credentials and connection details to access and download a model from a defined bucket, ensuring that it’s available in the processing pipeline.

```yaml
contextual data:
 ...
    - minio stores:
        name: Telco ML Models
        connection:
          endpoint: "https://localhost"
          port: 9000
          tls: false
          credentials:
            access key: "XXXXXXX"
            secret key: "YYYYYYYYYYYYYYY"
        stores:
          predictors:
            bucketId: models
            initial version Id: 12345
            download dir: /home/joule/telco-models/tmp
```

### **Explanation**

This configuration specifies an **S3 bucket** (`models`) as the data source for machine learning models.

On startup, the model with the defined version (`12345`) is downloaded to the local directory (`/home/joule/telco-models/tmp`).

The `pmml predictor processor` reference implementation is then used to apply the model within Joule’s processing framework.

For further details on configuring this processor, refer to the [PMML Predictor Documentation](../analytics/ml-inferencing/online-predictive-analytics.md).

### Attributes schema

<table><thead><tr><th width="171">Attribute</th><th width="280">Description</th><th width="188">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Logical name of contextual data set for given target connection</td><td>String</td><td>true</td></tr><tr><td>connection </td><td>Connection details</td><td><a href="../connectors/sources/minio-s3.md#connection-attributes">See Connection configuration</a></td><td>true</td></tr><tr><td>stores</td><td>Map of logical names to bucket level configurations. <a href="minio-s3.md#bucket-attributes">See bucket attributes section</a></td><td>Map</td><td>true</td></tr></tbody></table>

### Bucket Attributes

<table><thead><tr><th width="171">Attribute</th><th width="280">Description</th><th width="188">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>bucketId</td><td>S3 bucket where the reference data resides </td><td>String</td><td>true</td></tr><tr><td>initial version id</td><td>The version id to use when priming the system. This is a global version for the given bucket otherwise the latest version of the object is used</td><td>String</td><td>false</td></tr><tr><td>download dir</td><td>Local path for bucket objects to be downloaded before processing starts </td><td>String<br>Default: ./tmp</td><td>false</td></tr></tbody></table>
