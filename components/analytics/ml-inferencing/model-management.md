---
description: Deploy a retrained model directly in to a running Joule with zero down time
---

# Model management

## Objective

The predictor processor can be configured to **load and refresh** a model from a linked reference data storage platform.

When this optional setting is applied the relevant logic to switch the model in-place is activated and a model file change process occurs. On a model update the predictor processor execution is paused while the model is replaced.&#x20;

Any model replacement can be trigger by external drift analysis by using the [model audit](model-audit.md) function.

<figure><img src="../../../.gitbook/assets/model-management.png" alt=""><figcaption><p>Example S3 integration for model management</p></figcaption></figure>

## Explanation on what is going on

Although the configuration may look confusing it due to the ability for every processor to be able to reuse reference data store while being able to gain flexible approach to building capabilities.&#x20;

To ensure the PMML processor operates with reliable models, it must connect to a model store, allowing for model replacement when necessary.

The key configuration items are:

1. Defines which `stores` to include. PMML only requires the model store.
2. Set the model store to the `production_churn_models.`
3. One processor initialisation the processor will configure file watchers on the store to trigger model refreshes.

## Example

{% hint style="info" %}
For more details or assistance, [Fractalworks](https://www.fractalworks.io/contact) is ready to support in your enquiries
{% endhint %}

The example below demonstrates how to link to model store.

```yaml
pmml predictor:
  name: customer_churn_predictor
  model: churn/customer_churn_rf.pmml
  response field: churn_prediction
  
  # Link to the store for this model
  model store: production_churn_models
  
  audit configuration:
    ...
  
  # 1. Define interested stores to bind too
  stores:
    # Model store
    production_churn_models:
      store name: customer_churn_models
    
    # Customer spend profiles
    production_customer_profiles:
      store name: customer_spend_profiles
```

### Storage definition

For this example to work the below reference data definition would need to be deployed so that the processor can successfully bind to the underlying storage platform.

See [Reference Data API](../../../developer-guides/api-endpoints/mangement-api/contextual-data.md) documentation for further details.

```yaml
reference data:
  name: predictiveModelStores
  data stores:
    - minio stores:
        name: production_models
        connection:
          endpoint: "https://localhost"
          port: 9000
          tls: false
          credentials:
            access key: "AKIAIOSFODNN7"
            secret key: "wJalrXUtnFEMIK7MDENGbPxRfiCY"
        
        stores:
          customer_churn_models:
            bucketId: churn_models
          customer_spend_profiles:
            bucketId: customer_historical_spend          
```
