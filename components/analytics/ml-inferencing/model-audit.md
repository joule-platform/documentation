---
description: >-
  Support explainability, detect drift, and manage retraining through model
  auditing
---

# Model audit

## Objective

This optional configuration provide the **ability to audit predications** to enable model retraining, feature and prediction [drift management](https://www.ibm.com/topics/model-drift), model observability and any local business governance requirements.&#x20;

The configuration will dynamically **create an in-memory database table**, using the process name as the target table and rest endpoints to enable direct access and export functions.

{% hint style="info" %}
Use this feature to support external drift monitoring and model retraining
{% endhint %}

## Example & DSL attributes

{% hint style="info" %}
For more details or assistance, [Fractalworks](https://www.fractalworks.io/contact) is ready to support in your enquiries
{% endhint %}

This configuration enables the auditing of model predictions for monitoring and retraining purposes.

The `pmml predictor` is configured to use a specific model: `iris_rf.pmml` and the `audit configuration` sets up an in-memory database to store audit data such as predictions.

The schema is set to `ml_audit`, with a queue capacity of 5000 and a flush frequency of 300 seconds, allowing for real-time tracking of prediction performance and potential model drift.

### Example

```yaml
pmml predictor:
  name: irisScorer
  model: ./models/iris_rf.pmml
  response field: flowerPrediction
  
  audit configuration:
    target schema: ml_audit
    queue capacity: 5000
    flush frequency: 300
```

### Attributes schema

<table><thead><tr><th width="225">Attribute</th><th width="314">Description</th><th width="137">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>target schema</td><td>name of the target schema where the table will be created</td><td>String</td><td>true</td></tr><tr><td>queue capacity</td><td>Number of prediction results to queue before flushing to target table</td><td>Integer<br>Default: 1000</td><td>false</td></tr><tr><td>flush frequency</td><td>Frequency the queue will be flushed to table as seconds</td><td>Long<br>Default: 60 Seconds</td><td>false</td></tr></tbody></table>
