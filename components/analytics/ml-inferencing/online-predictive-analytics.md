---
description: JPMML prediction processor evaluates event feature vectors in near-real-time
---

# Online predictive analytics

## Objective

Provide streaming predictions and scoring using a standard PMML online predictor implementation.&#x20;

{% hint style="info" %}
The implementation leverages the JPMML open source library developed by [Villu Ruusmann](https://github.com/vruusmann)
{% endhint %}

## Example & DSL attributes

{% hint style="info" %}
For more details or assistance, [Fractalworks](https://www.fractalworks.io/contact) is ready to support in your enquiries
{% endhint %}

The example below demonstrates the ease at which a machine learning model can be embedded within a stream processing pipeline and have events scored.

### Model preparation

On Joule startup the following is performed before events are processed:

1. Model is loaded in to memory from disk.
2. The required feature vector attributes are derived from the model ready to be retrieved from the presented event.
3. A response field is configured.

### Example

```yaml
pmml predictor:
  name: irisScorer
  model: ./models/iris_rf.pmml
  response field: flowerPrediction
```

### Model scoring

Events are scored in near real-time using this process:&#x20;

1. Extract the required feature vector attributes from the event.
2. Execute the model inferencing function using the prepared feature vector.
3. Place the resulting score in to the event assigned to the `response field`.

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="299">Description</th><th width="144">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>name of the counter.</td><td>String</td><td>true</td></tr><tr><td>model</td><td>model filename that specifies  path and model filename.</td><td>String</td><td>true</td></tr><tr><td>model store</td><td>Reference data logical store name where the model resides. See <a href="../../contextual-data/">Contextual data documentation</a> for further information on configuration</td><td>String</td><td>false</td></tr><tr><td>features field</td><td>Field where all the required features exist for the model prediction function. This is an optional field where engineered map of features are placed and applied to the model evaluation. See the <a href="feature-engineering/">Feature Engineering</a> documentation for further details</td><td>String</td><td>false</td></tr><tr><td>response field</td><td>Name of field where the result of the prediction function will be assigned too</td><td>String</td><td>false</td></tr><tr><td>unpack results</td><td>Unpack the algorithm response variables directly into the <code>StreamEvent</code> object. Otherwise the process will add the complete response object in to the response field</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>audit configuration</td><td>Every prediction can be audited along with the features used.</td><td>See <a href="model-audit.md">model audit</a>  page </td><td>false</td></tr></tbody></table>
