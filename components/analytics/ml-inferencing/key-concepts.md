---
hidden: true
---

# Key concepts

## What we will learn on this article?



## Online stream prediction architecture

The Joule architecture **seamlessly integrates core features** such as feature engineering, prediction, model auditing and model management.

The following example showcases a predictive use case, leveraging the robust features that Joule brings to the forefront.

<figure><img src="../../../.gitbook/assets/prediction-arch.jpg" alt=""><figcaption><p>Joule Prediction architecture </p></figcaption></figure>





### Architecture components
