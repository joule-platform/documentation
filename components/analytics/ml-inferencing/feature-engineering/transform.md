---
description: Generate analytics-ready features from data
---

# Transform

## Objective

Feature engineering prepares raw data for analysis by creating new, insightful features:

1. [<mark style="color:green;">**Log transform**</mark>](transform.md#log-transform)\
   Applies a log function to positive values, commonly used to handle skewed data.
2. [<mark style="color:green;">**Day of week transform**</mark>](transform.md#day-of-week-transform)\
   Converts a date to its day of the week as a number (1-7).
3. [<mark style="color:green;">**Day binning**</mark>](transform.md#day-binning)\
   Categorises a date as a weekday (1) or weekend (2).
4. [<mark style="color:green;">**Age binning**</mark>](transform.md#age-binning)\
   Categorises ages into specified age ranges for easier analysis.

Each method produces targeted features, simplifying data for analytics.

## Log transform

Log transformation is a data transformation method in which it replaces each variable x with a log(x) where x is a positive number and greater than zero

### Example

```yaml
feature engineering:
  ...
  features:
    compute:
      log_spend:
        function:
          log transform:
            source field: spend
```

### Attributes schema

<table><thead><tr><th width="176">Attribute</th><th width="301">Description</th><th width="130">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>source field</td><td>The  column to perform the calculation upon</td><td>Double</td><td>true</td></tr></tbody></table>

## Day of week transform

Provide the day of week from the passed date object to a number between 1 and 7, where start of week is Monday = 1.

Supported date objects:

1. `java.time.LocalDate`
2. `java.sql.Date`
3. `org.joda.time.DateTime`

### Example

```yaml
feature engineering:
  ...
  features:
    compute:
      day_of_week:
        function:
          day-of-week transform:
            source field: date
```

### Attributes schema

<table><thead><tr><th width="176">Attribute</th><th width="301">Description</th><th width="130">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>source field</td><td>The  column to perform the calculation upon</td><td>Double</td><td>true</td></tr></tbody></table>

## Day binning

Categorise a day into one of two categories following the Gregorian calendar.

1. Weekday (Mon-Fri) = 1
2. Weekends (Sat-Sun) = 2

### Example

```yaml
feature engineering:
  ...
  features:
    compute:
      day_bin:
        function:
          day binning:
            source field: date
```

### Attributes schema

<table><thead><tr><th width="176">Attribute</th><th width="301">Description</th><th width="130">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>source field</td><td>The  column to perform the calculation upon</td><td>Double</td><td>true</td></tr></tbody></table>

## Age binning

Categorise a passed age in a pre-configured age bin as either an integer or date object.

### Example

```yaml
feature engineering:
  ...
  features:
    compute:
        age_bin:
          function:
            age binning:
              bins: [ [0,18], [19,21], [22, 40], [41, 55], [56,76]]
              base date: 2023-01-01
              source field: current_age
```

### Attributes schema

<table><thead><tr><th width="137">Attribute</th><th width="301">Description</th><th width="211">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>bins</td><td>Array of age bins to use. Default bins are set to 0-9, 10-19,...110-119</td><td>Int[][]</td><td>false</td></tr><tr><td>as date</td><td><p>Passed event field is a supported date object</p><p></p><p>Supported Data classes:</p><ul><li><code>java.time.LocalDate</code></li><li><code>java.sql.Date</code></li></ul></td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>base date</td><td>Provide a date which is used to calculate the age. Default set to the date process is started</td><td><p>String</p><p>Format: YYYY-MM-DD</p></td><td>false</td></tr><tr><td>source field</td><td>The  column to perform the calculation upon</td><td>Double</td><td>true</td></tr></tbody></table>
