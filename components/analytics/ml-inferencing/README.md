---
icon: microchip-ai
description: Leverage streaming online predictions to drive insights to action
---

# ML inferencing

## Overview

For cutting-edge stream-based use cases, the incorporation of machine learning models is integral for next best action processing.

Out-of-the-box, Joule offers the capability to seamlessly deploy [JPMML](https://github.com/jpmml) models within a processing pipeline using a predictive processor.

Learn more about Joule's [ML inferencing architecture here](key-concepts.md).

{% hint style="success" %}
Joule provides the capability to perform **near-real-time predictions** to enable best next action use cases.
{% endhint %}

## Use cases

1. Online customer segmentation
2. Predictive service maintenance
3. Behaviour anomaly detection
4. Best next action
5. Customer conversation scoring

## Available ML inferencing options

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Online predictive analytics</strong></mark></td><td>JPMML prediction processor evaluates event feature vectors in near-real-time </td><td></td><td><a href="online-predictive-analytics.md">online-predictive-analytics.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Feature engineering</strong></mark></td><td>Decorate a feature vector with enriched features specific to the deployed model</td><td></td><td><a href="feature-engineering/">feature-engineering</a></td></tr><tr><td><mark style="color:orange;"><strong>Model audit</strong></mark></td><td>Monitor model predictions and performance metrics to support explainability, detect drift, and manage retraining</td><td></td><td><a href="model-audit.md">model-audit.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Model management</strong></mark></td><td>Deploy a retrained model directly in to a running Joule with zero down time</td><td></td><td><a href="model-management.md">model-management.md</a></td></tr></tbody></table>
