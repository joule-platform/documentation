---
description: >-
  Define math expressions or provide as a file using Joule supported languages
  and APIs
icon: trowel-bricks
---

# Analytic tools

## Overview

With Joule's streaming analytics tools, organisations can leverage real-time data to drive operational efficiency, optimise resource allocation and uncover valuable insights for informed decision-making.&#x20;

The platform's flexibility and ease of use empower users to tackle diverse analytical challenges and extract maximum value from their streaming data sources.&#x20;

To this end Joule provides a suite of analytic tools to perform real-time insights from streaming events.&#x20;

## Use cases

Joule's Analytics features provide you to the ability to define **various forms of custom calculations** that can drive facilitating the implementation of specific use cases on your stream events.

* Customer churn prediction
* Customer experience enhancement
* IoT predictive maintenance
* Geospatial marketing analytics

## Available analytical features

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>User defined analytics</strong></mark></td><td>Define math expressions or provide as a file using Joule supported languages </td><td></td><td><a href="user-defined-analytics/">user-defined-analytics</a></td></tr><tr><td><mark style="color:orange;"><strong>Window analytics</strong></mark></td><td>Standard window functions are provided to perform event based analytics</td><td></td><td><a href="window-analytics/">window-analytics</a></td></tr><tr><td><mark style="color:orange;"><strong>Analytic functions</strong></mark></td><td>Develop custom analytic functions using the provided Analytics API's</td><td></td><td><a href="analytic-functions/">analytic-functions</a></td></tr><tr><td><mark style="color:orange;"><strong>Advanced analytics</strong></mark></td><td>Advanced analytic processors such as geospatial tracking</td><td></td><td><a href="advanced-analytics/">advanced-analytics</a></td></tr></tbody></table>

