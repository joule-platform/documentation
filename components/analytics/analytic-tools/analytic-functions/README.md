---
description: Analytic functions perform complex calculation over a window of grouped events
---

# Analytic functions

## Objective

Analytic functions are used when calculations over an event window is required. These functions are executed on each window generation trigger, windows are generated when the window policy is satisfied.&#x20;

Joule is shipped with a starter pack of stateful and stateless functions to support initial use cases. The platform can be extended using the [Analytics API](../../../../developer-guides/builder-sdk/analytics-api/) to build custom functions.

## Example

This code defines a **sliding time window** for calculating and emitting additional attributes. On `emit` of data, `EMA` is appended on the property name automatically.

1. <mark style="color:green;">**window functions**</mark>
   1. <mark style="color:green;">**ema rates**</mark>\
      Calculates the exponential moving average (EMA) for `ask` and `bid` with a smoothing factor of 0.001996007984032.
   2. <mark style="color:green;">**ranges**</mark>\
      Calculates the min-max range for `ask` and `bid`.
   3. <mark style="color:green;">**norms**</mark>\
      Computes the Z-score for `ask` and `bid`.
2. <mark style="color:green;">**policy**</mark>
   1. <mark style="color:green;">**slidingTime**</mark>\
      The window slides every 500 milliseconds.
   2. <mark style="color:green;">**window size**</mark>\
      The window covers 2500 milliseconds (2.5 seconds).

This configuration calculates attributes like `ask_EMA`, `bid_EMA`, `ask_MINMAX_NORM`, `bid_MINMAX_NORM`, `ask_ZSCORE`, and `bid_ZSCORE` over a sliding time window.

```yaml
time window:
  emitting type: slidingQuoteAnalytics
  window functions:
    ema rates:
      function:
        exponential moving average:
          parameters:
            smoothing factor: 0.001996007984032
      attributes: [ ask,bid ]

    ranges:
      function:
        minmax: {}
      attributes: [ ask,bid ]

    norms:
      function:
        zscore: {}
      attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

## Available options

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th><select></select></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Stateful analytics</strong></mark></td><td>Stateless based analytics over a grouped set of events</td><td></td><td></td><td><a href="stateless/">stateless</a></td></tr><tr><td><mark style="color:orange;"><strong>Stateless analytics</strong></mark></td><td>Stateful based analytics over a grouped set of events</td><td></td><td></td><td><a href="stateless/">stateless</a></td></tr></tbody></table>
