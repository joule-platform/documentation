---
description: Stateful based analytics over a grouped set of events
---

# Stateful

> Stateful math functions refer to mathematical operations or transformations that maintain some form of internal state across invocations

## Available functions

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td></td><td><mark style="color:orange;"><strong>Exponential moving average</strong></mark></td><td></td><td><a href="exponential-moving-average.md">exponential-moving-average.md</a></td></tr><tr><td></td><td><mark style="color:orange;"><strong>Rolling sum</strong></mark></td><td></td><td><a href="rolling-sum.md">rolling-sum.md</a></td></tr></tbody></table>
