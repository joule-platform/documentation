# Rolling Sum

## Objective

Calculate the rolling sum of values across provided attributes.

## Example

```yaml
time window:
  emitting type: slidingRollingSum
    window fucntions:
    rolling volume:
      function:
        rolling sum: {}
      attributes: [ volume ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `ROLLING_SUM`

| Before processing | After processing     | Type         |
| ----------------- | -------------------- | ------------ |
| volume            | volume\_ROLLING\_SUM | double value |
