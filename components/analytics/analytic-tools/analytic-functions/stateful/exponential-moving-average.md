# Exponential moving average

## Objective

Exponential smoothing or exponential moving average is a rule of thumb technique for smoothing time series data using the exponential window function. Whereas in the simple moving average the past observations are weighted equally, exponential functions are used to assign exponentially decreasing weights over time.

## Example

```yaml
time window:
  emitting type: priceEMAAnalytics
  window functions:
  ema rates:
    function:
      exponential moving average:
        parameters:
          smoothing factor: 0.001996007984032
    attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `EMA`

| Before processing | After processing | Type         |
| ----------------- | ---------------- | ------------ |
| ask               | ask\_EMA         | double value |
| bid               | bid\_EMA         | double value |

### Parameters

<table><thead><tr><th width="186">Parameter</th><th width="341">Description</th><th>Type</th></tr></thead><tbody><tr><td>smoothing_factor</td><td>Smoothing constant to be applied within the EMA calculation<br>Use ( 2 / 1 - Time Periods) as the smoothing constant.</td><td>Double<br>Default: 0.333</td></tr></tbody></table>

