---
description: Key statistical functions and measures
---

# Statistics

> Statistics provides various methods for summarising and analysing event data within streaming environments. Operations such as moving averages, event counts, and statistical summaries help derive insights by applying functions like weighted or simple averages, counts, and robust statistics to event attributes over defined time windows.

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td></td><td><mark style="color:orange;"><strong>Statistic summaries</strong></mark></td><td></td><td><a href="statistic-summaries.md">statistic-summaries.md</a></td></tr><tr><td></td><td><mark style="color:orange;"><strong>Weighted moving average</strong></mark></td><td></td><td><a href="weighted-moving-average.md">weighted-moving-average.md</a></td></tr><tr><td></td><td><mark style="color:orange;"><strong>Simple moving average</strong></mark></td><td></td><td><a href="simple-moving-average.md">simple-moving-average.md</a></td></tr><tr><td></td><td><mark style="color:orange;"><strong>Count</strong></mark></td><td></td><td><a href="count.md">count.md</a></td></tr></tbody></table>
