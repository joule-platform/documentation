# Statistic summaries

## Objective

Provide a statistics summary of the provided event attribute&#x20;

## Example

```yaml
time window:
  emitting type: slidingStatisticSummaries
  window functions:
    stats summary:
        function:
           statistics summary: {}
        attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `STATS`

| Before processing | After processing | Type          |
| ----------------- | ---------------- | ------------- |
| ask               | ask\_STATS       | double values |
| bid               | bid\_STATS       | double values |

### Generated statistics

<table><thead><tr><th width="272">Type</th><th>Description</th></tr></thead><tbody><tr><td>SUM</td><td>Sum of field values</td></tr><tr><td>MIN</td><td>Min of field value</td></tr><tr><td>MAX</td><td>Max of field value</td></tr><tr><td>MEAN</td><td>Mean of field value</td></tr><tr><td>VARIANCE</td><td>Variance of field value</td></tr><tr><td>STDEV</td><td>Standard deviation of field value</td></tr><tr><td>FIRST</td><td>First field value in window</td></tr><tr><td>LAST</td><td>Last field value in window</td></tr><tr><td>HARMONIC_MEAN</td><td>Harmonic mean of field value</td></tr><tr><td>GEOMETRIC_MEAN</td><td>Geometric mean of field value</td></tr><tr><td>PVARIANCE</td><td>Population variance of field value</td></tr><tr><td>SECOND_MOMENT</td><td>Seond moment of field value</td></tr><tr><td>SUM_SQRTS</td><td>Sum of square root of field value</td></tr><tr><td>SUM_LOGS</td><td>Sum of log of field value</td></tr></tbody></table>
