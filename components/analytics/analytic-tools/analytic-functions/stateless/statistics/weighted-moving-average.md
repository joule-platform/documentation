# Weighted moving average

## Objective

Provide a weighted moving average of the provided event attributes

## Example

```yaml
time window:
  emitting type: slidingWeightedAverages
  window functions:
    stats summary:
        function:
           weighted moving average:
             parameters:
               period: 500
        attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `WMA`

| Before processing | After processing | Type         |
| ----------------- | ---------------- | ------------ |
| ask               | ask\_WMA         | double value |
| bid               | bid\_WMA         | double value |
