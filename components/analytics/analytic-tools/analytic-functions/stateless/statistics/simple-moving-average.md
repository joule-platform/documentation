# Simple moving average

## Objective

Provide a simple moving average of the provided event attributes

## Example

```yaml
time window:
  emitting type: slidingAverages
  window functions:
    stats summary:
        function:
           simple moving average: {}
        attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `SMA`

| Before processing | After processing | Type         |
| ----------------- | ---------------- | ------------ |
| ask               | ask\_SMA         | double value |
| bid               | bid\_SMA         | double value |
