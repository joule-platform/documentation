# Count

## Objective

Provide the number of events within the window defiend by the group by defintion

## Example

```yaml
time window:
  emitting type: slidingCountPrices
  window fucntions:
    px count:
      function:
        count: {}
      attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `COUNTW`

| Before processing | After processing | Type          |
| ----------------- | ---------------- | ------------- |
| ask               | ask\_COUNTW      | integer value |
| bid               | bid\_COUNTW      | integer value |
