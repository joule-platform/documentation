# Mean

## Objective

Mean normalization is a process used in machine learning that calculates and subtracts the mean for every feature.

## Example

```yaml
time window:
  emitting type: slidingMeanNormalization
  window functions:
    feature norm:
      function:
        mean norm: {}
      attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `MEAN_NORM`

| Before processing | After processing | Data type    |
| ----------------- | ---------------- | ------------ |
| ask               | ask\_MEAN\_NORM  | double value |
| bid               | bid\_MEAN\_NORM  | double value |
