# Log

## Objective

Log normalization is a method for standardizing data that can be useful when we have features with high variance. Log normalization applies a logarithmic transformation to our values, which transforms them onto a scale that approximates normality - an assumption that many models make

## Example

```yaml
time window:
  emitting type: slidingLogNormalization
  window functions:
    feature norm:
      function:
        log norm: {}
      attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `LOG_NORM`

| Before processing | After processing | Type         |
| ----------------- | ---------------- | ------------ |
| ask               | ask\_LOG\_NORM   | double value |
| bid               | bid\_LOG\_NORM   | double value |
