---
description: Change the distribution shape of your data
---

# Normalisation

> In statistics, normalisation is the process of adjusting values that are measured on different scales to a common scale. This is often done before averaging. Normalisation can also involve dividing a count by something else to make a number more comparable.

## Available functions

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Absolute max</strong></mark></td><td></td><td></td><td><a href="absolute-max.md">absolute-max.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Min max</strong></mark></td><td></td><td></td><td><a href="min-max.md">min-max.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Standardisation</strong></mark></td><td></td><td></td><td><a href="standardisation.md">standardisation.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Mean</strong></mark></td><td></td><td></td><td><a href="mean.md">mean.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Log</strong></mark></td><td></td><td></td><td><a href="log.md">log.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Z-Score</strong></mark></td><td></td><td></td><td><a href="z-score.md">z-score.md</a></td></tr></tbody></table>
