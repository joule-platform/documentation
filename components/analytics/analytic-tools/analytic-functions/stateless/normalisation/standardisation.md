# Standardisation

## Objective&#x20;

Standardization is another Feature scaling method where the values are centered around the mean with a unit standard deviation. This means that the mean of the attribute becomes zero, and the resultant distribution has a unit standard deviation.

## Example

```yaml
time window:
  emitting type: slidingStandardNormalization
  window functions:
    normalization:
      function:
        standard norm: {}
      attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `STANDARD_NORM`

| Before processing | After processing    | Type         |
| ----------------- | ------------------- | ------------ |
| ask               | ask\_STANDARD\_NORM | double value |
| bid               | bid\_STANDARD\_NORM | double value |
