# Min max

## Objective&#x20;

Min-max normalization is one of the most common ways to normalize data. For every feature, the minimum value of that feature gets transformed into a 0, the maximum value gets transformed into a 1, and every other value gets transformed into a decimal between 0 and 1.

## Example

```yaml
time window:
  emitting type: slidingMinMaxNormalization
  window functions:
    normalization:
      function:
        minmax norm: {}
      attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `MINMAX_NORM`

| Before processing | After processing  | Type         |
| ----------------- | ----------------- | ------------ |
| ask               | ask\_MINMAX\_NORM | double value |
| bid               | bid\_MINMAX\_NORM | double value |
