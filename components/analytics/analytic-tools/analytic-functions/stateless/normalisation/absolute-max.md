# Absolute max

## Objective&#x20;

Maximum absolute scaling scales the data to its maximum value; that is, it divides every observation by the maximum value of the variable: The result of the preceding transformation is a distribution in which the values vary approximately within the range of -1 to 1.

## Example

```yaml
time window:
  emitting type: slidingAbsMaxNormalization
  window functions:
    normalization:
      function:
        absmax norm: {}
      attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `ABSMAX_NORM`

| Before processing | After processing  | Type         |
| ----------------- | ----------------- | ------------ |
| ask               | ask\_ABSMAX\_NORM | double value |
| bid               | bid\_ABSMAX\_NORM | double value |
