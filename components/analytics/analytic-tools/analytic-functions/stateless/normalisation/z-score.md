# Z-Score

## Objective

A Z-Score is a statistical measurement of a score's relationship to the mean in a group of scores. A Z-score can reveal to a trader if a value is typical for a specified data set or if it is atypical. In general, a Z-score of -3.0 to 3.0 suggests that a stock is trading within three standard deviations of its mean.

## Example

```yaml
time window:
  emitting type: slidingZScore
  window functions:
    normalization:
      function:
        zscore: {}
      attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `ZSCORE`

| Before processing | After processing | Type         |
| ----------------- | ---------------- | ------------ |
| ask               | ask\_ZSCORE      | double value |
| bid               | bid\_ZSCORE      | double value |
