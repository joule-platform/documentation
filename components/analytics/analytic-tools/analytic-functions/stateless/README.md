---
description: Stateless based analytics over a grouped set of events
---

# Stateless

> Stateless functions are functions that are not influenced by previous events or outside data and are only dependent on the data passed to them as arguments.&#x20;

## Available functions

Set of stateless analytic functions

<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Normalisation</strong></mark></td><td>Change the distribution shape of your data</td><td></td><td><a href="normalisation/">normalisation</a></td></tr><tr><td><mark style="color:orange;"><strong>Scaling</strong></mark></td><td>Change the range of your data</td><td></td><td><a href="scaling/">scaling</a></td></tr><tr><td><mark style="color:orange;"><strong>Statistics</strong></mark></td><td>Key statistical functions and measures</td><td></td><td><a href="statistics/">statistics</a></td></tr><tr><td><mark style="color:orange;"><strong>General</strong></mark></td><td>Utility and supporting analytic functions </td><td></td><td><a href="general/">general</a></td></tr></tbody></table>
