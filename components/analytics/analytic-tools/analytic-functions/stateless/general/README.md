---
description: Utility and supporting analytic functions
---

# General

> General purpose functions, among them the euclidean length computes the distance of a vector by taking the square root of the sum of its squared components. This method is often used to quantify the magnitude of multi-dimensional data points, such as attributes in event streams.

{% content-ref url="euclidean.md" %}
[euclidean.md](euclidean.md)
{% endcontent-ref %}
