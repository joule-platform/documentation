# Euclidean

## Objective

Compute the Euclidean length by the square root of the sum of the squares.

## Example

```yaml
time window:
  emitting type: slidingEuclidean
  window functions:
    vector lengths:
      function:
        euclidean length: {}
      attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `EUCL_LEN`

| Before processing | After processing | Type         |
| ----------------- | ---------------- | ------------ |
| ask               | ask\_EUCL\_LEN   | double value |
| bid               | bid\_EUCL\_LEN   | double value |
