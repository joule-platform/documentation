# Unit scale

## Objective

Every feature scaled by the Euclidean length of the passed vector, uses the [Euclidean length ](../general/euclidean.md)function.

## Example

```yaml
time window:
  emitting type: slidingUnitScaler
  window functions:
    feature scaling:
      function:
        unit scaler: {}
      attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `UNIT_SCALE`

| Before processing | After processing | Type         |
| ----------------- | ---------------- | ------------ |
| ask               | ask\_UNIT\_SCALE | double value |
| bid               | bid\_UNIT\_SCALE | double value |
