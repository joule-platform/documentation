# Robust Scale

## Objective&#x20;

Robust scaling is less sensitive to outliers compared to standardization. It uses the median and interquartile range (IQR) instead of the mean and standard deviation.

## Example

```yaml
time window:
  emitting type: slidingRobustNormalization
  window functions:
    normalization:
      function:
        robust scale: {}
      attributes: [ ask,bid ]
  policy:
    type: slidingTime
    slide: 500
    window size: 2500
```

### Output attributes

After processing, attributes receive automatically applied postfix ID = `ROBUST_SCALE`

| Before processing | After processing | Type         |
| ----------------- | ---------------- | ------------ |
| ask               | ask\_LOG\_NORM   | double value |
| bid               | bid\_LOG\_NORM   | double value |
