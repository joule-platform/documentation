---
description: Change the range of your data
---

# Scaling

> Scaling adjusts features by the Euclidean length of the passed vector, allowing for comparable scales across different data points.

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-type="content-ref"></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td></td><td><mark style="color:orange;"><strong>Unit scale</strong></mark></td><td></td><td></td><td><a href="unit-scale.md">unit-scale.md</a></td></tr><tr><td></td><td><mark style="color:orange;">R<strong>obust scale</strong></mark></td><td></td><td></td><td><a href="robust-scale.md">robust-scale.md</a></td></tr></tbody></table>
