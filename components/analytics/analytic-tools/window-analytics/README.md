---
description: Standard window functions are provided to perform event based analytics
---

# Window analytics

## Objective

Joule provides the ability to perform time or number of events **window based analytics**. A suite of standard window functions are provided out-of-the-box.

Developers can create custom analytic functions which are deployable as discrete components on the classpath.

Joule provides tumbling and sliding window analytics.

{% tabs %}
{% tab title="Tumbling" %}
Tumbling windows are a non-overlapping events arrays ordered by time. In a tumbling window, events are grouped in to a single window based on time or count.

An event belongs to only one window. Events within a window are only processed once when a new window is presented for processing.

<figure><img src="../../../../.gitbook/assets/tumbling.png" alt=""><figcaption><p>Tumbling windows</p></figcaption></figure>
{% endtab %}

{% tab title="Sliding" %}
Sliding windows, unlike tumbling windows, output events only for points in time when the content of the window actually changes.

In other words, when an event enters or exits the window. Every window has at least one event. Events can belong to more than one sliding window.

<figure><img src="../../../../.gitbook/assets/slide.png" alt=""><figcaption><p>Sliding windows</p></figcaption></figure>
{% endtab %}
{% endtabs %}

## Example & DSL attributes

This code defines a **tumbling time window** for aggregating data in the **processing unit** pipeline:

1. <mark style="color:green;">**aggregate functions**</mark>\
   Applies functions like FIRST, LAST, MIN, MAX, SUM, MEAN, VARIANCE, and STDEV on fields like `ask`, `bid`, `volume`, and `volatility`.
2. <mark style="color:green;">**policy**</mark>\
   The window size is set to 5000 milliseconds (5 seconds).

It processes and emits analytics based on the specified window and functions.

```yaml
processing unit:
  pipeline:
    - time window:
        emitting type: tumblingQuoteAnalytics
        aggregate functions:
          FIRST: [ask]
          LAST: [ ask ]
          MIN: [ ask ]
          MAX: [ bid ]
          SUM: [ volume ]
          MEAN: [ volatility ]
          VARIANCE: [ volatility ]
          STDEV: [ volatility ]
        policy:
          type: tumblingTime
          window size: 5000
```

### Attribute schema

<table><thead><tr><th width="209">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>emitting type</td><td>name of the event</td><td>String</td><td>true</td></tr><tr><td>preprocessor</td><td>Window preprocessor</td><td></td><td>false</td></tr><tr><td>window listeners</td><td>Custom window listeners which are executed on window generation. See window listener documentation below</td><td>See documentation below.</td><td>false</td></tr><tr><td>aggregate functions</td><td>Standard set of aggragation functions based over a window using group by semantics</td><td>See documentation below.</td><td>false</td></tr><tr><td>policy</td><td><p>Type of window processing. Supported types: </p><ul><li><p>Tumbling</p><ul><li>tumblingTime</li><li>tumblingCount</li></ul></li><li><p>Sliding</p><ul><li>slidingTime</li><li>slidingCount</li></ul></li></ul></td><td><p>String</p><p>e.g. slidingCount</p></td><td>true</td></tr></tbody></table>

## Available options

In the following pages you can read further how to apply the window functionalities and a list of all the aggregate functions.

<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Tumbling window</strong></mark></td><td>Fixed-sized, non-overlapping window analytic function support</td><td></td><td><a href="tumbling-window.md">tumbling-window.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Sliding window</strong></mark></td><td>Fixed-sized, overlapping window analytic function support</td><td></td><td><a href="sliding-window.md">sliding-window.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Aggregate functions</strong></mark></td><td>Standard statistics calculations for streaming event windows</td><td></td><td><a href="aggregate-functions.md">aggregate-functions.md</a></td></tr></tbody></table>
