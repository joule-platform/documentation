---
description: Standard statistics calculations for streaming event windows
---

# Aggregate functions

## Objective

Joule provides a set of standard aggregate functions that can be applied to a set of events within a triggered window.

These are processed using the `group by` definition provided with the use case.

## Example

```yaml
processing unit:
  pipeline:
    - time window:
        emitting type: tumblingAggregatesAnalytics
        aggregate functions:
          FIRST: [ask,bid]
          LAST: [ ask,bid ]
          MIN: [ ask,bid ]
          MAX: [ ask,bid ]
          SUM: [ volume ]
          MEAN: [ volatility ]
          VARIANCE: [ volatility ]
          STDEV: [ volatility ]
        policy:
          type: tumblingTime
          window size: 5000
```

This will produce the defined set of aggregate values and add these to the emitting event. For example these attributes will be added to the event:

* `ask_FIRST` and `bid_FIRST`
* `ask_LAST` and `bid_LAST`
* `volatility_MEAN`
* etc;.

## Available aggregate functions

When the function applied the field the result is added in the returned stream events as an additional field i.e. `<field_name>_SUM`

<table><thead><tr><th width="272">Type</th><th>Description</th></tr></thead><tbody><tr><td>SUM</td><td>Sum of field values</td></tr><tr><td>MIN</td><td>Min of field value</td></tr><tr><td>MAX</td><td>Max of field value</td></tr><tr><td>MEAN</td><td>Mean of field value</td></tr><tr><td>VARIANCE</td><td>Variance of field value</td></tr><tr><td>STDEV</td><td>Standard deviation of field value</td></tr><tr><td>FIRST</td><td>First field value in window</td></tr><tr><td>LAST</td><td>Last field value in window</td></tr><tr><td>HARMONIC_MEAN</td><td>Harmonic mean of field value</td></tr><tr><td>GEOMETRIC_MEAN</td><td>Geometric mean of field value</td></tr><tr><td>PVARIANCE</td><td>Population variance of field value</td></tr><tr><td>SECOND_MOMENT</td><td>Seond moment of field value</td></tr><tr><td>SUM_SQRTS</td><td>Sum of square root of field value</td></tr><tr><td>SUM_LOGS</td><td>Sum of log of field value</td></tr></tbody></table>
