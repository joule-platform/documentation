---
description: Fixed-sized, overlapping time or event count window analytic function support
---

# Sliding window

## Objective

Sliding windows, unlike tumbling windows, output events only for points in time when **the content of the window actually changes**.

In other words, when an event enters or exits the window. Every window has at least one event. Events can belong to more than one sliding window.

## Time based windows

### Example

Configures a sliding time based window with a size of 10 seconds using an analytical slide window of 7 seconds.

```yaml
time window:
  emitting type: pageFailures
  policy:
    type: slidingTime
    slide: 7
    window size: 10
    time unit: SECONDS
```

### Attributes schema

<table><thead><tr><th width="155">Attribute</th><th width="280">Description</th><th width="220">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>window size</td><td>Size of window with respect to timeUnit</td><td>Long</td><td>true</td></tr><tr><td>slide</td><td>Time interval to shift in to next window</td><td>Long</td><td>true</td></tr><tr><td>time unit</td><td><p>TimeUnit used for configuring window trigger. Supported types:</p><ul><li>NANOSECONDS</li><li>MICROSECONDS</li><li>MILLISECONDS</li><li>SECONDS</li><li>MINUTES</li><li>HOURS</li><li>DAYS</li></ul></td><td><p>TimeUnit</p><p>Default: MILLISECONDS</p></td><td>false</td></tr></tbody></table>

## Event count based windows

### Example

Configures a sliding window with a maximum of 1000 events with a slide size of 300 events.

```yaml
time window:
  emitting type: pageFailures
  policy:
    type: slidingCount
    window size: 1000
    slide: 300
```

### Attributes schema

<table><thead><tr><th width="160">Attribute</th><th width="285">Description</th><th width="201">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>window size</td><td>Maximum number of events before triggering window processing</td><td>Long</td><td>true</td></tr><tr><td>slide</td><td>Number of latest events to shift in to the next window</td><td>Long</td><td>true</td></tr></tbody></table>
