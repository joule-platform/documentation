---
description: Advanced analytic processors such as geospatial tracking
---

# Advanced analytics

These advanced analytic processors support a distinct set of use cases such as geospatial tracking and marketing.

## Available options

<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Geospatial</strong></mark></td><td>Optimised real-time geospatial analytics for geofence occupancy and tracking</td><td></td><td><a href="geospatial/">geospatial</a></td></tr><tr><td><mark style="color:orange;"><strong>HyperLogLog</strong></mark></td><td>Probabilistic counter for large or high cardinality datasets</td><td></td><td><a href="hyperloglog.md">hyperloglog.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Distinct counter</strong></mark></td><td>Calculate distinct field values over time</td><td></td><td><a href="distinct-counter.md">distinct-counter.md</a></td></tr></tbody></table>
