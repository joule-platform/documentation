---
description: Calculate distinct field values over time
---

# Distinct counter

## Overview

The distinct counter  is a stateful processor which tracks distinct values for specified fields. Two key functions are provided:

* Number of distinct field values
* Number of of times the distinct field value has been seen

## Example & DSL attributes

```yaml
distinct counter:
  fields:
    - imsi
    - imei
```

### Response

The processor adds a distinctCounter attribute with the following result

```
distinctCounter: 
    imsi:
        505010000011111 : 101
        904130454090869 : 78
    imei:
         350123451234560 : 101
         346543125767631 : 78
         983453473245799 : 12
```

## Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Descriptive name of the processor function</td><td><p>String</p><p>Default: Random UUID</p></td><td>false</td></tr><tr><td>fields</td><td> List of fields to count the number of time the same value has been seen</td><td>String List </td><td>true</td></tr></tbody></table>
