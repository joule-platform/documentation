---
description: Trigger geospatial events in real-time to drive location based use cases
---

# Geofence occupancy trigger

## Overview

Trigger a geofence based occupancy trigger using a custom event plugin.

## Example & DSL attributes

This example executes a custom marketing messenger plugin.

```yaml
geofence occupancy trigger:
    name: marketingCampaign
    tracker field: geoTrackingInfo
    plugin: 
      com.fractalworks.streams.examples.telco.marketing.MarketingCampaignMessenger
```

When the processor receives event it checks if the `geoTrackingInfo` exist with a valid `GeoTrackingInfo` object.

### Plugin Example

Simple example on how to implement an EventFunction for this processor type.

```java
/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.examples.telco.marketing;

import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.processors.geospatial.types.geo.GeoFenceConstants;
import com.fractalworks.streams.sdk.functions.EventFunction;

import java.util.UUID;

/**
 * Simple marketing messenger event function
 *
 * @author Lyndon Adams
 */
public class MarketingCampaignMessenger implements EventFunction {

    private final UUID uuid = UUID.randomUUID();
    public static String MARKETING_MSG_FIELD = "MarketingCampaignMessenger";

    public MarketingCampaignMessenger() {
        // Required constructor
    }

    @Override
    public void onEvent(StreamEvent event, Context context) {
        GeoFenceConstants state = (GeoFenceConstants) context.getValue("state");
        int geofenceId = (int) context.getValue("geofenceId");
        Object trackingTag = context.getValue("trackingTag");
        String message = null;

        switch( state){
            case ENTERED:
                message = String.format("Welcome to geofence %d", geofenceId);
                break;
            case DWELLING:
                message = String.format("Enjoy just ask from geofence %d",geofenceId);
                break;
            case EXITED:
                message = String.format("See you next time from geofence %d",geofenceId);
                break;
            default:
                break;
        }

        if( message!= null){
            event.addValue(uuid, MARKETING_MSG_FIELD, message);
        }
    }
}

```

### Response

The processor adds a `MarketingCampaignMessenger` attribute with the following result

```yaml
MarketingCampaignMessenger: Welcome to geofence 123
```

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Descriptive name of the processor function</td><td><p>String</p><p>Default: Random UUID</p></td><td>false</td></tr><tr><td>tracker field</td><td>Field that holds the <code>GeoTrackingInfo</code> </td><td>String </td><td>true</td></tr><tr><td>plugin</td><td>Custom event plugin that is called for every geofence the event has been </td><td><p>Class: EventType</p><p>See <a href="broken-reference">documentation</a></p></td><td>true</td></tr></tbody></table>
