---
description: Real-time entity geo tracking for advanced use cases
---

# Entity geo tracker

## Overview

Events with location information, longitude and latitude, can be used for entity based geo tracking. Entities such as mobile phone and GPS enabled devices with unique identifiers can used since they provide the required location attributes for the analytics to function.

This is a stateful based feature, once an entity enters a geofence it is tracked until it has exited. There are three states the entity triggers for further processing.

#### Key tracking features

* Entry
* Dwelling
* Exit

Tracking information provided as an attribute geoTrackingInfo field within the the processed event, see  the [Geo Tracking Info Response](entity-geo-tracker.md#geo-tracking-info-response)  section below.

## Example & DSL attributes

```yaml
entity geo tracker:
    name: promotions
    entity key: imsi
    min dwelling time: 2
    timeUnit: MINUTES
    geofences:
      -
        id: 107
        coordinates: ["51.506645","-0.20406286"]
        radius: 150
      -
        id: 106
        coordinates: ["51.503845","-0.2172389"]
        radius: 150
      -
        id: 119
        coordinates: ["51.509743","-0.18553847"]
        radius: 150  
```

### Response

The processor adds a `geoTrackingInfo` attribute with the following result

```yaml
geoTrackingInfo:
    geoTrackingInfo:
        GeoTrackingInfo:
            trackingTag=1234567890, 
            currentSpeedAndDirection=Triple{x=0.018464618471082432, y=GeoDirection{description='NorthEast'}, z=58.10799195554135}, 
            previousSpeedAndDirection=Triple{x=0.03388915574387011, y=GeoDirection{description='East'}, z=79.01663649781628}, 
            distanceTraveled=92.32309235541216, 
            previousDistanceTraveled=169.44577871935053, 
            previousLat=51.504119873046875, 
            previousLng=-0.2196580022573471, 
            currentLat=51.50470733642578, 
            currentLng=-0.2187139093875885, 
            geoFenceOccupancyState={106=ENTERED}, 
            previousUpdatedTimestamp=1715617238476, 
            currentUpdatedTimestamp=1715617243476}, 
            latitude=51.50470697, 
            imsi=1234567890, 
            longitude=-0.218713905
```

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>geofences</td><td>List of geofences to trigger entity </td><td>See geofence attribute section</td><td>true</td></tr><tr><td>entity key</td><td>Entity value to track. provide unique key</td><td><p>String</p><p>Default: id</p></td><td>true</td></tr><tr><td>geo tracking info</td><td>StreamEvent key to place tracking information</td><td><p>String</p><p>Default: geoTrackingInfo</p></td><td>false</td></tr><tr><td>latitude field</td><td>StreamEvent field holding latitude value</td><td><p>String</p><p>Default: latitude</p></td><td>false</td></tr><tr><td>longitude field</td><td>StreamEvent field holding longitude value</td><td><p>String</p><p>Default: longitude</p></td><td>false</td></tr><tr><td>dwelling time</td><td>Minimum dwelling time within a geofence</td><td><p>Long</p><p>Default: 15 Seconds</p></td><td>false</td></tr><tr><td>timeUnit</td><td><p>Time unit used to assess dwelling time.</p><p>Supported units:</p><ul><li>SECONDS</li><li>MINUTES</li><li>HOURS</li></ul></td><td><p>TimeUnit</p><p>Default: SECONDS</p></td><td>false</td></tr><tr><td>default radius</td><td>Geofence radius override to be applied if reference data  is missing</td><td><p>Float</p><p>Default: 4.0f ( xx feet)</p></td><td>false</td></tr><tr><td>spatial index</td><td>An area is set out as a search tree. The default area is a flattened world divided out in to rectangles.</td><td>See <a href="spatial-index.md">spatial index documentation</a></td><td>false</td></tr></tbody></table>

## Geofence Attribute

A geofence is a circle defined up a unique Id, its centre point(latitude and longitude coordinates)  and radius. One or more geofences are defined as a list.

```
geofences:
  -
    id: 1000
    coordinates: ["51.4623328","-0.1759467"]
    radius: 150
  -
    id: 2000
    coordinates: ["51.5136287","-0.1137969"]
    radius: 150
```

### Attributes schema

<table><thead><tr><th width="209">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>id</td><td>Unique Id for the geofence</td><td>Integer</td><td>true</td></tr><tr><td>coordinates</td><td>Centre latitude and longitute coordinates defined as string to ensure correct resolution</td><td>String Array</td><td>true</td></tr><tr><td>radius</td><td>Radius of geofence in feet</td><td>Float</td><td>true</td></tr></tbody></table>

## Geo Tracking Info Response

The geoTrackingInfo field, or the one specified by the developer, is added to the processed StreamEvent object on processing completion. A `GeoTrackingInfo` object is assigned to the field. Below are the attributes held within the object.&#x20;

```markup
Key: geoTrackingInfo 
Value: GeoTrackingInfo
      - trackingTag
      - currentSpeedAndDirection
      - previousSpeedAndDirection
      - distanceTraveled
      - previousDistanceTraveled
      - previousLat
      - previousLng
      - currentLat
      - currentLng
      - geoFenceOccupancyState
      - previousUpdatedTimestamp
      - currentUpdatedTimestamp
```
