---
description: Google reverse geocode using longitude and latitude to street address
---

# Reverse geocoding

## Overview

Reverse geocoding using provided longitude and latitude to gain a human-readable street address.

## Example & DSL attributes

```yaml
reverse geocoding:
  name: googleLookup
  apiKey: BJazSyDCKmFA1bTBjAp4beYMp7sVVSAqfBtNeuZ0
  response field: currentLocation
```

### Google Geocode Requests

Joule calls the Google geocode api directly using the provided api key and latitude and longitude provided attributes.

{% hint style="info" %}
Google Geocode request used

[https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452\&key=YOUR\_API\_KEY](https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452\&key=YOUR\_API\_KEY)
{% endhint %}

**Note** Be aware a high frequency of google api calls will incur high charges against your google project. Joule will cache responses based upon matching `latlng` to reduce calls.

### Response

This is a direct Google Geocode API request which places the response payload in the response field. Further details on what is returned can be found [here](https://developers.google.com/maps/documentation/geocoding/start?\_gl=1\*13qcz28\*\_up\*MQ..\*\_ga\*NzIzNTg3MDg5LjE3MTYyMTQyOTY.\*\_ga\_NRWSTWS78N\*MTcxNjIxNDI5Ni4xLjAuMTcxNjIxNDI5Ni4wLjAuMA..#quotas).

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Descriptive name of the processor function</td><td><p>String</p><p>Default: Random UUID</p></td><td>false</td></tr><tr><td>apiKey</td><td>Your Google billing project API key</td><td>String </td><td>true</td></tr><tr><td>response field</td><td>Custom field name to place result of search lookup</td><td>String<br>Default: geoLocation</td><td>false</td></tr><tr><td>latitude field</td><td>Custom field name to gain longitude dimension</td><td>String<br>Default: latitude</td><td>false</td></tr><tr><td>longitude field</td><td>Custom field name to gain longitude dimension</td><td>String<br>Default: longitude</td><td>false</td></tr></tbody></table>
