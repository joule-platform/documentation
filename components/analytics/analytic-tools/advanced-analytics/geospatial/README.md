---
description: Optimised real-time geospatial analytics for geofence occupancy and tracking
---

# Geospatial

## Overview

Event based geospatial analytics provide the ability to create location aware solutions. Joule provides a small set of processor optimised for **real-time geospatial analytics**.

Users can define geofences of interest and configure triggers to inform them of occupancy.

### Example applications

1. Fleet management
2. Asset management
3. Geo-marketing
4. Personalised recommendations

## Available geospatial options

<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Entity geo tracker</strong></mark></td><td>Real-time entity geo tracking for advanced use cases</td><td></td><td><a href="entity-geo-tracker.md">entity-geo-tracker.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Geofence occupancy trigger</strong></mark></td><td>Trigger geospatial events in real-time to drive location based use cases</td><td></td><td><a href="geofence-occupancy-trigger.md">geofence-occupancy-trigger.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Geo search</strong></mark></td><td></td><td>Real-time view on what entities are nearby with respect to geofence radius</td><td><a href="geo-search.md">geo-search.md</a></td></tr><tr><td><mark style="color:orange;"><strong>IP address resolver</strong></mark></td><td>Take an IP address and perform a geo location search</td><td></td><td><a href="ip-address-resolver.md">ip-address-resolver.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Reverse geocoding</strong></mark></td><td>Google reverse geocode using longitude and latitude to street address</td><td></td><td><a href="reverse-geocoding.md">reverse-geocoding.md</a></td></tr></tbody></table>

## Supporting Services

<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Spatial index</strong></mark></td><td>Optimised data structure for spatial search</td><td></td><td><a href="spatial-index.md">spatial-index.md</a></td></tr></tbody></table>
