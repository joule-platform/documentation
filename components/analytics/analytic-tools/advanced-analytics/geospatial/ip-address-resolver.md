---
description: Take an IP address and perform a geo location search
---

# IP address resolver

## Overview

Translate IP address to specific geo locations to enable web traffic insights such as web reach, DDOS attacks, location density and frequency.&#x20;

## Example & DSL attributes

```yaml
ip geo resolver:
  name: badActors resovlver
  ip field: ip_address
  response field: ipGeoEntity

  mapping entities:
    source: data/mini_ipaddress.csv
```

### Response

```yaml
ipGeoLocation:
    country: JP
    area: Tokyo
    city: Minato
```

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Descriptive name of the processor function</td><td><p>String</p><p>Default: Random UUID</p></td><td>false</td></tr><tr><td>ip field</td><td>Field that holds the ip address to be searched</td><td>String </td><td>true</td></tr><tr><td>response field</td><td>Custom field name to place result of search lookup</td><td>String<br>Default: ipGeoLocation</td><td>false</td></tr><tr><td>mapping entities</td><td>Section describing the parser and file to be used</td><td>See <a href="ip-address-resolver.md#mapping-entities">mapping entities</a> section</td><td>true</td></tr></tbody></table>

## Mapping Entities

### File Format

```csv
fromip,toip,country,area,city
1.1.1.0     ,1.1.1.255    ,AU,Queensland                       ,Brisbane
1.1.2.0     ,1.1.7.255    ,CN,Fujian                           ,Fuzhou
1.1.8.0     ,1.1.8.255    ,CN,Shanghai Shi                     ,Shanghai
1.1.9.0     ,1.1.63.255   ,CN,Guangdong                        ,Guangzhou
1.1.64.0    ,1.1.112.255  ,JP,Tokyo                            ,Minato
1.1.113.0   ,1.1.113.255  ,JP,Tokyo                            ,Chiyoda
1.1.114.0   ,1.1.125.255  ,JP,Tokyo                            ,Minato
1.1.126.0   ,1.1.127.255  ,JP,Tokyo                            ,Chiyoda
```

### Attributes schema

<table><thead><tr><th width="122">Attribute</th><th width="297">Description</th><th width="236">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>parser</td><td>Custom parser that converts to a custom type that extends GeoNode, see <a href="../../../../../what-is-joule/data-types/geonode.md">documentation</a> for further details on implementing a GeoNode entity</td><td>String<br>Default: IpCityArrowParser</td><td>false</td></tr><tr><td>source</td><td>File location of the mapping entities</td><td>String</td><td>true</td></tr></tbody></table>
