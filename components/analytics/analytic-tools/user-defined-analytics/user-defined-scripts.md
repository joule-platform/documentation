---
description: Leverage pre-existing analytics scripts within a streaming context
---

# User defined scripts

## Objective

The analytics processor feature set is extended to host external scripts within the Joule processing context. Enabling existing scripting assets to be leveraged within a streaming context.

Apply external analytical scripts within a real-time streaming context.

{% hint style="info" %}
Currently only Javascript ECMAScript 2024 and Python 3.11 are supported
{% endhint %}

<figure><img src="../../../../.gitbook/assets/image (2).png" alt=""><figcaption><p>Graphic data flow of a functioning UDF</p></figcaption></figure>

## Execution models

This guide explores different execution models for integrating scripts and functions within a stream processing environment.

We will discuss how to use pre-existing JavaScript or Python scripts, custom functions, and expressions to perform complex analytics and calculations on event data.

1. [<mark style="color:green;">**Script and expression**</mark>](user-defined-scripts.md#script-and-expression)\
   Learn how to incorporate existing JavaScript files and use expressions that reference event attributes, ideal when you want to calculate new variables on-the-fly.
2. [<mark style="color:green;">**Script and function**</mark>](user-defined-scripts.md#script-and-function)\
   Understand how to define custom functions that process entire events, allowing for deeper calculations by accessing multiple event attributes.

### Script and Expression

Use this option when you have pre-existing scripts which you want to leverage within a stream processing context.

```yaml
analytic:
  script: ./scripts/js/myCustomFunctions.js
  expression: bid - squareRoot(`${ask}` / `${bid}`)
  assign to: new_event_variable
```

#### **User defined function**

This example applies the `bid` and `ask` event attributes to the expression.

Note, the current implementation requires the script to be provided using the `js` extension due to the way the expression is defined.

All other variables are provided as global within the execution context.

{% tabs %}
{% tab title="Javascript" %}
```javascript
export function squareRoot(num) {
    if (num < 0) return NaN;
    return Math.sqrt(num);
}
```
{% endtab %}

{% tab title="Python" %}
```python
import math

def square_root(num):
    if num < 0:
        return float('nan')
    return math.sqrt(num)
```
{% endtab %}
{% endtabs %}

### Script and function

This option builds upon the previous example, but uses a method which is supplied with a `StreamEvent` which can further call imported scripts.

All other variables are provided as global within the execution context.

```yaml
analytic:
  script: ./scripts/js/usedDefinedCustomModuleFunctions.js
  function: totalCompensationPackage
  assign to: total_compensation
```

#### **User defined function**

The event is automatically passed as a method parameter per execution call.

This method uses the Javascript modules which are defined using the `mjs` file extension.&#x20;

{% tabs %}
{% tab title="Javascript" %}
```javascript
export function totalCompensationPackage(event) {
    return event.getValue("salary") +
           event.getValue("bonus") + 
           event.getValue("benefits");
}
```
{% endtab %}

{% tab title="Python" %}
```python
def total_compensation_package(event):
    return (event.get("salary", 0) +
            event.get("bonus", 0) +
            event.get("benefits", 0))
```
{% endtab %}
{% endtabs %}

## Attributes schema

The same attributes described in User defined analytics are applicable, see [documentation](user-defined-analytics.md).

<table><thead><tr><th width="144">Attribute</th><th width="497">Description</th><th width="291">Data Type</th></tr></thead><tbody><tr><td>script</td><td>Path of the script to loaded within the Joule processing context</td><td>String</td></tr><tr><td>expression</td><td>Math expression without an assignment variable. Required if method has not been provided.</td><td>String</td></tr><tr><td>function</td><td>Function to execute within the provided script using a StreamEvent as a parameter</td><td>String</td></tr></tbody></table>
