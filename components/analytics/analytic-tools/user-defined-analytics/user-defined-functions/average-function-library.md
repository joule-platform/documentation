---
description: Joule math functions
---

# Average function library

## Objective

Averages are essential for summarising trends and smoothing fluctuations in datasets. This section covers three common types of averages:

1. [<mark style="color:green;">**Exponential Moving Average (EMA)**</mark>](average-function-library.md#exponential-moving-average)\
   This method emphasises recent data points, making it responsive to changes, particularly in financial applications.
2. [<mark style="color:green;">**Weighted Moving Average**</mark>](average-function-library.md#weighted-moving-average)\
   This average assigns different weights to data points, allowing for a focus on more important observations.
3. [<mark style="color:green;">**Simple Moving Average (SMA)**</mark>](average-function-library.md#simple-moving-average)\
   The SMA calculates the unweighted mean of a fixed number of previous data points and is often used to identify trends over time.

Each type of average offers unique insights, making them valuable tools in data analysis.

## Functions

Following is a list of the average functions which can be leverage through Joule.

### Exponential Moving Average

> An exponential moving average (EMA), also known as an exponentially weighted moving average (EWMA),[\[5\]](https://en.wikipedia.org/wiki/Moving\_average#cite\_note-5) is a first-order [infinite impulse response](https://en.wikipedia.org/wiki/Infinite\_impulse\_response) filter that applies weighting factors which decrease [exponentially](https://en.wikipedia.org/wiki/Exponential\_decay). The weighting for each older [datum](https://en.wikipedia.org/wiki/Data) decreases exponentially, never reaching zero. This formulation is according to Hunter (1986) \[[wikipedia](https://en.wikipedia.org/wiki/Moving\_average#Exponential\_moving\_average)].

<table><thead><tr><th width="144">Attribute</th><th width="367">Description</th><th width="128">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>fields</td><td>Fields to calculated from the event</td><td>String[]</td><td>true</td></tr><tr><td>event history</td><td>Number of rolling events to stored and used for the calculation</td><td>Integer</td><td>false</td></tr><tr><td>assign prefix</td><td>Prefix to use for the result assignment. This is used to allow the same function to be used multiple times. If this is not provided the function postfix will be applied e.g. ask_EMA</td><td>String<br>Default: Function postfix </td><td>false</td></tr></tbody></table>

### Weighted Moving Average

> A weighted average is an average that has multiplying factors to give different weights to data at different positions in the sample window. Mathematically, the weighted moving average is the [convolution](https://en.wikipedia.org/wiki/Convolution) of the data with a fixed weighting function \[[wikipedia](https://en.wikipedia.org/wiki/Moving\_average#Weighted\_moving\_average)].&#x20;

### Simple Moving Average

> In financial applications a simple moving average (SMA) is the unweighted [mean](https://en.wikipedia.org/wiki/Arithmetic\_mean) of the previous 𝑘![{\displaystyle k}](https://wikimedia.org/api/rest\_v1/media/math/render/svg/c3c9a2c7b599b37105512c5d570edc034056dd40) data-points. However, in science and engineering, the mean is normally taken from an equal number of data on either side of a central value. This ensures that variations in the mean are aligned with the variations in the data rather than being shifted in time \[[wikipedia](https://en.wikipedia.org/wiki/Moving\_average#Simple\_moving\_average)].
