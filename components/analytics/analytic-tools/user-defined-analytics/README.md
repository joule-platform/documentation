---
description: >-
  Define analytic expressions or provide as a script using supported interpreted
  languages
---

# User defined analytics

## Overview

Joule offers a user-friendly streaming analytics capability tailored to deliver real-time insights efficiently.

It enables users to harness existing analytical resources, define expressions, integrate reference data, and develop custom analytic plugins for complex calculations.

## Key features

* Expressions, scripting and function execution
* Prime calculations with starting values
* Constants and reference data repositories
* Stateful calculations with analytic result memory

Additionally developers can extend the analytical capabilities by implementing various analytics APIs. Drive business with the ability to execute imperative expressions, functions and scripts.

Platform users can deploy custom stateful and stateless analytics using the Joule DSL using analytics processors.

## Example

Learn how to apply the analytical features through an example

{% content-ref url="streaming-analytics-example.md" %}
[streaming-analytics-example.md](streaming-analytics-example.md)
{% endcontent-ref %}

## Available options

{% hint style="info" %}
Javascript and Python language support using the Graalvm runtime compilation engine
{% endhint %}

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>User defined analytics</strong></mark></td><td>Declarative stateful math expressions evaluated in real-time</td><td></td><td><a href="user-defined-analytics.md">user-defined-analytics.md</a></td></tr><tr><td><mark style="color:orange;"><strong>User defined scripts</strong></mark></td><td>Leverage pre-existing analytics scripts within a streaming context</td><td></td><td><a href="user-defined-scripts.md">user-defined-scripts.md</a></td></tr><tr><td><mark style="color:orange;"><strong>User defined functions</strong></mark></td><td>Extend the analytics ecosystem using the Analytics API</td><td></td><td><a href="user-defined-functions/">user-defined-functions</a></td></tr></tbody></table>

***
