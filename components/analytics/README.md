---
icon: chart-mixed-up-circle-dollar
description: Analytics form the core platform feature that facilitates insight to value
---

# Analytics

## Overview

Joule offers a low-code streaming analytics capability tailored to **deliver real-time insights**.

It enables your developers to harness existing analytical resources, define expressions, integrate reference data, and develop custom analytic plugins for complex calculations.

{% hint style="success" %}
Drive contextual insights by combining schedule event metrics and reference data to calculate real-time analytics
{% endhint %}

## Key features of Joule's streaming analytic capability include

1. <mark style="color:green;">**Streamlined integration of analytical assets**</mark>\
   Seamlessly incorporate and leverage existing analytical tools and resources within the Joule platform **minimising setup time and maximising productivity**.
2. <mark style="color:green;">**Expression definition**</mark>\
   Define complex analytical expressions easily using Javascript or Python, enabling quick implementation of calculations and transformations on streaming data.
3. <mark style="color:green;">**Scripting definition**</mark>\
   Define complex analytical expressions easily using Javascript or Python, enabling quick implementation of calculations and transformations on streaming data.
4. <mark style="color:green;">**Reference data binding**</mark>\
   Integrate reference data sources directly into analytics workflows, enriching streaming data with contextual information for more comprehensive analysis.
5. <mark style="color:green;">**Custom analytic plugins**</mark>\
   Develop and deploy custom plugins tailored to specific analytical requirements, allowing users to perform sophisticated calculations and manipulations on streaming data streams.
6. <mark style="color:green;">**Real-time insights**</mark>\
   Gain actionable insights in real-time, enabling prompt decision-making and responsiveness to evolving data patterns and trends.
7. <mark style="color:green;">**Scalable architecture**</mark>\
   Joule's architecture is designed for scalability, ensuring performance efficiency even with large volumes of streaming data and complex analytical workflows.

## Types of analytics

Joule provides various methods to define and leverage analytics within a streaming context.

Joule offers many out-of-the-box analytic implementations that support the implementation of use cases.&#x20;

Analytics are categorised by function.

<table data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Analytic tools</strong></mark></td><td>Define math expressions or provide as a file using Joule supported languages and APIs </td><td></td><td><a href="analytic-tools/">analytic-tools</a></td></tr><tr><td><mark style="color:orange;"><strong>ML inferencing</strong></mark></td><td>Leverage streaming online predictions to drive insights to action</td><td></td><td><a href="ml-inferencing/">ml-inferencing</a></td></tr><tr><td><mark style="color:orange;"><strong>Metrics engine</strong></mark></td><td>In-memory SQL compliant metrics engine for advance analytics calculations, features or alert based triggers</td><td></td><td><a href="metrics-engine/">metrics-engine</a></td></tr></tbody></table>

## Extending capabilities

Joule provides an Analytics SDK to enable business developers to extend the analytical capabilities.&#x20;

{% hint style="info" %}
See Analytics API [documentation](../../developer-guides/builder-sdk/analytics-api/) for further information.
{% endhint %}
