---
description: Reuse existing metrics on startup and reduce the operational memory overhead
---

# Manage metrics

## Objective

Provide optional ability to reuse existing metrics on startup and reduce the operational memory overhead for stored metrics.

## Example & DSL attributes

This example performs two key metric table functions:

1. **Truncates** the `BidMovingAverage` table on process startup providing a clean restart.
2. Executes the default compaction query on **a 8 hour schedule** for the `BidMovingAverage` metric.

### Example

```yaml
metrics engine:
  ...
  foreach metric compute:
    metrics:
    - name: BidMovingAverage
    ...  
    #
    # Truncate BidMovingAverage on startup
    #
    truncate on start: true
    
    #
    # Manage metric storage
    #
    compaction policy:
      frequency: 8
      time unit: HOURS
```

### Metric Management Attributes

Each metric definition includes management attributes to control how the metric table is managed throughout the Joule process lifetime. Since Joule uses an in-memory database the size of table needs to be managed.

<table><thead><tr><th width="164">Attribute</th><th width="302">Description</th><th width="158">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>truncate on start</td><td>Truncate metric data on restart. <em>Note</em> if you import metrics using the initialisation DSL element you will need to set this to false</td><td><p>Boolean</p><p>Default: true</p></td><td>true</td></tr><tr><td>compaction policy</td><td>Execute table compaction on a scheduled cycle. See below.</td><td>See <a href="manage-metrics.md#compaction-policy-attributes">compaction section</a></td><td>true</td></tr></tbody></table>

### Compaction Policy Attributes <a href="#compaction-policy-attributes" id="compaction-policy-attributes"></a>

Managing metrics is a key aspect to the performance of the Joule runtime. By applying the compaction  policy this will reduce the memory footprint required of the JVM and therefore the number of GC cycles.

<table><thead><tr><th width="166">Attribute</th><th width="331">Description</th><th width="160">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>frequency</td><td>Frequency metrics are computed. Minimum value cannot be set below 1 for minute or hour time units.</td><td><p>Long</p><p>Default: 1</p></td><td>true</td></tr><tr><td>compaction query</td><td><p>User defined compaction query. </p><p></p><p><strong>Note:</strong> The default query compacts the metric table by deleting all row less than the max <code>createdTimestamp</code>.</p></td><td>String</td><td>false</td></tr><tr><td>time unit</td><td><p>Time unit used to set the scheduled processing policy.</p><p></p><p>Supported units: MINUTES, HOURS</p></td><td><p>TimeUnit</p><p>Default: HOURS</p></td><td>false</td></tr></tbody></table>
