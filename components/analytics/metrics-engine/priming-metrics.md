---
description: Prime metrics with existing values on process startup
---

# Priming metrics

## Objective

Prime a metric family dataset with existing calculated data on process startup.

{% hint style="success" %}
Joule will load an existing dataset from disk to memory as an initial image if this feature is enabled
{% endhint %}

## Uses

Often business applications will start processing immediately rather than wait for a set period of time before systems settle, or in this case metrics created.

Hence, by priming the Joule process with a pre-calculated set of metrics processing can start immediately.&#x20;

{% hint style="info" %}
For configuration details read the [Data priming at initialisation](../../pipelines/data-priming/) page
{% endhint %}

## Example

This example loads a local parquet file to the target `BidMovingAverage` metric family table

```yaml
stream:
  ...
  initialisation:
    data import:
      parquet:
        - schema: standardQuoteAnalyticsStream
          table: BidMovingAverage      
          files: ['data/parquet/mvavgs-prime.parquet']
          drop table: true
          index:
            fields: [ 'symbol' ]
            unique: false
```
