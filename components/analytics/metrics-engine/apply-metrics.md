---
description: Apply metrics within stream processing functions
---

# Apply metrics

## Objective

Use calculated metrics within processor logic, analytics, ML inferencing or simply send metrics to connected consumers.

## Metrics applications

To access metrics two methods have been provided:

* <mark style="color:green;">**Emit projection**</mark>\
  Metrics can be included within the emit `select` statement to enhance output events.
* <mark style="color:green;">**Query interface**</mark>\
  Metrics can also be accessed via the Metric Query API for use in custom processors, enhancing the integration of metric data in analytics workflows.

## Emit projection

Metrics can be added to the final output event using the emit `select` statement metrics query syntax.&#x20;

This section explains how use the metrics query language.

### Example

This example will add the `avg_bid_max` metric to the output event on a successful symbol look up match.

```yaml
emit:
  select: "symbol, BidMovingAverage.avg_bid_max; WHERE symbol=${symbol} 'avg_bid_max'"
```

## Query Structure

Users define metric queries directly within the `select` statement, allowing for dynamic lookups based on conditions such as a `symbol` value.

The query structure is formed of three required components and an optional override output alias.

Required components:

* metric-family
* metrics
* predicate

Optional

* output-alias

{% hint style="info" %}
**Structure follows a pragmatic form:**

\<metrics-family>.\<metrics>;\<predicate> '\<output-alias>' &#x20;
{% endhint %}

### metric-family

This is the logical name of the group of metrics, which should correspond to a metric name declared within the compute section.

### metrics

A comma delimited list of metrics or asterisk. Any defined metric needs to correspond the query projection attributes.&#x20;

### predicate

Matching and filtering statement using standard SQL. This is used to extract only those records that fulfils a specified condition.

<pre class="language-sql"><code class="lang-sql"><strong>-- Simple lookup
</strong>WHERE symbol='IBM'

-- Parameter replace using the corresponding the event object symbol value
WHERE symbol=${symbol}

-- Apply additonal filtering criteria
WHERE symbol=${symbol} AND avg_bid_max > 120.88
</code></pre>

### output alias

Use an alias for the output metric rather than the metric name for a single field output.

### Examples

```sql
-- Get all metrics from the BidMovingAverage metrics family for IBM 
BidMovingAverage.*;WHERE symbol='IBM'

-- Get the avg_bid_max metric from the BidMovingAverage metrics family all symbols matching the reference ${symbol} variable 
BidMovingAverage.avg_bid_max;WHERE symbol=${symbol}

-- Same as above with the ability to define a custom name for the resulting field
BidMovingAverage.avg_bid_max;WHERE symbol=${symbol} 'avg_bid_max'

-- Get the avg_bid_max and avg_bid_min metric from the BidMovingAverage metrics family
BidMovingAverage.avg_bid_max,avg_bid_min;WHERE symbol=${symbol}
```

## Query interface

Metrics can also be accessed via the Metric Query API for use in custom processors, enhancing the integration of metric data in analytics workflows.

{% hint style="info" %}
Find further information on how to leverage the value through the [API documentation](../../../developer-guides/builder-sdk/analytics-api/create-custom-metrics.md)
{% endhint %}
