# Last Will and Testament

## Overview

Last Will and Testament (LWT) is a powerful feature in MQTT that allows clients to specify a message that will be automatically published by the broker on their behalf, if or when an unexpected disconnection occurs.&#x20;

It provides a reliable means of communication and ensures that clients can gracefully handle disconnections without leaving topics in an inconsistent state. This feature is particularly valuable when clients must notify others of their unavailability or convey important information upon an unexpected disconnection, reference [HiveMQ](https://www.hivemq.com/blog/mqtt-essentials-part-9-last-will-and-testament/).&#x20;

Joule supports this feature by providing a dedicated DSL attribute, `last will`. The feature is applicable for both the source and sink transports.&#x20;

### **Example**

In this example, the `last will` feature is configured for the MQTT publisher.

It specifies that if the publisher disconnects unexpectedly, a message will be sent to the topic `humiditycontroller/status` to notify subscribers of the disconnection.

The message will indicate that the `Humidity Controller publisher disconnected from broker` and the publisher will wait for a delay of 7 seconds before sending this notification.

```yaml
mqttPublisher:
  ...

  last will:
    topic: humiditycontroller/status
    message: "Humidity Controller publisher disconnected from broker"
    interval delay: 7
```

### **Last will** attributes schema

These attributes provide the configuration parameters to send publisher disconnection notifications to connected subscribers.

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>topic</td><td>Topic to sent status message on to</td><td>String</td><td>true</td></tr><tr><td>message</td><td>Message to send to consumers when publisher no longer exists</td><td><p>String </p><p>Default: "Publisher disconnected from broker"</p></td><td>true</td></tr><tr><td>interval delay</td><td>The Server delays publishing the Client's Will Message until the Will Delay Interval has passed or the Session ends, whichever happens first.</td><td><p>Long</p><p>Default: 5 seconds</p></td><td>false</td></tr><tr><td>retain</td><td>If message is retained</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>qos</td><td>Quality of service</td><td><p>Integer</p><p>Default: 1</p></td><td>false</td></tr></tbody></table>
