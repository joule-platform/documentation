# Persistent messaging

## Overview

Within MQTT 5, clients can set a unique Message Expiry Interval in seconds for each PUBLISH message. This interval establishes the duration the broker preserves the PUBLISH message for subscribers that match the topic but are currently offline. If the interval isn’t defined, the broker must indefinitely hold the message for matching subscribers, yet disconnected subscribers. Furthermore, if the ‘retained=true’ option is selected during the PUBLISH message, the interval also dictates the length of time a message is retained on a particular topic, reference [HiveMQ](https://www.hivemq.com/blog/mqtt-essentials-part-9-last-will-and-testament/).

### Example

This example uses the MQTT v5 capabilities to store published messages on the MQTT broker.

{% tabs %}
{% tab title="DSL" %}
```
mqttPublisher:
  ...
  message expiry interval: 15
  retain: true
```
{% endtab %}

{% tab title="MQTT Configuration" %}
To enable message persistence two key broker configuration attributes are rquired.

```bash
persistence true
persistence_location /var/lib/mosquitto/mosquitto.db
```
{% endtab %}
{% endtabs %}

### **Message persistence** attributes schema

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>message expiry interval</td><td>The maximum time in seconds the broker will hold the message for matching subscribers, who may be offline (see <a href="../../sources/mqtt/session-management.md">Session Management</a>)</td><td><p>Long</p><p>Default: 0 seconds</p></td><td>false</td></tr><tr><td>retain</td><td>If message is retained on the broker</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr></tbody></table>
