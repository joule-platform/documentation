---
description: >-
  Integrate with distributed caching for complex use cases needing high
  availability
---

# Geode



## Overview

Joule’s **Geode Publisher Transport** leverages Apache Geode as a high-performance, distributed event transport, ideal for complex, high-availability use cases.

This system distributes events across a Geode cluster for further processing with **minimal latency, high concurrency and reliable transaction** support.

Geode's shared-nothing architecture ensures database-like consistency while maintaining low-latency performance. This robust architecture, deployed in production for over 15 years, has proven effective for large-scale enterprise solutions that demand both scalability and resilience.

{% hint style="info" %}
**Driver details:** [org.apache.geode:geode-core:1.15.1](https://mvnrepository.com/artifact/org.apache.geode/geode-core/1.15.1)
{% endhint %}

## Example & DSL attributes

This example configures the **Geode Publisher** to connect to a local Geode cluster (address `localhost`, port `10334`).

It uses `employee_id` as the key to identify records in the `employees` region.

Data is serialised with the `PDX` format, which is efficient for distributed caching and allows for compatibility across Geode clients.

```yaml
geode publisher:
  connection:
    locator address: localhost
    locator port: 10334

  key: employee_id
  region: employees
  serialization type: PDX
```

### Attributes schema

<table><thead><tr><th width="146">Attribute</th><th width="284">Description</th><th width="213">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>connection</td><td>Cluster connection details</td><td><p>See Connection attributes</p><p></p></td><td>true</td></tr><tr><td>key </td><td>Key attribute</td><td>String<br>Default: uuid</td><td>false</td></tr><tr><td>region</td><td>Storage target region</td><td><p>String</p><p>Default: streamEvents</p></td><td>false</td></tr><tr><td>serialization type</td><td>Serialisation method to use. <a href="../serialisers/serialisation/">See serialisation section</a></td><td><p>Enum</p><p>Default: PDX</p></td><td>false</td></tr></tbody></table>

## Additional resources

* To learn more about Apache Geode, [see the documentation](https://geode.apache.org/docs/)
