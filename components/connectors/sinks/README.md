---
icon: nfc-symbol
description: Distribute insights to consuming systems
---

# Sinks

## Overview

Data sinks in Joule enable integration with various downstream systems, enhancing the application’s ability to support additional business needs.

By **connecting to multiple types of systems**, Joule allows users to route processed data efficiently to databases, messaging platforms, cloud storage and more.

These integrations empower users to leverage Joule's data processing across different environments and tools, creating more **comprehensive, scalable solutions**.

## Available options

{% tabs %}
{% tab title="Streaming" %}
These guides provide configurations for Joule integrations using Kafka, RabbitMQ and MQTT. Enable flexible data streaming through standard messaging platforms.

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden></th><th data-hidden data-card-cover data-type="files"></th><th data-hidden data-card-target data-type="content-ref"></th><th data-hidden data-type="rating" data-max="5"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Kafka</strong></mark></td><td>Sends events to specified Kafka topics, enabling real-time data streams</td><td></td><td></td><td></td><td><a href="kafka.md">kafka.md</a></td><td>null</td></tr><tr><td><mark style="color:orange;"><strong>RabbitMQ</strong></mark></td><td><p>RabbitMQ is lightweight and easy to deploy messaging platform for event-based data integration</p><p></p></td><td></td><td></td><td></td><td><a href="rabbitmq/">rabbitmq</a></td><td>null</td></tr><tr><td><mark style="color:orange;"><strong>MQTT</strong></mark></td><td>Allows publishing events through MQTT, ideal for IoT and lightweight streaming</td><td><p></p><p></p></td><td></td><td></td><td><a href="mqtt/">mqtt</a></td><td>null</td></tr></tbody></table>
{% endtab %}

{% tab title="Platforms" %}
These guides provide configurations for Joule integrations with MinIO S3 and direct file storage. Enable flexible event archiving through cloud or local storage solutions.

<table data-card-size="large" data-view="cards" data-full-width="false"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Slack</strong></mark></td><td>Sent critical alerts to a monitored Slack channel</td><td></td><td><a href="slack.md">slack.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Email</strong></mark></td><td>Send critical alerts as an email message to key stakeholders</td><td></td><td><a href="email.md">email.md</a></td></tr></tbody></table>


{% endtab %}

{% tab title="Databases" %}
These guides provide configurations for Joule integrations with SQL databases: InfluxDB, MongoDB and Geode. Enable efficient data storage and retrieval across various database types.

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-cover data-type="files"></th><th data-hidden data-card-target data-type="content-ref"></th><th data-hidden data-type="rating" data-max="5"></th><th data-hidden></th></tr></thead><tbody><tr><td><p><mark style="color:orange;"><strong>SQL databases</strong></mark></p><p>Write events in micro batches to any database which supports JDBC Type 4 drivers</p></td><td></td><td></td><td></td><td><a href="sql-databases.md">sql-databases.md</a></td><td>null</td><td></td></tr><tr><td><p><mark style="color:orange;"><strong>InfluxDB</strong></mark></p><p>Standard time-series database idea for instrumentation or live KPI tracking</p></td><td></td><td></td><td></td><td><a href="influxdb.md">influxdb.md</a></td><td>null</td><td></td></tr><tr><td><mark style="color:orange;"><strong>MongoDB</strong></mark></td><td>Publish JSON based events to the mature standard document <code>storePublishes</code> JSON-based events to MongoDB, a widely-used document store</td><td></td><td></td><td><a href="mongodb.md">mongodb.md</a></td><td>null</td><td></td></tr><tr><td><mark style="color:orange;"><strong>Geode</strong></mark></td><td>Integrates with distributed caching for complex use cases needing high availability</td><td></td><td></td><td><a href="geode.md">geode.md</a></td><td>null</td><td></td></tr></tbody></table>
{% endtab %}

{% tab title="HTTP" %}
This guide provide configurations for Joule integrations using web sockets. Enable real-time data delivery to web-based clients for interactive applications.

<table data-card-size="large" data-view="cards" data-full-width="false"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>WebSockets endpoint</strong></mark></td><td>Publishes processed events to web clients via WebSocket transport, ideal for real-time dashboards and applications</td><td></td><td><a href="websocket-endpoint.md">websocket-endpoint.md</a></td></tr></tbody></table>
{% endtab %}

{% tab title="File" %}
These guides provide configurations for Joule integrations with MinIO S3 and direct file storage. Enable flexible event archiving through cloud or local storage solutions.

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>MinIO S3</strong></mark></td><td>Produces files to MinIO S3-compatible storage, whether cloud or locally hosted</td><td></td><td><a href="minio-s3.md">minio-s3.md</a></td></tr><tr><td><mark style="color:orange;"><strong>File transport</strong></mark></td><td>Write events directly to files in Joule-supported formats for archiving or later processing</td><td></td><td><a href="file-transport.md">file-transport.md</a></td></tr></tbody></table>
{% endtab %}
{% endtabs %}

## Transformation

Joule connectors send data as `StreamEvents`, which can be transformed into various output formats.

The currently supported formats include:

1. <mark style="color:green;">**CSV**</mark>\
   A standard format for spreadsheets and data analysis tools.
2. <mark style="color:green;">**JSON**</mark>\
   Common for web applications and lightweight data exchanges.
3. <mark style="color:green;">**Parquet**</mark>\
   Optimised for large-scale data processing and analytics.
4. <mark style="color:green;">**AVRO**</mark>\
   Suitable for row-based storage and schema evolution.

For additional customisation, the Joule SDK provides interfaces to develop custom formatters. Refer to the [Joule SDK documentation](../../../developer-guides/builder-sdk/) for further guidance.
