---
description: >-
  Write events directly to files in Joule-supported formats for archiving or
  later processing
icon: file
---

# File transport

## Overview

The **File transport** enables direct writing of events to files in supported formats, ideal for archiving or further processing.

This setup is suitable for **long-term storage**, legacy system **integration** and **validation or testing** workflows.

With options for **various file formats, storage locations** and **batch settings**, this transport is adaptable to both local and cloud-based storage.&#x20;

## Example & DSL attributes

This example configures the **File transport** to save events as a CSV file named `nasdaqAnalytic.csv` in the directory `/home/app/data/output/analytics`.

Events are written in batches of 1024 records, with a 1000-millisecond timeout for each batch. The CSV file uses [UTF-8 encoding](https://en.wikipedia.org/wiki/UTF-8) and a pipe (`|`) as the delimiter.

```yaml
file:
  filename: nasdaqAnalytic
  path: /home/app/data/output/analytics
  batchSize: 1024
  timeout: 1000
  formatter:
    csvFormatter:
      contentType: text/csv
      encoding: UTF-8
      delimiter: "|"
```

### Attributes schema

<table><thead><tr><th width="150">Attribute</th><th width="173">Description</th><th width="288">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>filename</td><td>file name to use</td><td><p>String</p><p></p></td><td>true</td></tr><tr><td>path</td><td>Absolute path files will be written too</td><td>String</td><td>true</td></tr><tr><td>maxFilesize</td><td>Maximum file size before new file is created</td><td><p>Long</p><p>Default: 67108864 (64MB)</p></td><td>false</td></tr><tr><td>batchSize</td><td>Number of events to trigger a file write process</td><td><p>Integer</p><p>Default: 1024</p></td><td>false</td></tr><tr><td>formatter</td><td>Target format of event data</td><td>See <a href="../serialisers/serialisation/">Serialisation documentation</a></td><td>true</td></tr></tbody></table>
