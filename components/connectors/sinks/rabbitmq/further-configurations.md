---
description: Further RabbitMQ distribution configurations
---

# Further configurations

{% hint style="info" %}
All examples assume default values when not applied
{% endhint %}

## Queue based eventing

In this example, a simple RabbitMQ publisher is set up to send JSON-formatted `StreamEvents` directly to a named queue. This approach is used for **point-to-point event delivery**, where each event is consumed by a single consumer from the queue.

Queue-based eventing ensures that each message is processed once, providing reliable message handling.

```yaml
rabbitPublisher:
  queue: quote_queue

  serializer:
    formatter:
      jsonFormatter:
        encoding: UTF-8
```

## Work Queues

This example demonstrates publishing to a Worker Queue, which distributes time-consuming tasks across multiple consumer workers. Each worker **processes a manageable number of tasks** from the queue, ensuring efficient load balancing and task distribution.

Work queues provide fine-grained control over message acknowledgment and worker management.

```yaml
rabbitPublisher:
  queue: quote_queue
  messageExpiration: 5000

  serializer:
    formatter:
      jsonFormatter:
        encoding: UTF-8
```

## Publish / Subscribe

In this example, RabbitMQ is configured for the publish / subscribe pattern using a **fanout exchange**. Multiple consumers receive the same events from an exchange without needing specific routing keys. This setup allows all consumers bound to the exchange to receive the same message.

Since fanout exchanges broadcast messages to all bound queues, there’s no need to specify routing keys.

```yaml
rabbitPublisher:
  exchange:
    name: quotes_exchange
    type: FANOUT

  serializer:
    formatter:
      jsonFormatter:
        encoding: UTF-8
```

## Routing

This example configures RabbitMQ for **topic-based routing**. Events are published using routing keys, allowing consumers to selectively subscribe to specific events. Routing keys are dynamically created based on fields in the event.

In this case, the routing key is created using the `market` and `symbol` fields, allowing targeted event distribution.

```yaml
rabbitPublisher:
  host: localhost
  exchange:
    name: quotes_exchange
    type: TOPIC

  routing:
    event fields:
      - market
      - symbol

  serializer:
    formatter:
      jsonFormatter:
        encoding: UTF-8
```

## Topic

This example is similar to the previous cases where we have published events using routing keys. The difference here is, that it allows consumers to use wildcards in routing keys to subscribe to a broader set of events. The **topic exchange** type enables flexible routing based on patterns with wildcards (`*` and `#`).

Consumers can subscribe to events for any market and stock symbol, such as `*.IBM` or `*.MSFT`.

See [RabbitMQ source](../../sources/rabbitmq/) documentation for further information.

```yaml
rabbitPublisher:
  host: localhost
  exchange:
    name: quotes_exchange
    type: TOPIC

  routing:
    event fields:
      - market
      - symbol

  serializer:
    formatter:
      jsonFormatter:
        encoding: UTF-8
```
