---
icon: envelope
description: Send critical email alerts to key stakeholders
---

# Email

## Overview

The **Email transport** provides the ability to send alerts as an email which can drive further processing.

{% hint style="info" %}
Note this is an incubating feature which is subject to change with respect to feature requests
{% endhint %}

## Example & DSL attributes

This example configures the **Email transport** to send IoT status notifications to a single recipient using the [mailtrap](https://mailtrap.io) solution.

```yaml
email publisher:
  name: iot_email_notification
  email:
    subject: IoT Valve Notification
    message template: Toggling sensor {sensorId} humidifier valve state to {state} (1:ON | 0:OFF)
    from: sender@domain.com
    recipients:
      - recipient@domain.com

  smtp:
    host: sandbox.smtp.mailtrap.io
    port: 25
    enable auth: true
    enable starttls: true
    ssl trust: sandbox.smtp.mailtrap.io
```

### Attributes schema

<table><thead><tr><th width="150">Attribute</th><th width="391">Description</th><th width="130">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Logical name for the configuration</td><td><p>String</p><p></p></td><td>true</td></tr><tr><td>email</td><td>Configure the message with template and recipients etc;</td><td>See <a href="email.md#email-attributes-schema">Email attributes schema</a></td><td>true</td></tr><tr><td>smtp</td><td>Configure the SMTP service </td><td>See <a href="email.md#smtp-attributes-schema">SMTP attributes schema</a></td><td>true</td></tr></tbody></table>

### Email attributes schema

<table><thead><tr><th width="165">Attribute</th><th width="367">Description</th><th width="135">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>subject</td><td>Slack api <strong>bot</strong> token. This is provided either within the configuration file or as the <code>SLACK_API_TOKEN</code> environment variable.</td><td>String</td><td>true</td></tr><tr><td>message template</td><td>Message template that applies the selected attributes from an emitted event to the message i.e. {sensorId} is replaced by the corresponding value in the event.<br><br><strong>Note</strong> HTML can be embedded within the template for richer email content.</td><td>String</td><td>true</td></tr><tr><td>from</td><td>Valid from email address. Often this will be a system level email address</td><td>String</td><td>true</td></tr><tr><td>recipients</td><td>List of valid recipient email addresses</td><td>List</td><td>true</td></tr><tr><td>recipient type</td><td>Optional attribute to send the email as 'TO', 'CC' or 'BCC'</td><td>String<br>Default: TO</td><td>false</td></tr><tr><td>content type</td><td><p>Optional message content type. Default: </p><pre><code>text/html; charset=utf-8
</code></pre></td><td>String</td><td>false</td></tr></tbody></table>

### SMTP attributes schema

{% hint style="warning" %}
SMTP service authentication username and password credentials are provided as system environment variables not within the DSL. \
\
Required variables to set within the executing environment:

JOULE\_SMTP\_USERNAME=xxxxxx\
JOULE\_SMTP\_PASSWORD=xxxxxx
{% endhint %}

<table><thead><tr><th width="163">Attribute</th><th width="364">Description</th><th width="138">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>host</td><td>smtp service host address</td><td><p>String</p><p></p></td><td>true</td></tr><tr><td>port</td><td>SMTP port</td><td>Integer<br>Default: 25</td><td>true</td></tr><tr><td>ssl trust</td><td>Address of the SSL trust service</td><td>String</td><td>true</td></tr><tr><td>enable auth</td><td>Enable user to authenticate before sending an email</td><td>Boolean<br>Default: true</td><td>false</td></tr><tr><td>enable starttls</td><td>Enable starttle protocol for all email traffic</td><td>Boolean<br>Default: true</td><td>false</td></tr></tbody></table>
