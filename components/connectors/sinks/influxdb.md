---
description: Standard time-series database idea for instrumentation or live KPI tracking
---

# InfluxDB

## Overview

**Time series database** support is provided using the **InfluxDB publisher connector.** Processed events are published to a designated InfluxDB organisation bucket using either the event or ingestion timestamp.

The transport is ideal for use cases such as **offline time series analytics, device monitoring and alerting**. It offers flexibility in how tags and measurements are configured, which are aligned with the final processed event in the pipeline.

Further details on [InfluxDB see the documentation](https://docs.influxdata.com/influxdb/v2/reference/urls/).

Some key features are:

1. <mark style="color:green;">**InfluxDB integration**</mark>\
   The transport stores data in an InfluxDB bucket, ideal for handling time series data.
2. <mark style="color:green;">**Independent configuration**</mark>\
   Tags and measurements can be configured separately and aligned with the processed events.
3. <mark style="color:green;">**Use cases**</mark>\
   Designed for time series analytics, device monitoring and alerting scenarios.

{% hint style="info" %}
**Driver details:** [com.influxdb:influxdb-client-java:6.5.0](https://mvnrepository.com/artifact/com.influxdb/influxdb-client-java/6.5.0)
{% endhint %}

## Example & DSL attributes

This example configures the InfluxDB transport to send data to a local instance at `http://localhost:8086`.

It stores data in the `banking` organisation and `nasdaqBaseAnalytics` bucket, with a 3-hour retention time.

`Gzip` compression is enabled and logging is set to `BASIC`. Events use the `symbol` as a tag and include three `Bollinger Band` values:

* `ask_upper_BollingerBand`
* `ask_middle_BollingerBand`
* `ask_lower_BollingerBand`

as `DOUBLE` measurements.

```yaml
influxdb:
  url: http://localhost:8086
  authToken: 1d892e1ab6f70b3b9b3d619899a847957033e76385c231e02892ebfa59f1791f
  organisation: banking
  bucket: nasdaqBaseAnalytics
  retentionTime: 10800
  enableGzip: true
  logLevel: BASIC
  formatter:
    useEventTime: true
    tags:
      - symbol
    measurements:
      ask_upper_BollingerBand: DOUBLE
      ask_middle_BollingerBand: DOUBLE
      ask_lower_BollingerBand: DOUBLE
```

### Attributes schema

Configuration parameters available for the InfluxDB publisher transport. The parameters are organised by order of importance, ranked from high to low.

<table><thead><tr><th width="163">Attribute</th><th width="217">Description</th><th width="246">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>url </td><td>InfluxDB server address</td><td><strong>http://&#x3C;ip-address>:port</strong></td><td>true</td></tr><tr><td>authToken</td><td>InfluxDB UI / CLI Authentication access token</td><td>String</td><td>true</td></tr><tr><td>username password</td><td><p>Authentication access details for v1.x InfluxDB. </p><p>Note: Only required if authToken has not been provided.</p></td><td>String</td><td>true</td></tr><tr><td>organisation</td><td>InfluxDB UI / CLI organisation token </td><td>String</td><td>true</td></tr><tr><td>bucket</td><td>Bucket where event measurements are persisted</td><td>String</td><td>true</td></tr><tr><td>formatter</td><td>See Formatter section below</td><td>See Formatter section below</td><td>true</td></tr><tr><td>enableGzip</td><td>Compress batched data to improve I/O transfer for large data loads</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>retentionTime</td><td>Number of seconds to retain measurements</td><td>Seconds as a integer value Default: 3600 seconds</td><td>false</td></tr><tr><td>loglevel</td><td>InfluxDB logging level</td><td><p>Options: NONE, BASIC, BODY, HEADERS </p><p>Default: BASIC</p></td><td>false</td></tr></tbody></table>

### Formatter Attributes

This topic provides configuration parameters available for the formatter attribute. The parameters are organised by order of importance, ranked from high to low.

<table><thead><tr><th width="161">Attribute</th><th width="220">Description</th><th width="259">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>measurements</td><td>Map of event measurements to persist mapped to data type</td><td>Map String -> DOUBLE, FLOAT, LONG, INTEGER, SHORT, BYTE, BOOLEAN, STRING</td><td>true</td></tr><tr><td>tags</td><td>List of record tags for enable aggregate query processing</td><td>String list</td><td>false</td></tr><tr><td>useEventTime</td><td>Specifies which event timestamp to persist</td><td><p>Boolean:  Default: true</p><p>True: eventTimestamp </p><p>False: ingestionTimestamp</p></td><td>false</td></tr></tbody></table>
