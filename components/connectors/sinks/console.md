---
description: For testing purposes a console output processor is provided.
hidden: true
---

# Console

## Configuration Guide

#### Example configuration

```yaml
stdout: {}
```
