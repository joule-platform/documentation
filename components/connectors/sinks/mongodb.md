---
description: >-
  Publish JSON based events to the mature standard document storePublishes
  JSON-based events to MongoDB, a widely-used document store
---

# MongoDB

## Overview

**Document database support** is provided using the **MongoDB Publisher Transport.** Processed events are published to a specified MongoDB database collection.

The events are serialised using a [document formatter](mongodb.md#document-formatter) serialiser or a custom transformer implementation.

This transport is designed to facilitate efficient data publishing into MongoDB with customisable serialisation options.

Some key features are:

1. <mark style="color:green;">**MongoDB integration**</mark>\
   Sends processed events to a defined MongoDB collection.
2. <mark style="color:green;">**Serialiser options**</mark>\
   Uses a default Document Formatter serialiser or allows custom serialisation.

{% hint style="info" %}
**Driver details:** [org.mongodb:mongodb-driver-sync:4.8.1](https://mvnrepository.com/artifact/org.mongodb/mongodb-driver-sync/4.8.1)
{% endhint %}

## Examples & DSL attributes

This example sets up the MongoDB Publisher Transport to send events to a local MongoDB instance (`localhost:27017`).

It stores data in the `customerdb` database and `emea_customers` collection, using ordered inserts.

Authentication is done with the `joule` user and SCRAM-SHA-1 mechanism. Events are serialised individually (not in batches) with `Base64` encoding and `application/octet-stream` content type. SSL is disabled.

<pre class="language-yaml"><code class="lang-yaml"><strong>mongodbPublisher:
</strong>  servers:
    localhost: 27017
  enable ssl: false

  database: customerdb
  collection: emea_customers
  ordered inserts: true

  credentials:
    user: joule
    password: password
    mechanism: SCRAM_SHA_1

  serializer:
    batch: false
    formatter:
      documentFormatter:
        contentType: application/octet-stream
        encoding: base64
</code></pre>

### Attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>database</td><td>Name of database</td><td>String</td><td>true</td></tr><tr><td>collection </td><td>Name of collection inserts will be applied to</td><td>String</td><td>true</td></tr><tr><td>ordered inserts</td><td>Hint to MongDB to insert documents using presented ordering</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>servers</td><td>Map of servers with associated ports</td><td>Map&#x3C;String,Integer></td><td>true</td></tr><tr><td>enable ssl</td><td>Enable SSL</td><td><p>boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>credentials</td><td>Credential configuration</td><td>See <a href="mongodb.md#credential-attributes-schema">Credential attributes</a> section</td><td>true</td></tr><tr><td>serializer</td><td>Serialisation configuration</td><td>See <a href="mongodb.md#serialisation-attributes-schema">Serialisation attributes</a> section</td><td>false</td></tr></tbody></table>

### Credential attributes schema

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>username</td><td>Username</td><td>String</td><td>true</td></tr><tr><td>password</td><td>password</td><td>String</td><td>true</td></tr><tr><td>authentication database</td><td>Database to use to authenticate connected user</td><td><p>String</p><p>Default: admin </p></td><td>false</td></tr><tr><td>mechanism</td><td><p>Authentication mechanism</p><p></p><p>Options</p><ul><li>PLAIN</li><li>SCRAM_SHA_1</li><li>SCRAM_SHA_256</li></ul></td><td><p>String</p><p>Default: SCRAM_SHA_1</p></td><td>false</td></tr></tbody></table>

### **Serialisation** attributes schema

This topic provides configuration parameters available object serialisation process.&#x20;

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>transform</td><td>User provided implementation</td><td>CustomTransformer</td><td>false</td></tr><tr><td>formatter</td><td>This is used when a custom transformer is not provided. Useful when chaining Joule processes</td><td><p>Formatter</p><p>Default: documentFormatter (See Document Formatter)</p></td><td>false</td></tr><tr><td>contentType</td><td>Type of content to inform receiving application</td><td><p>String</p><p>Default: application/octet-stream</p></td><td>false</td></tr><tr><td>encoding</td><td>Payload encoding method</td><td><p>String</p><p>Default: base64</p></td><td>false</td></tr><tr><td>batch</td><td>Flag to batch multiple messages in to a single payload</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr></tbody></table>

### **Serialiser example**

The configuration below will serialise `StreamEvents` as MongoDB documents.

```yaml
mongodbPublisher:
  ...

  serializer:
    batch: false
    formatter:
      documentFormatter:
        contentType: application/octet-stream
        encoding: base64
```

## Document formatter

The **document formatter** in Joule is a built-in tool that converts `StreamEvents` into MongoDB's **Document** type.

This allows the processed events to be **formatted correctly before being inserted** into a MongoDB collection. The formatter ensures that the data is serialised in a way that MongoDB can **understand and store efficiently**.

## Additional resources

* Official MongoDB [documentation](https://www.mongodb.com/docs/)
* Official MongoDB [docker image](https://hub.docker.com/\_/mongo)
* Mongo Compass [UI](https://www.mongodb.com/products/compass)&#x20;
