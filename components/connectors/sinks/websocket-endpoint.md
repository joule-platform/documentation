---
description: >-
  Publishes processed events to web clients via WebSocket transport, ideal for
  real-time dashboards and applications
---

# WebSocket endpoint

## Overview

The **WebSocket Publisher Transport** enables real-time event streaming to web clients, ideal for dashboards and interactive applications. Processed events are published as Json to subscribing client processes.

The example WebSocket client provided connects to a local WebSocket server (`ws://localhost:7070/joule/ws/stream/quotes`) and logs incoming event data to the browser console, allowing users to verify the streaming functionality.

{% hint style="info" %}
**Client library:** [io.javalin:javalin:5.6.3](https://mvnrepository.com/artifact/io.javalin/javalin/5.6.3)
{% endhint %}

## Examples & DSL attributes

This simple example leverages the default setting resulting in events published as `StreamEvent` JSON object to connected clients.

```yaml
websocketPublisher:
  topic: quotes
```

### Example client

This HTML page acts as a WebSocket client to test data streaming from a Joule server.

Open it in Chrome, go to **Developer Tools > Console**, and click `Start Streaming` to view live event data streamed from `ws://localhost:7070/joule/ws/stream/quotes`.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WebSocket Client</title>
</head>

<body>
    <button onclick="contactServer">Start Streaming</button>
</body>

<script>
    const socket = new WebSocket('ws://localhost:7070/joule/ws/stream/quotes');
    socket.addEventListener('open', function (event) {
        socket.send('Connection Established');
    });

    socket.addEventListener('message', function (event) {
        console.log(event.data);
    });

    const contactServer = () => {
        socket.send("Initialize");
    }
</script>
</html>
```

### Attributes schema

<table><thead><tr><th width="150">Attribute</th><th width="307">Description</th><th width="185">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>topic</td><td>User defined topic this stream relates too</td><td>String</td><td>true</td></tr></tbody></table>
