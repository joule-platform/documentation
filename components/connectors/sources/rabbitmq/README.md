---
description: AMQP messaging solution ideal for IoT and client / server use cases
---

# RabbitMQ

## Overview

RabbitMQ is a lightweight and easy-to-deploy messaging platform that supports multiple messaging protocols. It can be configured in distributed and federated setups to meet high-scale and high-availability requirements.

Joule enables publishing to RabbitMQ queues, exchanges and topics, providing flexible event-driven architectures.

Additionally, the page highlights the usage of Joule to consume events via RabbitMQ messaging subscriptions.

{% hint style="info" %}
**Client library**: [com.rabbitmq:amqp-client:5.16.0](https://mvnrepository.com/artifact/com.rabbitmq/amqp-client/5.16.0)
{% endhint %}

## Examples & DSL attributes

This example shows how to configure a RabbitMQ consumer that subscribes to specific event types using routing keys with a topic exchange.

The consumer listens for events related to quotes for specific stocks (IBM, MSFT and GOOG) from the `marketQuotes` exchange.

The events are deserialised from a `StreamEvent` JSON object into a `Joule StreamEvent` object using a custom parser.

```yaml
rabbitConsumer:
  host: localhost
  exchange:
    name: marketQuotes
    type: TOPIC
  
  routing:
    keys:
      - NASDAQ.IBM
      - NASDAQ.MSFT
      - NASDAQ.GOOG
  
  deserializer:
    parser: 
      com.fractalworks.examples.banking.data.QuoteToStreamEventParser
```

### Attributes schema

<table><thead><tr><th width="214">Attribute</th><th width="217">Description</th><th width="215">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>host</td><td>RabbitMQ server hostname or Ip address</td><td><p>String</p><p>Default: localhost</p></td><td>false</td></tr><tr><td>port </td><td>RabbitMQ server port, must be greater than 0</td><td><p>Integer</p><p>Default: 5672</p></td><td>false</td></tr><tr><td>username</td><td>User name</td><td><p>String</p><p>Default: guest</p></td><td>false</td></tr><tr><td>password</td><td>password</td><td><p>String</p><p>Default: guest</p></td><td>false</td></tr><tr><td>virtualHost</td><td><p>Creates a logical group of connections, exchanges, queues, bindings, user permissions, etc. within an instance</p><p></p><p>See <a href="https://www.cloudamqp.com/blog/what-is-a-rabbitmq-vhost.html">vhost documentation</a> for further information</p></td><td><p>String</p><p>Default: /</p></td><td>false</td></tr><tr><td>autoAck</td><td>Flag to consider if server should message acknowledged once messages have been delivered. False will provide explicit  message acknowledgment</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>prefetchCount</td><td>QoS -maximum number of messages that the server will deliver, 0 if unlimited</td><td><p>Integer</p><p>Default: 1</p></td><td>false</td></tr><tr><td>global</td><td>QoS - true if the settings should be applied to the entire channel rather than each consumer</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>durable</td><td>Set a durable queue (queue will survive a server restart)</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>autoDelete</td><td>Server deletes queue when not in use</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>exclusive</td><td>Exclusive queue to this connection</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>arguments</td><td>Additional queue properties</td><td><p>Map&#x3C;String, Object></p><p>Default: null</p></td><td>false</td></tr><tr><td>exchange</td><td>Exchange configuration</td><td>See <a href="./#exchange-attributes">exchange sttributes</a> section</td><td>false</td></tr><tr><td>routing</td><td>Routing configuration</td><td>See <a href="./#routing-attributes">routing attributes</a> section</td><td>false</td></tr><tr><td>deserializer</td><td>Deserialisation configuration</td><td>See Deserialisation <a href="../../serialisers/deserialisers/">documentation</a></td><td>false</td></tr></tbody></table>

### **Exchange attributes schema**

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Name of exchange</td><td>String</td><td>true</td></tr><tr><td>type</td><td>In RabbitMQ, messages are published to an exchange and depending on the type of exchange, the message gets routed to one or more queues</td><td><p>TOPIC, DIRECT, FANOUT, HEADERS</p><p>Default: TOPIC</p></td><td>false</td></tr><tr><td>arguments</td><td>Additional binding properties</td><td><p>Map&#x3C;String, Object></p><p>Default: null</p></td><td>false</td></tr></tbody></table>

### **Exchange example**

```yaml
rabbitConsumer:
    ...
    exchange:
        name: marketQuotes
        type: DIRECT
```

### **Routing attributes schema**

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>keys</td><td><p>Consumers can selectively consume events sent to a <code>topic</code> exchange by specifying one or more words delimited by dots. <a href="further-rabbitmq-examples.md#topic">Learn more about how to apply it here.</a></p><p></p><p>Valid key format examples: 'nasdaq.ibm', 'dept.inventory.bolts'.</p><p></p><p>There are two important special cases to define keys:</p><ul><li><code>*</code>(star) can be substituted for exactly one word</li><li><code>#</code>(hash) can be substituted for zero or more words</li></ul></td><td><p>String</p><p>Default: All</p><p>Constraints: Any number of dot separated words to a limit of 255 bytes</p></td><td>false</td></tr></tbody></table>

### **Routing example**

This example will subscribe to all events that match the first component of the key `'NASDAQ'`

```yaml
rabbitConsumer:
    ...
    routing:
        keys:
          - NASDAQ.#
```

## Additional resources

Further AMQP resources can be found on the following links:

* Official [RabbitMQ documentation](https://www.rabbitmq.com/documentation.html)
* CloudAMQP [documentation](https://www.cloudamqp.com/docs/index.html)
