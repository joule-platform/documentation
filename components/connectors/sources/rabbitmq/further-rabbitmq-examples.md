---
description: Further collection of examples using RabbitMQ
---

# Further RabbitMQ configurations

{% hint style="info" %}
All examples assume default values when not applied
{% endhint %}

## Queue based eventing

In this example, a simple RabbitMQ consumer is set up to subscribe directly to events from a queue. This approach is used for **point-to-point event delivery**, where each event is consumed by a single consumer from the queue.

Queue-based eventing ensures that each message is **processed once by a consumer**, providing reliable message handling.

```yaml
rabbitConsumer:
  queue: point_to_point_queue
  ...
```

## Work Queues

In this example, RabbitMQ is used to implement work queues, which **distribute time-consuming tasks** among multiple consumer workers.

This setup ensures that tasks are **processed efficiently** by a pool of workers, with each worker receiving a manageable number of tasks from the queue. It also provides **fine-grained control** over message acknowledgment and task distribution.

```yaml
rabbitConsumer:
  queue: worker_queue
  autoAck: false
  durable: true
  prefetchCount: 1
  ...
```

## Publish / Subscribe

In this example, RabbitMQ is configured to use the **publish / subscribe** pattern, where multiple consumer processes receive the same message. This is achieved using a **fanout exchange**, which broadcasts messages to all bound queues without requiring specific routing keys.

Since the `fanout` exchange type sends a copy of each message to all queues bound to it, there’s no need to specify routing keys. Every consumer connected to the exchange will receive the same event.

```yaml
rabbitConsumer:
  exchange:
    name: quotes_exchange
    type: FANOUT    
  ...
```

## Routing

In this example, RabbitMQ is configured to use **direct routing**, where each consumer subscribes only to a specific subset of messages based on the routing keys. This allows for more selective message consumption, meaning that the consumer will only receive the events it is interested in, based on its assigned routing keys.

1. <mark style="color:green;">**Exchange type**</mark>\
   The `quotes_exchange` is of type `DIRECT`, which routes messages based on exact matching of routing keys.
2. <mark style="color:green;">**Routing keys**</mark>\
   In this case, the consumer subscribes to two specific routing keys: `NASDAQ.IBM` and `NASDAQ.MSFT`. This means the consumer will only receive messages related to these two stock quotes.

<pre class="language-yaml"><code class="lang-yaml"><strong>rabbitConsumer:
</strong>  host: localhost
  exchange:
    name: quotes_exchange
    type: DIRECT

  routing:
    keys:
      - NASDAQ.IBM
      - NASDAQ.MSFT
  ...
</code></pre>

## Topic

In this example, RabbitMQ uses a **topic exchange** to allow consumers to subscribe to a wider set of events using **wildcards**.

The **topic exchange** type enables more flexible routing compared to direct exchanges, allowing the consumer to use patterns with the dot notation to match multiple routing keys.

1. <mark style="color:green;">**Exchange type**</mark>\
   The `quotes_exchange` is of type `TOPIC`, which allows the use of routing key patterns with wildcards (`*` and `#`).
2. <mark style="color:green;">**Routing keys**</mark>\
   The consumer subscribes to routing keys with wildcards to receive all quotes for specific stocks. In this case, `*.IBM` and `*.MSFT` will match all events for any market venue related to IBM and MSFT.
   1. The `*` wildcard matches exactly one word in the routing key.
   2. This allows the consumer to receive market venue quotes for both IBM and MSFT from any market source (e.g., NASDAQ, NYSE).

<pre class="language-yaml"><code class="lang-yaml"><strong>rabbitConsumer:
</strong>  host: localhost
  exchange:
    name: quotes_exchange
    type: TOPIC

  routing:
    keys:
      - "*.IBM"
      - "*.MSFT"
  ...
</code></pre>
