# Session management

## Overview

The Session Expiry Interval is a parameter a client sets during the CONNECT packet stage, specified in seconds. This parameter indicates the duration the broker retains the client’s session information. If this interval is set to zero, or if the CONNECT packet does not specify an expiry value, the session data is promptly deleted from the broker as soon as the client’s network connection terminates. Notably, the maximum session expiry interval is `UINT_MAX` (4,294,967,295), enabling an offline session to persist for an extended duration of just over 136 years following client disconnection, reference [HiveMQ](https://www.hivemq.com/blog/mqtt-essentials-part-9-last-will-and-testament/).

The feature is applicable to both of the consumer and publisher transports.

### Example

```
// Some code
mqttConsumer:
 ...
  session expiry interval: 15
  clean start: false
```

### Session management attributes schema

These are optional attributes to control the behaviour of client session state management.

**Note:** To use client recoverable sessions set expiry above zero and set the `clean start` attribure to false.

<table><thead><tr><th width="179">Attribute</th><th width="383">Description</th><th width="303">Data Type</th></tr></thead><tbody><tr><td>session expiry interval</td><td>The maximum time in seconds the broker will hold the session state for a client.</td><td><p>Long</p><p>Default: 0 seconds</p></td></tr><tr><td>clean start</td><td>Provide a clean restart (i.e. create a new session for the connected process). </td><td><p>Boolean</p><p>Default: true</p></td></tr></tbody></table>

