---
icon: binary-circle-check
description: Get data ready for consumption serialising incoming and outgoing data
---

# Serialisers

## Overview

Incoming and outgoing data requires mapping to appropriate formats to support **seamless processing and distribution** within Joule. **Serialisers facilitate this process** by efficiently translating between source and target data types.

> In computing, serialization (or serialisation) is the process of translating a data structure or object state into a format that can be stored (e.g. files in secondary storage devices, data buffers in primary storage devices) or transmitted (e.g. data streams over computer networks) and reconstructed later (possibly in a different computer environment).
>
> [_wikipedia_](https://en.wikipedia.org/wiki/Serialization)

Joule processes incoming data by converting it from the source format into a `StreamEvent`.

`StreamEvents` can be **transformed back** into domain-specific data types, making them ready for consumption by other systems.&#x20;

### Supported tools

1. <mark style="color:green;">**Developer SDK**</mark>\
   Ability to write, deploy and reuse custom domain parsers and transformers.
2. <mark style="color:green;">**AVRO support**</mark>\
   Support to read and write domain-specific data types.
3. <mark style="color:green;">**Native StreamEvent**</mark>\
   Binary and JSON `StreamEvent` interoperability support between Joule processes.

## Available serialising options

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden></th><th data-hidden data-card-cover data-type="files"></th><th data-hidden data-card-target data-type="content-ref"></th><th data-hidden data-type="rating" data-max="5"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Serialisation</strong></mark></td><td>Data emitted to downstream systems is serialised using provided or custom serialisers</td><td></td><td></td><td></td><td><a href="serialisation/">serialisation</a></td><td>null</td></tr><tr><td><mark style="color:orange;"><strong>Deserialisation</strong></mark></td><td>Consumed data is mapped to internal <code>StreamEvents</code> using provided or custom deserialisers</td><td></td><td></td><td></td><td><a href="deserialisers/">deserialisers</a></td><td>null</td></tr></tbody></table>
