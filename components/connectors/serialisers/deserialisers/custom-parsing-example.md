---
description: >-
  Custom parser converts a domain Quote object into an internal Joule
  StreamEvent
---

# Custom parsing example

## Objective

The objective of this example is to demonstrate how to implement a custom `StreamEventParser` to convert domain-specific objects, such as a `Quote`, into the required Joule `StreamEvent` object ready for processing.

### Example

```java
@JsonRootName(value = "quote parser")
public class QuoteToStreamEventParser implements StreamEventParser {

    public QuoteToStreamEventParser() {
        // Required
    }

    @Override
    public Collection<StreamEvent> translate(Object o) throws TranslationException {
        Collection<StreamEvent> events = null;
        if(o instanceof Quote){
            Quote quote = (Quote) o;
            StreamEvent event = new StreamEvent("quote");
            event.setEventTime(quote.time());
            event.addValue("symbol", quote.symbol());
            event.addValue("bid", quote.bid());
            event.addValue("ask", quote.ask());
            event.addValue("volatility", quote.volatility());
            event.addValue("volume", quote.volume());
            event.addValue("date", quote.date());

            events = Collections.singletonList( event);
        }
        return events;
    }
}
```

## Explanation

The example demonstrates a custom implementation of the `StreamEventParser` interface, specifically designed to convert a `Quote` object into a `StreamEvent`.

### Parser Annotation

The class annotation defines the parser DSL element, indicating the parser's role in converting `Quote` objects. It aids in custom serialisation settings. This is required.

### Constructor

The constructor is required but does not contain any specific logic for this example.

### Translate method

The core method that converts the input object into a collection of `StreamEvent` objects.

This example the method checks if the input object is an instance of `Quote`. If not, it returns `null`, indicating non-valid conversion.&#x20;

Otherwise, the quote attributes are mapped in to a single  `StreamEvent` which is then placed in to a `Collections.singletonList()` and returned as a collection, as required by the method's signature.

```java
@Override
public Collection<StreamEvent> translate(Object o) throws TranslationException {
    Collection<StreamEvent> events = null;
    if(o instanceof Quote){
        ...
    }
}
```

### Quote attributes description

| Attributes | Description                          | Type        |
| ---------- | ------------------------------------ | ----------- |
| eventTime  | The time the quote was recorded      | Date / time |
| symbol     | The symbol of the asset being quoted | String      |
| bid        | The bid price of the asset           | Double      |
| ask        | The ask price of the asset           | Double      |
| volatility | The volatility of the asset          | Double      |
| volume     | The volume of trades for the asset   | Double      |
| date       | The date the quote was issued        | Date        |
