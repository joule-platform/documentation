---
icon: circle-nodes
description: Out-of-the-box Joule provides key data connectors for streaming use cases
---

# Connectors

## Overview

Data connectors, sources and sinks are a key component within the Joule low-code ecosystem that **consume and distribute data**.&#x20;

To get you started processing data Joule provides a catalog of widely used standard connector solutions. For example, Joule supports real-time data **streaming and batch processing** connector integrations with major industry standards such as Kafka, MQTT and RabbitMQ, facilitating seamless data transfer across diverse applications and systems.

If any of the provided connectors do not fit your requirements, Joule makes it possible to build your own connector by leveraging the [**Connector API**](../../developer-guides/builder-sdk/connector-api/). This SDK offers customisable templates and a robust API, empowering business developers to create connectors tailored to their unique data sources or destinations.

## Key features of Joule's connectors capability include

1. <mark style="color:green;">**Streaming**</mark> \
   Standard industry messaging connectors such as Kafka, MQTT and RabbitMQ are provided for real-time use cases.&#x20;
2. <mark style="color:green;">**Batch processing**</mark>\
   Joule does not differentiate between batch and real-time data; All incoming batch data is converted to stream events and outgoing can be either batched or streamed.
3. <mark style="color:green;">**Additive connector features**</mark>\
   Where ever possible additional features have been added to connectors such as wire compression, auto data type translations, change listeners etc.
4. <mark style="color:green;">**Flexible transformation**</mark>\
   Ingesting and publishing requires some level of data transformation complexity. Standard formats such as JSON, CSV, Parquet, AVRO and the ability to define custom transformer, (de)serialisation code are all supported.

## Types of connectors & serialisation

{% hint style="info" %}
Build your own connectors using the [**Connector API**](../../developer-guides/builder-sdk/connector-api/) and quick start templates
{% endhint %}

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Sources</strong></mark></td><td>Connect to key data sources to drive use cases</td><td></td><td><a href="sources/">sources</a></td></tr><tr><td><mark style="color:orange;"><strong>Sinks</strong></mark></td><td>Distribute insights to consuming systems</td><td></td><td><a href="sinks/">sinks</a></td></tr><tr><td><mark style="color:orange;"><strong>Serialisers</strong></mark></td><td>Get data ready for consumption serialising incoming and outgoing data</td><td></td><td><a href="serialisers/">serialisers</a></td></tr></tbody></table>
