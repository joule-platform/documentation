---
description: All you need to get started with Joule.
---

# Getting started

{% hint style="info" %}
The tutorial is built against **Version 1.2.1-SNAPSHOT** of the platform.
{% endhint %}

## Overview

### What this tutorial is about

This tutorial aims to teach how to run a real-time banking example with your first Joule.

### Who this tutorial is intended for

We have tailored this tutorial for anyone who wants to learn how to use Joule for the first time and who wishes to learn how to deploy a first use case given that your environment is setup already.

{% hint style="success" %}
Build your environment with [this tutorial](../developer-guides/builder-sdk/environment-and-setup/environment-setup.md).
{% endhint %}

### What we will learn

By the end of this tutorial, we will have learned:

1. How to start and stop Joule.
2. Deploy a first real-time use case.
3. Access the Redpanda interface to see the deployed use case in action.

## Prerequisites

1. Understanding of how to navigate with the command line through directories. Not sure how to navigate with command line? [Follow this tutorial](https://www.freecodecamp.org/news/command-line-for-beginners/).
2. Java is installed. Don’t have Java? [Follow this tutorial](https://www.graalvm.org/downloads/).

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXcBoSBuBcdwu4pCbDUyMvEnsriqrQGFHMx2H1ziKhiP-f9P453dx5wJ1bMJENk_6ybhFo6rz7DSTTZJjRe9C_aDgAYTL0aSbaAD4oMX7tUyjSpUluHTThyon2UpXdyku5kYCFC8wTEZo7daM2NYMj5EiI0?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Java version needed</p></figcaption></figure>

3. Postman is installed. Don’t have Postman? [Follow this tutorial](https://www.postman.com/downloads/).
4. Docker or any other alternative to run containers.
5. Git is installed. Don’t have Git? [Follow this tutorial](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

#### Optional

Only install the below if you do not want to run Docker images of vendor software:

1. Redpanda is installed. Don’t have Redpanda? [Follow this tutorial](https://docs.redpanda.com/current/get-started/rpk-install/).

## Implementation

### Step 1 - Start up our environment

The following steps will guide us on setting up our environment to deploy our images and containers with Joule.

1. We will clone the repository with this command.

{% code fullWidth="false" %}
```sh
git clone 
https://gitlab.com/joule-platform/fractalworks-stream-gettingstarted/
```
{% endcode %}

2. Once cloned, we will checkout to dev.

```sh
git checkout dev
```

3. We will navigate to the `quickstart` directory.
4. We will make sure that Docker or our alternative is running.

We are now ready to start building Joule. To start Joule, we need to run the following command which will start building our images and containers.

{% hint style="info" %}
```bash
Joule needs to run from the root directory as an example:

pi@raspberrypi:~/projects/fractalworks-joule-platform $ ./bin/startJoule.sh
```
{% endhint %}

In the case of this use case, we will run the following command from the `~/quickstart` location:

```bash
./startupJoule.sh
```

The command will start up the following containers ready for use case deployment:

* Joule Daemon
* Joule Database
* Redpanda

#### We should expect this output

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXcIKvYDFI0q38fkcO-n5CSEZZspEJZRC3PqASJXKlhG8qnzdvORJqqArqqJgyq6nkyA3nzlUasGHlgpjo0-Io9x1pG_XNHT3-MEIrJGexO4yveUmr-7lobO3k_BHKa4egebAhCxjxFIfDcvljyEwy23biHm?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Expected result when images built</p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (3).png" alt=""><figcaption><p>Expected result when containers built</p></figcaption></figure>

### Step 2 - Run the Getting Started example

The banking demo directories provides a set of examples in the form of a postman collection and environment. These can be found under the `examples` directory.

We will now get started running the banking `Getting Started` demo example using Postman.

1. First we will import the use case demos and environment files from the banking-demo `examples` directory.

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXf5JHkc7_Tfc2MvnqIHK1MRd-upreyDdiqg9gYM302t29uE_Ib9PDUeT-LpBm7NzjJugiik1BekSneb4pzot00AJoqarjYWXcmWchEwyCumPP6LWzmWvQsn62-25KqOEkuc4C3zWqj6hOsbvvP-NlvZ6AHJ?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Postman collection</p></figcaption></figure>

2. We will set the Postman environment to `Joule`.

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXeGCvg5cfVNCGRBxOEdGBL89RoDUEvgVeLXKv7gyXyK03Y4NCUzZcB6NvnvHNhHNzFGhT0m6zUzghkOJwRtyzKzAZCsMWkThvKlwB3WvvrUMY9cvG__MSRZAwDesAfc-faHhNcSA4G5V98jkMs7-1K8yPg?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Setting the environment</p></figcaption></figure>

3. From the `Getting started \ Deploy` folder click `Run folder` from the menu.

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXdwWf2umFzS0E3gvzzOCQRWuO_iAdsH4kV9DuObwsIzt05XtOHw1-E5LbbVBAyW8iRkTU7v44tdS8uOjzHT3_0pdbGvERo2GKxv-pTJYktY-eS5s9qpN7DGSL8UWkOKW3aH6jX7Fl8tyQbiVs3fJHvW2tA?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Deploying the example</p></figcaption></figure>

4. Finally execute the run order by clicking the `Run Joule - Banking demo` button.

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXdwanPsEUz3AnAD2L_EJ1muFXV6n1cF5MYvXoYKpXH4AQiXYGSqqLy4m93Nn3hTw-dlpmYPKqZKvNigoZWSBsT1Zqin-y16NC5qsVeZRqmzP3QOTakQBMh6dTCnjt0mvU1pwmATFiiH-nNcfsb-aMrz3jDb?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Running the example</p></figcaption></figure>

This will deploy the source, stream, sink and the use case binding definition to the platform.

{% hint style="info" %}
Note on a restart these setting will be rehydrated and will automatically start.
{% endhint %}

#### We should expect this output

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXeP8_KOtQ040swLPjz7GStiWgPz7zrm6PQwYOu74WxhSdgDBrwLHD-jWE8LljsfQhLtFGWyBqLMWvlODKKNfg2DUHQYCINFlCKGseraUToKTnXWeAAXQY0DrNobTbYH-2VExdm6E3mePlhYR9KXbYVTTTXM?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Expected result in Postman</p></figcaption></figure>

### Step 3 - Will start the quote publisher

The banking demo is provided with a quote simulator which is seeded using

```bash
./bin/startQuotePublisher.sh
```

This command will generate a continuous set of quote and publish them to the `quotes` topic. We will see how to access the topic on the next step.

#### We should expect this output

Press `return` to continue.

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXcM1NujkCE6scyzvbc-EIE7YFMv3eafbsUZS5NstWcs3AfU1s9qPShrW6WFVUFMf2Waf7r85fDrxIAEzLwHzz6hvpXLsztUi4FEVNyqDh9rBAYXantOogSRaR9OjcDsE_ZzcZiGrXcXO_1QcxYx1j6Dze4t?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Expected result when the quote publisher has started</p></figcaption></figure>

### Step 4 - Now review our use case outputs

Once the processing pipeline completes its tasks, the resulting events are published on to the `analytics_view` topic ready for further consumption (i.e. distribution, dashboard, dynamic portfolio risk measures etc).

To learn how to navigate the interface, [look at this tutorial](https://www.redpanda.com/redpanda-console-kafka-ui).

<table data-card-size="large" data-column-title-hidden data-view="cards" data-full-width="false"><thead><tr><th></th><th data-hidden></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td>Access the Redpanda interface</td><td></td><td></td><td><a href="http://localhost:8080/topics">http://localhost:8080/topics</a></td></tr></tbody></table>

#### We should expect this output

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXf9q2ohcASfljCNvp9i-gvPphrK1jLaCCPukooZek9pOVjwdwcliHMTgCF8ae5SNPvDb7kezxPYqMdZlgFXsPkCAzDU61wwO31JtFAYHPZ21cdA-Bj2aEFDMqn_yWSK_smyPvcbrYcKGwmPSr0iq7Z3OM8l?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Expected result when Redpanda interface loaded</p></figcaption></figure>

### Step 5 - Stop the quotes simulator

Once we are ready to shut down Joule, we will first stop the quotes publisher by executing the command below.

```bash
./bin/stopQuotePublisher.sh
```

We can now undeploy the use case and its linked components by executing the `Run Joule - Banking demo` under the `Undeploy` use case folder.

In case we want to execute **complete shut down of Joule**, we can instead follow the steps below.

#### We should expect this output

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXfGdd8r18p7Uxdt9NhA9yWlspWrsVh3_SyFFykAxYZUfzvVksTJF64sZ72FH9PrrKf8VzoVTq8fmY5RiUvYD8IsKcsAEYTQHGPb-2I_DOHwPN4KaK_TdW-oaUqOZ8n22Rt7hAdQ39tXvOwNWxpTMNJbTbMp?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Expected result when the quote publisher has been stopped</p></figcaption></figure>

### Step 6 - Shut the environment down

Once quotes publisher has been stopped the Joule environment can now be shutdown. We have 2 options to shut Joule down.

{% tabs %}
{% tab title="Complete shutdown and clean environment" %}
We will execute the following command to shut down the environment.

```bash
./shutdownAndCleanJoule.sh
```

#### We should expect this output

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXdyfzuObQDg6WxCMFLk--V23tZddhw4tCcABDcYm1UwzDltAnxse6Vl5CCk5-o35yaFfg8po7ZtC6GievnpJn8msYg_GrWZUTYweaI2cNMqpAoucw288wZXHxffGojeWkf1CnFJp7zO8DmxPip0xiAInlg?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Expected result shutdown and clean Joule</p></figcaption></figure>
{% endtab %}

{% tab title="Only shutdown" %}
In case we want to shut down the environment and remove all the created directories, files and containers.

```bash
./shutdownJoule.sh
```

#### We should expect this output

<figure><img src="https://lh7-rt.googleusercontent.com/docsz/AD_4nXciQuzWffQ7TXHxPuLz_3q-Qh7r6B-JGuiqSBxJIECLjNLKjAYqVYDQVpSb1Dua6EpevocDDW_3bbv8Sf_Wsy49J6CCPz2pNkefDSn5tGjELXq80N42y7Hpzy_rhPWO8ze32dDH1wyc257Tyvkvr8UaN4pU?key=Wdx8MWBEj8y-rQeGTiRyfQ" alt=""><figcaption><p>Expected result only shutdown Joule</p></figcaption></figure>
{% endtab %}
{% endtabs %}

## What did we learn?

We covered essential steps to run our first Joule including cloning the repository and running a first streaming banking use case via Postman.

In summary we have learned:

1. How to publish continuous data streams with the quote simulator.
2. How to see our use case in execution in the Redpanda interface.
3. How to review and process analytics outputs through our default use case.
4. How to stop, shut down the Joule environment and clean up the system.

## What’s next?

Now that we have learned the basics about Joule, it is time to learn how to create our own use case.
