---
icon: newspaper
description: Learning-oriented step-by-step guides. Ideal to begin your journey with Joule.
---

# Tutorials

## Starting point

{% stepper %}
{% step %}
[Getting started](getting-started.md)

Learn to build a Joule locally and run a first banking **real-time streaming** use case.
{% endstep %}

{% step %}
[Build your first use case](../tutorials/build-your-first-use-case.md)

Learn to use **filters, enrichment, user defined alerts**, and data connectors.
{% endstep %}

{% step %}
[Stream sliding window quote analytics](../tutorials/stream-sliding-window-quote-analytics.md)

Define and deploy a set of **sliding window analytics** using OOTB analytic features.
{% endstep %}
{% endstepper %}

## Advanced tutorials

Progress beyond simple use cases by developing custom processors and analytics, use Joule's analytic capabilities and build ML prediction based pipelines.

{% stepper %}
{% step %}
[Custom missing value processor](../tutorials/advanced-tutorials/build-a-processor.md)

Build, deploy and apply a custom **missing value processor** to provide consistent values over time.
{% endstep %}

{% step %}
[Build an analytic user defined function](../tutorials/advanced-tutorials/stateless-bollinger-band-analytics.md)

Create a custom **Bollinger band implementation** using the analytics API  and apply it within a use case context.
{% endstep %}

{% step %}
Build a sink data connector

Create, deploy and use a custom sink data connector.
{% endstep %}

{% step %}
Build your first machine learning pipeline


{% endstep %}
{% endstepper %}
