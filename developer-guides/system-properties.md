---
description: Configure and enable core Joule features based on deployment needs
icon: rectangles-mixed
---

# System properties

## Overview

Joule internal setting can be custom configured to **suit the needs of your environment**.

This guide allows precise control over Joule’s setup for optimised deployment.

Key sections include:

1. <mark style="color:green;">**General configuration**</mark>\
   Set metadata and enable core services like the internal database and REST API.
2. <mark style="color:green;">**Service enabling**</mark>\
   Control features such as event lineage, internal database, and REST API.
3. <mark style="color:green;">**Database settings**</mark>\
   Configure Joule’s embedded DuckDB for metrics and state queries, including memory limits and thread usage.
4. <mark style="color:green;">**Rest API**</mark>\
   Customise REST API options, including file handling, security, and SSL settings.

## Example configuration

```properties
# General Rest API details 
joule.version=1.1.0
joule.contact.name=Lyndon Adams
joule.contact.email=enquiries@fractalworks.io
joule.contact.url=https://fractalworks.io

# Enable service switches
joule.lineage.enable=false
joule.internaldb.enable=true
joule.restapi.enable=true

# Internal database setting
joule.db.path=./db/joule.db
joule.db.memoryOnly=false
joule.db.memory_limit='512MB'
joule.db.access_mode=READ_WRITE
joule.db.threads=2
joule.db.worker_threads=2
joule.db.default_order='DESC'

# RestAPI setting
joule.rest.port=9080

# Directory to write files that exceed the in-memory limit
joule.rest.downloadPath=downloads

# The maximum individual file size allowed In GB
joule.rest.maxFileSize=2

# The maximum file size to handle in memory in MB
joule.rest.InMemoryFileSize=100

# The maximum size of the entire multipart request in GB
joule.rest.totalRequestSize=5

# SSL config
joule.rest.enableSSL=false
joule.rest.pemCertPath=./conf/cert.pem
joule.rest.keyCertPath=./conf/key.pem
```

### Joule CLI switch

Apply the below command line switch to the Joule process when using a custom `joule.properties` file. Setting within this file will override the default setting.

{% hint style="info" %}
To override the default setting provide a single configuration file and use the `-c` command line switch to specify the file path.
{% endhint %}

```
-c /app/joule/config/joule.properties
```

### Enable services

To enable services using default setting the following&#x20;

```properties
# Enable service switches
joule.lineage.enable=false
joule.internaldb.enable=true
joule.restapi.enable=true
```

### Event lineage&#x20;

Any event processor that changes the state of the passed event can have this action recorded within the event. There is a small memory overhead to keep changed state. &#x20;

```properties
joule.lineage.enable=false
```

This is currently an experimental feature which is undergoing further development to determine its usefulness within a stream processing context. Therefore it is set to disabled as a default setting.

### Internal database

Joule ships with an embedded database, [DuckDB](https://duckdb.org/), that provides core features such as metrics processing, event tapping and external state queries. This feature is enabled by default.

```properties
joule.internaldb.enable=true
```

See [internal database configuration](system-properties.md#internal-database-configuration) for further details.

### Rest API

Joule ships with an embedded Rest API that provides external query and Web socket APIs. See [Rest API](system-properties.md#rest-api-configuration) documentation for further details. This feature is enabled by default.

```properties
joule.restapi.enable=true
```

See [Rest API configuration](system-properties.md#rest-api-configuration) for further details.

## Internal database configuration

Joule ships with an in-memory database to support event capturing, metrics and ad-hoc queries for processors.&#x20;

### Example

```yaml
# Internal database setting
joule.db.path=./db/joule.db
joule.db.memoryOnly=false
joule.db.memory_limit=4GB
joule.db.access_mode=READ_WRITE
joule.db.threads=8
joule.db.default_order=DESC
```

### Attributes schema

<table><thead><tr><th>Property</th><th width="307">Description</th><th>Default</th></tr></thead><tbody><tr><td>joule.db.path</td><td>Relative path to the Joule process include database file name.</td><td><em>default: ./db/joule.db</em></td></tr><tr><td>joule.db.memoryOnly</td><td>Boolean flag set to true an <strong>in-memory database</strong> is created. Note that for an in-memory database no data is persisted to disk. </td><td><em>default: false</em></td></tr><tr><td>joule.db.memory_limit</td><td>The maximum memory of the database system (e.g. 1GB)</td><td><em>default: 512MB</em></td></tr><tr><td>joule.db.access_mode</td><td>Access mode of the database (<strong>AUTOMATIC</strong>, <strong>READ_ONLY</strong> or <strong>READ_WRITE</strong>).</td><td><em>default: READ_WRITE</em></td></tr><tr><td>joule.db.threads</td><td>The number of total worker threads used by the database.</td><td><em>default: 2</em></td></tr><tr><td>joule.db.default_order</td><td>The query set order type used when none is specified (<strong>ASC</strong> or <strong>DESC</strong>).</td><td><em>default: DESC</em></td></tr></tbody></table>

### Rest API Configuration&#x20;

Joule enables Rest API when the in-memory database is enabled. Dynamic endpoints are created to support data exports and paginated queries. See [Rest API documentation](../components/connectors/sources/rest-endpoints.md) for further information.

```properties
# RestAPI setting
joule.rest.port=9080

# Directory to write files that exceed the in-memory limit
joule.rest.downloadPath=downloads

# The maximum individual file size allowed In GB
joule.rest.maxFileSize=2

# The maximum file size to handle in memory in MB
joule.rest.InMemoryFileSize=100

# The maximum size of the entire multipart request in GB
joule.rest.totalRequestSize=5

# SSL config
joule.rest.enableSSL=false
joule.rest.pemCertPath=./conf/cert.pem
joule.rest.keyCertPath=./conf/key.pem
```

### Attributes schema

<table><thead><tr><th width="251">Property</th><th width="295">Description</th><th>Default</th></tr></thead><tbody><tr><td>joule.rest.port</td><td>Rest API port</td><td><em>default: 9080</em></td></tr><tr><td>joule.rest.downloadPath</td><td>Directory the system rest consumer will download files to ready for data ingestion<br>See <a href="../components/connectors/sources/rest-endpoints.md">Rest API</a> documentation</td><td><em>default: downloads</em></td></tr><tr><td>joule.rest.maxFileSize</td><td>The maximum individual file size allowed In GB</td><td><em>default: 2GB</em></td></tr><tr><td>joule.rest.InMemoryFileSize</td><td>The maximum file size to handle in memory in MB</td><td><em>default: 100MB</em></td></tr><tr><td>joule.rest.totalRequestSize</td><td>The maximum size of the entire multipart request in GB</td><td><em>default: 5GB</em></td></tr><tr><td>joule.rest.enableSSL</td><td><p>If you want to secure your Joule process with SSL set this flag to true.</p><p>Supported certificate formats are PKCS#12 (.p12) and PKCS#8 (.pem) and Java KeyStore (.jks).</p></td><td><em>default: false</em></td></tr><tr><td>joule.rest.pemCertPath</td><td>Certificate from a trusted CA or a self-signed certificate.</td><td><p><em>default:</em></p><p><em>/app/joule/certs/cert.pem</em></p></td></tr><tr><td>joule.rest.keyCertPath</td><td>Private key</td><td><p><em>default:</em></p><p><em>/app/joule/certs/key.pem</em></p></td></tr></tbody></table>
