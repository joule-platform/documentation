---
description: Joule REST APIs enables easy custom data processing and integration
icon: gear-complex-code
---

# API Endpoints

## Overview

This API Endpoints page offers an overview of Joule's core API functionalities, management capabilities and setup instructions. Joule offers two main API types, each tailored to specific development needs.

{% hint style="info" %}
This is v1.2.1 an incubating feature which is constantly being refined and extended until v1.3
{% endhint %}

### API reference

For detailed OpenAPI documentation, start Joule and access Swagger documentation at:&#x20;

```
http://<host>:9080/swagger
```

Joule offers two main API types:

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><p><mark style="color:orange;"><strong>Management API</strong></mark><br></p><p>Manages use cases and dependencies, providing endpoints for deployment, pipeline management, connector registration and contextual data sources</p></td><td></td><td></td><td><a href="mangement-api/">mangement-api</a></td></tr><tr><td><p><mark style="color:orange;"><strong>Data access API</strong></mark><br></p><p>Enables data interactions, allowing for querying, publishing and consuming data events</p></td><td></td><td></td><td><a href="data-access-api/">data-access-api</a></td></tr></tbody></table>
