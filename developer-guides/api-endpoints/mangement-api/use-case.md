---
icon: list
description: Deploy, pause, resume and undeploy use cases
---

# Use case

## Overview

The Use case API allows developers to deploy, test, validate and undeploy use case. Use case definitions provide the required binding of the stream and data transports.&#x20;

For further information on the structure of the content payload refer to this documentation.&#x20;

#### Example use case content

```json
{
  "use case": {
    "name": "basic_twindow_analytics",
    "sources": [
      "nasdaq_quotes_stream"
    ],
    "stream name": "basic_tumbling_window_pipeline",
    "sinks": [
      "kafka_analytics_view"
    ]
  }
} 
```

***

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/usecase/deploy" method="post" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/usecase/detail" method="get" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/usecase/list" method="get" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/usecase/undeploy" method="delete" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

***

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/usecase/pause" method="put" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/usecase/resume" method="post" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

