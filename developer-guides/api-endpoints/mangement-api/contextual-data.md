---
icon: head-side-brain
description: Register, list, detail and unregister contextual data sources
---

# Contextual data

## Overview

Contextual data is often used within stream processing to drive complex logic paths. Joule Management API provides the ability to register, list, detail and undeploy contextual data.

For further information on the structure of the content payload refer to this [documentation](../../../components/contextual-data/).&#x20;

## Example contextual data content

```json
{
  "contextual data": {
    "name": "banking market data",
    "data sources": [
      {
        "geode stores": {
          "name": "us markets",
          "connection": {
            "locator address": "192.168.86.39",
            "locator port": 41111
          },
          "stores": {
            "nasdaqIndexCompanies": {
              "region": "nasdaq-companies",
              "keyClass": "java.lang.String",
              "gii": true
            },
            "holidays": {
              "region": "us-holidays",
              "keyClass": "java.lang.Integer"
            }
          }
        }
      }
    ]
  }
}
```



{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/contextualdata/deploy" method="post" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/contextualdata/deploy/multi" method="post" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/contextualdata/detail" method="get" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/contextualdata/undeploy" method="delete" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

