---
description: >-
  Manages applications and integrations, providing endpoints for deployment,
  pipeline management, connector registration and contextual data sources
---

# Mangement API

## Overview

The Joule Management API offers a comprehensive set of REST endpoints for managing applications, integrations and data workflows within the Joule platform.

It allows developers to:

1. Deploy and control use cases
2. Manage data pipelines
3. Register connectors
4. Organise contextual data sources

By centralising application and integration management, the Joule Management API streamlines deployment, monitoring and maintenance for data-driven applications.

## API capabilities

### **Use case management**

The use case management endpoints enable full lifecycle control of applications deployed within Joule.

Developers can deploy new use cases, as well as pause, resume or undeploy existing ones as needed.

This feature is essential for **managing and monitoring** application workloads.

{% content-ref url="use-case.md" %}
[use-case.md](use-case.md)
{% endcontent-ref %}

### **Pipeline management**

Pipeline management endpoints allow for registration, listing, detailing and removal of stream processing pipelines.

These pipelines are fundamental for managing data flows through Joule, **enabling structured and efficient data processing** across use cases.

{% content-ref url="pipelines.md" %}
[pipelines.md](pipelines.md)
{% endcontent-ref %}

### **Connector management**

Connector management endpoints support the registration, listing, detailing and removal of various data connectors, including both sources and sinks.

This feature enables Joule to **interact with external data systems**, making it easier to bring in data for processing or export processed data to other systems.

{% content-ref url="data-connectors.md" %}
[data-connectors.md](data-connectors.md)
{% endcontent-ref %}

### **Context management**

The context management endpoints help register, list, detail and unregister contextual data sources, allowing for the organisation of data sources that are specific to a particular context or use case.

This ensures data is properly **segmented and accessible based** on its relevance to specific workflows.

{% content-ref url="contextual-data.md" %}
[contextual-data.md](contextual-data.md)
{% endcontent-ref %}
