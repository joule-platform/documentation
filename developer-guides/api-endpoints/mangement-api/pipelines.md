---
icon: person-from-portal
description: Register, list, detail and unregister stream processing pipelines
---

# Pipelines

## Overview

Streams are the backbone of all processing. Joule Management API provides the ability to register, list, detail and undeploy stream pipelines.&#x20;

For further information on the structure of the content payload refer to this [documentation](../../../components/pipelines/).&#x20;

## Example stream content

```json
{
  "stream": {
    "name": "basic_tumbling_window_pipeline",
    "enabled": true,
    "eventTimeType": "EVENT_TIME",
    "sources": [
      "nasdaq_quotes_stream"
    ],
    "processing unit": {
      "pipeline": [
        {
          "time window": {
            "emitting type": "tumblingQuoteAnalytics",
            "aggregate functions": {
              "FIRST": [
                "ask"
              ],
              "LAST": [
                "ask"
              ]
            },
            "policy": {
              "type": "tumblingTime",
              "window size": 1000
            }
          }
        }
      ]
    },
    "emit": {
      "select": "symbol, ask_FIRST, ask_LAST"
    },
    "group by": [
      "symbol"
    ]
  }
}
```

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/stream/register" method="post" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/stream/list" method="get" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/stream/detail" method="get" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/stream/unregister" method="delete" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}
