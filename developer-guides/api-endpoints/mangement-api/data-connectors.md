---
icon: pipe-collar
description: Register, list, detail and unregister data sources and sinks
---

# Data connectors

## Overview

Transports provide the integration links required to consume and publish computed stream results. Joule Management API provides the ability to register, list, detail and undeploy transports.

For further information on the structure of the content payload refer to this [documentation](../../../components/connectors/).&#x20;

## Example data connector content

```json
{
    "kafkaConsumer": {
        "name": "nasdaq_quotes_stream",
        "cluster address": "joule-gs-redpanda-0:9092",
        "consumerGroupId": "nasdaq",
        "topics": [
        "quotes"
        ],
        "deserializer": {
        "parser": "com.fractalworks.examples.banking.data.QuoteToStreamEventParser",
        "key deserializer": "org.apache.kafka.common.serialization.IntegerDeserializer",
        "value deserializer": "com.fractalworks.streams.transport.kafka.serializers.object.ObjectDeserializer"
        },
        "properties": {
        "partition.assignment.strategy": "org.apache.kafka.clients.consumer.StickyAssignor",
        "max.poll.records" : "7000",
        "fetch.max.bytes" : "10485760"
        }
    }
}
```

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/transports/register" method="post" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/transports/list" method="get" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/transports/detail" method="get" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/management/transports/unregister" method="delete" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}
