---
description: This is an incubating feature which is constantly being refined and extended
---

# SQL support

## Overview

Joule ships with an embedded in-memory modern SQL engine, [DuckDB](https://duckdb.org). This is used to capture events flowing through the processing pipeline along with supporting the metrics engine implementation.

### Key Features

* [Event tap](../../components/processors/event-tap/) for event capture and storage
* [Metrics engine](../../components/analytics/metrics-engine/) to provide SQL analytics
* Data access via the [Rest API](data-access-api/)
