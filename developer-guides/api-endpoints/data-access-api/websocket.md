---
icon: plug
description: Subscribe to events from a WebSocket
---

# WebSocket

## Overview

The Websocket API allows developers to subscribe to processed events from a Joule process. This is useful for validating processing streams.

***

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/ws/stream/{topicId}" method="get" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

