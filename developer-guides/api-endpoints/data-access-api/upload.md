---
icon: chevrons-up
description: Upload files and stream events in to a running Joule process
---

# Upload

***

## Overview

The Upload API allows developers to upload a file and/or a array of StreamEvents in to a running Joule process ready for stream processing.

***

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/consumer/file/{topicId}" method="post" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/consumer/stream/{topicId}" method="post" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}



