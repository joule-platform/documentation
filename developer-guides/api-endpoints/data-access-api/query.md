---
icon: arrow-progress
description: Fetch, export, delete and subscribe to data
---

# Query

***

## Overview

The Query API allows developers to list available data to query, fetch, export and delete data from the internal in-memory database.

***

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/query/list" method="get" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/query/{schema}/{table}" method="get" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/export/{schema}/{table}/{type}" method="get" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/swagger-docs (1).json" path="/joule/delete/{schema}/{table}" method="delete" %}
[swagger-docs (1).json](<../../../.gitbook/assets/swagger-docs (1).json>)
{% endswagger %}
