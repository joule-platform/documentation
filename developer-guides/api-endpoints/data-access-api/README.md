---
description: >-
  Enables data interactions, allowing for querying, publishing and consuming
  data events
---

# Data access API

## Overview

The **Joule Data API** provides a powerful set of REST endpoints for **accessing, publishing and consuming** data within the Joule platform.

Designed for high-performance data interactions, it allows developers to:

1. Query in-memory processed data
2. Export information
3. Manage real-time event streams via WebSockets

## API capabilities

### **Query**

Use the Query endpoints to **search and retrieve stored events** within Joule’s internal database.

This functionality enables you to extract historical data, perform analysis and export results for further processing.

It is essential for use cases where you need insights from past events or data archives.

{% content-ref url="query.md" %}
[query.md](query.md)
{% endcontent-ref %}

### **Upload**

The Upload endpoints allow you to **push data files or stream data directly** into Joule, integrating external data sources or updating current datasets.

This feature supports seamless data ingestion, making it easy to incorporate new data points for analysis and processing within Joule.

{% content-ref url="upload.md" %}
[upload.md](upload.md)
{% endcontent-ref %}

### **WebSocket**

The WebSocket endpoints enable **real-time data interaction by allowing clients to subscribe to events** as they occur.

With WebSockets, you can build applications that respond instantly to data changes, creating dynamic, event-driven solutions that leverage Joule’s real-time processing capabilities.

{% content-ref url="websocket.md" %}
[websocket.md](websocket.md)
{% endcontent-ref %}
