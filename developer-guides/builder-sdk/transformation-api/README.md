# Transformation SDK

## Development steps

### Interface

```java
public interface CustomPlugin {

    /**
     * Initialise sets up the user plugin
     *
     * @throws CustomUserPluginException
     */
    default void initialize() throws CustomUserPluginException {
    }

    /**
     * Validate plugin setting
     *
     * @throws InvalidSpecificationException
     */
    default void validate() throws InvalidSpecificationException {
    }

    /**
     * Set plugin properties
     *
     * @param properties plugin properties
     */
    @JsonProperty(value = "properties")
    default void setProperties(Properties properties){
    }
}
```
