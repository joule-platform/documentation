---
description: Joule SDK APIs enable connectors, processing, analytics and transformations
icon: uikit
---

# Builder SDK

{% hint style="warning" %}
To use this section, [set up the environment](environment-and-setup/environment-setup.md)\
All `gradle` commands must be execute at the root of the project directory
{% endhint %}

## Overview

Joule APIs enable developers to extend the platform with custom domain artefacts. The table below showcases all endpoints, a description and a specific use case example for each endpoint.

To get started to modify Joule to your needs, you will need to set up a development environment.

Follow [the steps on this page](environment-and-setup/) to get setup for success and start unlocking the full potential of Joule’s APIs and SDKs.

{% hint style="info" %}
This is v1.2.1 an incubating feature which is constantly being refined and extended until v1.3
{% endhint %}

<table><thead><tr><th width="170.33333333333331">API</th><th width="338">Description</th><th>Use case</th></tr></thead><tbody><tr><td><a href="connector-api/">Connector</a></td><td>Enables users to programmatically develop custom data connectors</td><td>Use when events need to be consumed or published from unsupported connectors.</td></tr><tr><td><a href="processor-api.md">Processor</a></td><td>Enables users to programmatically develop custom event processors which leverages Joules existing features</td><td>Ideal for advanced analytics where additional data processing is required.</td></tr><tr><td><a href="analytics-api/">Analytics</a></td><td>Provides APIs for defining metrics, complex SQL queries and advanced analytics</td><td>Supports advanced analytics, machine learning and dynamic logic processing.</td></tr><tr><td><a href="analytics-api/create-custom-metrics.md">Metrics</a></td><td>Embeds computed metrics within custom processors</td><td>Support advanced analytics, machine learning, dynamic logic processing etc</td></tr><tr><td><a href="analytics-api/sql-queries.md">SQL</a></td><td>Enables ANSI SQL querying within custom processors</td><td>Executes complex queries across metrics, contextual data and other datasets</td></tr><tr><td><a href="transformation-api/">Transformation</a></td><td>Facilitates data transformations for feature engineering and data quality functions</td><td>Developing feature engineering, data quality functions, etc</td></tr><tr><td><a href="file-processing.md">File processing</a></td><td>Accesses processed data directly</td><td>Primarily used for auditing and data review</td></tr></tbody></table>
