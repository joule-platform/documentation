---
icon: screwdriver-wrench
description: Setting up Joule environment for seamless data processing applications
---

# Setting up developer environment

## Overview

Setting up your development environment for Joule is key to harnessing its full capabilities in data processing and event-driven application development.

This section covers the essential components and tools required, as well as setup options that suit different project needs.

You can either build the environment from source or use pre-built Docker images to streamline setup.

Docker images, offer a ready-made setup with Joule’s core, connectors and processors. Making it ideal for quick deployment.

<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Environment setup</strong></mark></td><td>Detailed prerequisites for the environment</td><td></td><td><a href="environment-setup.md">environment-setup.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Build &#x26; deploy</strong></mark></td><td>Define components, build, test, package, deploy project</td><td></td><td><a href="build-and-deploy.md">build-and-deploy.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Install Joule</strong></mark></td><td>Joule Docker images, installation, custom builds and examples</td><td></td><td><a href="install-joule/">install-joule</a></td></tr></tbody></table>
