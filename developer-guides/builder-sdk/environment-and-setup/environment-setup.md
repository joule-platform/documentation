---
description: Detailed prerequisites for the environment
---

# Environment setup

## Overview

Joule is designed to operate with the latest available releases of dependencies and tools, ensuring it incorporates the most current technologies and industry-standard bug fixes.

To get started, ensure you have the necessary prerequisites, including **Java Development Kit (JDK),** **Git** and your preferred scripting language. Use **Gradle** only when you move to building new functionalities in Joule.

For advanced options, consider using **GraalVM** to enable multi-language support within Joule.

## Requirements break down

### JDK

{% hint style="info" %}
Joule works with version 23 of the OpenJDK. **Any other version will not work with Joule.**
{% endhint %}

Preferred implementation [GraalVM CE 23.0.1](https://www.graalvm.org/jdk23/docs/). Using GraalVM is optional but is strongly recommended to take advantage of user defined expressions and language support.

GraalVM offers enhanced performance for applications and enables multi-language support, which can be beneficial for using dynamic scripting languages within Joule.

```bash
openjdk version "23.0.1" 2024-10-15
OpenJDK Runtime Environment GraalVM CE 23.0.1+11.1 (build 23.0.1+11-jvmci-b01)
OpenJDK 64-Bit Server VM GraalVM CE 23.0.1+11.1 (build 23.0.1+11-jvmci-b01, mixed mode, sharing)
```

### Gradle

Install [Gradle 8.12 ](https://docs.gradle.org/current/userguide/installation.html#installation)

Gradle is required to manage Joule’s build lifecycle. It provides build automation for multi-language applications, enhancing the overall development process.

```bash
------------------------------------------------------------
Gradle 8.12
------------------------------------------------------------

Build time:    2024-12-20 15:46:53 UTC
Revision:      a3cacb207fec727859be9354c1937da2e59004c1

Kotlin:        2.0.21
Groovy:        3.0.22
Ant:           Apache Ant(TM) version 1.10.15 compiled on August 25 2024
Launcher JVM:  23.0.1 (GraalVM Community 23.0.1+11-jvmci-b01)
```

### Git

[Git is essential for cloning and managing](https://docs.gitlab.com/ee/topics/git/how_to_install_git/) Joule repositories. Ensure you have the latest version installed to avoid compatibility issues with repository operations.
