---
description: Define components, build, test, package, deploy project
---

# Build and deploy



{% hint style="success" %}
[Set up the environment](environment-setup.md)\
All gradle commands must be execute at the root of the project directory
{% endhint %}

## Build and deploy steps

1. Defined components in plugins.properties
2. Build, test and package
3. Deploy to project

## Explaining each step <a href="#explaining-each-step" id="explaining-each-step"></a>

### Step 1: Define components in plugins.properties

Create a plugins.properties file with a similar definition for your custom class or classes under the `resources/META-INF/services` directory. If you are packaging more than one component add the definition on a new line.

```properties
com.yourdomain.processors.transformers.CustomProcessor
```

### Step 2: Package, test and package

A single project can have a number of custom components which can be packaged into a single or multiple jar files. The template project provides basic `grade.build` file. The project will execute these tests during the gradle build cycle and deploy to your local maven repository.&#x20;

#### Example gradle file

```groovy
plugins {
    id "java"
    id 'maven-publish'
}

jar.doFirst {
    archiveBaseName = 'mydomain-customtransformer-processor'
    manifest {
        attributes "Class-Path" : configurations.compileClasspath.collect { it.getName() }.join(' ')
    }
}

dependencies {
    implementation "com.fractalworks.streams.platform:fractalworks-stream-platform:$platformVersion"
}
```



Execute the below command at the project root. This will build, test, package and publish the resulting artifacts to your local maven repository.

```
gradle build publishToMavenLocal
```

### Step 3: Deploy to project <a href="#step-1-create-the-destination-using-the-template" id="step-1-create-the-destination-using-the-template"></a>

Once your package has been successfully created you are ready to deploy to a Joule project. The resulting jar artefacts' needs to be placed in to the `userlibs` directory in your Joule projects directory. See provided examples [documentation](../../../use-case-examples/overview.md) for further directions.

```bash
cp build/libs/<your-component>.jar <location>/userlibs
```
