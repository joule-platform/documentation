---
description: Joule Docker images, installation, custom builds and examples
---

# Install Joule

## Overview

This page explains how to install and use Joule via prebuilt Docker images or by building from source.

In addition, find how to install and run examples.

<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td></td><td><mark style="color:orange;"><strong>Install with Docker</strong></mark></td><td></td><td><a href="install-with-docker.md">install-with-docker.md</a></td></tr><tr><td></td><td><mark style="color:orange;"><strong>Install from source</strong></mark></td><td></td><td><a href="install-from-source.md">install-from-source.md</a></td></tr><tr><td></td><td><mark style="color:orange;"><strong>Install Joule examples</strong></mark></td><td></td><td><a href="install-joule-examples.md">install-joule-examples.md</a></td></tr></tbody></table>
