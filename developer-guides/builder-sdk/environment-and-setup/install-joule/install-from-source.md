---
description: Install Joule from source, connectors, and processors locally
---

# Install from source

## Overview

Joule and most of its plugins are open-source software.

This means the codebases are freely available for you to download and build from source. You might choose to install from source if you want the latest code or to install Joule from a specific commit.

This option can be particularly useful when you are contributing changes or debugging a past change.

Downloading and building Joule enables you to contribute to the project, whether by fixing bugs or implementing requested features.

## Installing Joule from GitLab

To install Joule from the GitLab source repository, follow these steps:

1. Clone the Joule Repository:

```bash
git clone https://gitlab.com/joule-platform/streamsplatform.git
cd fractalworks-stream-platform
```

2. Build and Install Joule: Run the following Gradle commands to build and install Joule into your local Maven directory:

```bash
gradle clean build publishToMavenLocal
```

This will install the latest joule jars and dependencies in to the local maven directory.

## Installing Joule connectors

Before installing Joule connectors, ensure that you have already built Joule core. Then follow these steps:

1. Clone the Connector Repository:

```bash
git clone https://gitlab.com/joule-platform/fractalworks-stream-transports.git
cd fractalworks-stream-transports
```

2. Build and Install Connectors: Run the following Gradle commands:

```bash
gradle clean build publishToMavenLocal
```

This will install the latest joule transport connectors and dependencies in to the local maven directory.

## Installing Joule Processors

Similar to connectors, you will need to have built Joule core before installing the processors. Follow these steps:

1.  Clone the Processor Repository:

    ```bash
    git clone https://gitlab.com/joule-platform/fractalworks-stream-processors.git
    cd fractalworks-stream-processors
    ```
2.  Build and Install Processors: Run the following Gradle commands:

    ```bash
    gradle clean build publishToMavenLocal
    ```

    This will install the latest Joule processors and dependencies into your local Maven directory.

By following these steps, you can set up Joule and its components from source, making it easy to customise the platform and contribute to its development.
