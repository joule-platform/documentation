---
description: Clone Joule examples from GitLab, build, explore updates.
---

# Install Joule examples

## Overview

Joule offers a set of examples to demonstrate how to use various platform components.

These examples can be cloned from the GitLab repository and built locally to explore different functionalities.

The repository is frequently updated, so it's recommended to check back regularly for new content and enhancements.

```bash
git clone https://gitlab.com/joule-platform/fractalworks-joule-examples.git
```

```bash
cd fractalworks-joule-examples
```

```bash
gradle clean build
```
