---
description: Customise and extend Joule analytics capability using the Windows API
---

# Windows API

## Pre-processor API

Before a window is process the user is able to inject a preprocessing function. The function returns a new window which is subsequently processed by defined window listeners and aggregate functions.

```java
import com.fractalworks.streams.core.data.streams.OrderedStreamEventWindow;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.data.streams.StreamTimeType;
import com.fractalworks.streams.core.data.streams.Window;
import com.fractalworks.streams.sdk.analytics.WindowPreprocessor;

/**
* Custom preprocessor 
*/
public class MyCustomPreprocessor extends WindowPreprocessor {

    public MyCustomPreprocessor(StreamTimeType streamTimeType, int count, int groupByKey) {
        super(streamTimeType, count, groupByKey);
    }

    public Window compute(Window window) {
        StreamEvent[] orderedEvents = // Do something interesting
        return new OrderedStreamEventWindow(orderedEvents, groupbyKey, streamTimeType);
    }
}

```

## WindowListener API

Custom window listeners can be developed and deployed in to the Joule environment. See [building and deploying custom components](../environment-and-setup/build-and-deploy.md) documentation for further information.

### API

```java
package com.fractalworks.streams.core.listeners;

import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Window;

import java.util.Map;

public interface WindowListener {
    Tuple<String, Map<String, Object>> apply(Window window, Context context);
}
```
