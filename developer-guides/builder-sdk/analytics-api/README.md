---
description: >-
  Analytics form the core of the platform to drive insight to value. Joule
  provides various methods to define and leverage analytics within a streaming
  context
---

# Analytics API

## Supported functions

Joule provides a set of APIs to enable stream based analytics

<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td></td><td><a href="create-custom-metrics.md"><mark style="color:orange;"><strong>Metrics</strong></mark></a></td><td></td><td></td></tr><tr><td></td><td><a href="define-analytics.md"><mark style="color:orange;"><strong>Analytics</strong></mark></a></td><td></td><td></td></tr><tr><td></td><td><a href="sql-queries.md"><mark style="color:orange;"><strong>SQL</strong></mark></a></td><td></td><td></td></tr><tr><td></td><td><mark style="color:orange;"><strong>Windows API</strong></mark></td><td></td><td><a href="windows-api.md">windows-api.md</a></td></tr></tbody></table>
