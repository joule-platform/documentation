---
description: >-
  Custom parser converts a domain Quote object into an internal Joule
  StreamEvent
---

# StreamEventParser API

## Objective

A `StreamEventParser`  converts domain-specific objects, such as a `Quote`, into the required Joule `StreamEvent` object ready for processing.

### Example

The example demonstrates a custom implementation of the `StreamEventParser` interface, specifically designed to convert a `Quote` object into a `StreamEvent`.

```java
@JsonRootName(value = "quote parser")
public class QuoteToStreamEventParser implements StreamEventParser {

    public QuoteToStreamEventParser() {
        // Required
    }

    @Override
    public Collection<StreamEvent> translate(Object o) throws TranslationException {
        Collection<StreamEvent> events = null;
        if(o instanceof Quote){
            Quote quote = (Quote) o;
            StreamEvent event = new StreamEvent("quote");
            event.setEventTime(quote.time());
            event.addValue("symbol", quote.symbol());
            event.addValue("bid", quote.bid());
            event.addValue("ask", quote.ask());
            event.addValue("volatility", quote.volatility());
            event.addValue("volume", quote.volume());
            event.addValue("date", quote.date());

            events = Collections.singletonList( event);
        }
        return events;
    }
}
```

## Class: StreamEventParser

All implementations must provide a concrete implement of the `translate` method.

```java
/**
* Parse passed byte array to collection of {@link StreamEvent}s
*
* @param payload byte array to translate
* @return collection of stream events
* @throws TranslationException is thrown when a translation failure occurs
*/
Collection<StreamEvent> translate(Object payload) throws TranslationException;

```

### Parser Annotation

The class annotation defines the parser DSL element, indicating the parser's role in converting `Quote` objects. It aids in custom serialisation settings. This is required.

### Constructor

The constructor is required but does not contain any specific logic for this example.

### Translate method

The core method that converts the input object into a collection of `StreamEvent` objects.

This example the method checks if the input object is an instance of `Quote`. If not, it returns `null`, indicating non-valid conversion.&#x20;

Otherwise, the quote attributes are mapped in to a single  `StreamEvent` which is then placed in to a `Collections.singletonList()` and returned as a collection, as required by the method's signature.

```java
@Override
public Collection<StreamEvent> translate(Object o) throws TranslationException {
    Collection<StreamEvent> events = null;
    if(o instanceof Quote){
        ...
    }
}
```
