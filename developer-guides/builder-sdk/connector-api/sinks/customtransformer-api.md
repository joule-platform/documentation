---
description: Custom parser converts an internal Joule StreamEvent into a domain object
---

# CustomTransformer API

## Objective

A  CustomTransformer implementation converts a StreamEvent to a custom domain type ready for consumer processing.

### Example

This example maps StreamEvent attributes to a Quote object attributes.

```java
public class QuoteTransformer implements CustomTransformer<Quote> {

    public QuoteTransformer() {
        // Required
    }

    @Override
    public Collection<Quote> transform(Collection<StreamEvent> payload) throws TranslationException {
        Collection<Quote> quotes = new ArrayList<>();
        if( payload!= null) {
            for(StreamEvent e : payload){
                quotes.add(transform(e));
            }
        }
        return quotes;
    }

    @Override
    public Quote transform(StreamEvent payload) throws TranslationException {
        return new Quote(
                (String)payload.getValue("symbol"),
                (double)payload.getValue("mid"),
                (double)payload.getValue("bid"),
                (double)payload.getValue("ask"),
                (long)payload.getValue("volume"),
                (double)payload.getValue("volatility"),
                (long)payload.getEventTime(),
                (Date)payload.getValue("date")
                );
    }
}
```



## Class: CustomTransformer

All implementations must provide a concrete implements of the `transform` methods.

```java
/**
 * Transform passed byte array to collection of StreamEvents
 *
 * @param payload collection of events to transform to target types
 * @return collection of T
 * @throws TranslationException is thrown when a translation failure occurs
 */
Collection<T> transform(Collection<StreamEvent> payload) throws TranslationException;

/**
 * Transform a single StreamEvent in to a specific type
 *
 * @param payload an event
 * @return
 * @throws TranslationException
 */
T transform(StreamEvent payload) throws TranslationException;
```

### Constructor

The constructor is required but does not contain any specific logic for this example.

### Transform single event method

This  core method that converts the `StreamEvent` object to a single domain specific object. This is performed by a simple attribute mapping from the StreamEvent to Quote constructor parameter requirements.

```java
@Override
public Quote transform(StreamEvent payload) throws TranslationException {
return new Quote(
        // Mapping
        ... 
        );
}
```

### Transform batch events method

This method is called when in micro-batching mode. It takes a collection of StreamEvents and converts to a collection of domain specific events using the defined single event transform method.

```java
@Override
public Collection<Quote> transform(Collection<StreamEvent> payload) throws TranslationException {
    Collection<Quote> quotes = new ArrayList<>();
    if( payload!= null) {
        for(StreamEvent e : payload){
            quotes.add(transform(e));
        }
    }
    return quotes;
}
```
