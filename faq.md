---
description: Collation of frequently asked questions
---

# FAQ

<mark style="color:green;">**Who develops Joule?**</mark>&#x20;

> Joule is maintained by [Lyndon Adams](https://www.linkedin.com/in/lyndonadams/) who has a extensive background in event based stream processing using distributed architectures. There is an open welcome invitation for collaborators to get involved with the project.

<mark style="color:green;">**Why call it Joule?**</mark>&#x20;

> A Joule is a unit of work or energy which is reflective on how the platform was envisioned and developed. A Joule process can either execute _part of_ or _complete_ use case depending upon complexity thus addressing multiple non-functional requirements while bringing simplicity to the solution

<mark style="color:green;">**Why use Joule?**</mark>

> Low-code use case driven process, standard data connector integrations, OOTB processors, analytics first approach plus more.&#x20;
>
> Spend one hour using the platform we love to gain feedback on whats good and what needs to be improved.

<mark style="color:green;">**Why would i use Joule verses other solutions such as Flink?**</mark>

> Joule has been designed to develop and pilot business use cases quickly using a low-code problem solving approach. After proving the use case there is no reason to switch to another platform but we would prefer you stay with Joule.&#x20;

<mark style="color:green;">**Do you support machine learning?**</mark>

> In short yes. Online ML model support is provided using the [openscoring](https://openscoring.io/) JPMML library. Supported models include Random Forest, XGBoost,  K-Means, NN, regression, Bayesian etc&#x20;

<mark style="color:green;">**Do you have a SDK?**</mark>

> Joule ships with a SDK to enable developers to build advanced analytics, data connectors and sinks, and custom stream processors.

<mark style="color:green;">**Can I run Joule on bare metal?**</mark>

> Yes, yes and hell yes. Download one of the example use case project or use the project template to get started.

<mark style="color:green;">**Is Joule a ETL tool?**</mark>

> Not really. Joule wasn't designed to be a ETL tool, although it does exhibit some of the feature characteristics.&#x20;

<mark style="color:green;">**Can Joule do reverse ETL?**</mark>

> Yes. Joule can present domain data structures using Avro schema when using the Kafka publishing connector.
