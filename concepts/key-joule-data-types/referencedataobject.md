---
description: ReferenceDataObject stores, queries and manages contextual reference data
---

# Contextual data

## Overview

[Contextual data](../../components/contextual-data/) provides additional context to support advanced stream processing use cases.

Joule offers a base implementation for storing contextual data elements, enabling the **development of custom components** that are both **efficient and flexible** in processing external data within the platform.

The `ReferenceDataObject` extends the `HashMap<String, Object>`, providing the **full benefits of a map structure** while adding extensions specifically designed for **storing and querying** contextual data.

This data structure allows for **seamless integration and management of contextual information** across Joule's platform.

## Key features

1. <mark style="color:green;">**Extends HashMap**</mark>\
   Inherits all features of `HashMap<String, Object>`, offering a flexible key-value data structure.
2. <mark style="color:green;">**Contectual data storage**</mark>\
   Designed for storing and querying contextual reference data efficiently.
3. <mark style="color:green;">**Custom data representation**</mark>\
   Supports storing and retrieving custom data, providing flexibility for various use cases.

## Build with the SDK

{% content-ref url="../../what-is-joule/data-types/referencedataobject.md" %}
[referencedataobject.md](../../what-is-joule/data-types/referencedataobject.md)
{% endcontent-ref %}
