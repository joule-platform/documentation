---
icon: hexagon-image
description: Key data types available within the Joule platform
---

# Key Joule data types

## What will we learn on this article?

In Joule, the foundation of data processing is built around various data types that facilitate the efficient management, transport and manipulation of external data within the platform.

These data types include the `StreamEvent`, `ReferenceDataObject` and `GeoNode`. Each of which serves a specific purpose to enhance flexibility, data integrity and processing efficiency.

### Key concepts

1. <mark style="color:green;">**StreamEvent**</mark>\
   The core data structure for **transporting and processing** external data in Joule. It supports operations like adding, updating, removing, and cloning fields, along with timestamping, change tracking and serialisation for flexible event-driven processing.
2. <mark style="color:green;">**ReferenceDataObject**</mark>\
   A flexible structure built on `HashMap<String, Object>` for **storing and querying** contextual data, useful for managing key-value pairs across components.
3. <mark style="color:green;">**GeoNode**</mark>\
   Designed for geospatial analytics, GeoNode stores location-based data and enables efficient querying of nearby entities using a spatial index.

These data types form the foundation for building high-performance, custom components within Joule. They **enable efficient data handling**, from generic attributes to geospatial information, while maintaining data integrity and supporting complex processing models.

## Available data types

<table data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>StreamEvent</strong></mark></td><td>StreamEvent enables flexible, efficient event-driven data processing in Joule</td><td></td><td><a href="streamevent-object.md">streamevent-object.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Contextual Data</strong></mark></td><td>ReferenceDataObject stores, queries, and manages contextual reference data</td><td></td><td><a href="referencedataobject.md">referencedataobject.md</a></td></tr><tr><td><mark style="color:orange;"><strong>GeoNode</strong></mark></td><td>Geospatial data structure for location-based spatial entity analysis</td><td></td><td><a href="geonode.md">geonode.md</a></td></tr></tbody></table>
