---
description: StreamEvent enables flexible, efficient event-driven data processing in Joule
---

# StreamEvent object

## Overview

The core of Joule's processing system revolves around the `StreamEvent`, a data structure that **facilitates the transport and manipulation of external data** within the platform.

By converting external data into `StreamEvent`, Joule **enables flexible processing models and efficient custom component development**.

The `StreamEvent` encapsulates data attributes, tracks changes and supports multiple operations like:

1. adding
2. updating
3. removing
4. cloning fields

This ensures **efficient data handling** and enables users to customise data flows while maintaining data integrity and transaction isolation.

### Key features

1. <mark style="color:green;">**Source system typing**</mark>\
   Attributes are typed according to their source system, ensuring clear identification and structured data representation.
2. <mark style="color:green;">**Timestamping**</mark>\
   Each event is timestamped either at its source or when ingested into the platform, providing precise event timing.
3. <mark style="color:green;">**Change tracking**</mark>\
   Changes to the event are tracked by time, value, and the processor that affected them, ensuring a transparent history of modifications.
4. <mark style="color:green;">**Cloneable**</mark>\
   Supports deep cloning to preserve transaction isolation, enabling parallel processing without data conflicts.
5. <mark style="color:green;">**Serialisable**</mark>\
   `StreamEvent` is serialisable, making it suitable for external consumption and integration with other systems.

## Build with the SDK

{% content-ref url="../../what-is-joule/data-types/streamevent.md" %}
[streamevent.md](../../what-is-joule/data-types/streamevent.md)
{% endcontent-ref %}
