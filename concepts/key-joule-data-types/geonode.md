---
description: Geospatial data structure for location-based spatial entity analysis
---

# GeoNode

## Overview

**GeoNode** provides a data structure designed for geospatial analytics, enabling users to **manage and query spatial entities** based on their geographic locations.

This structure allows you to store and search geospatial data through the use of a spatial index, helping to identify nearby entities within a specific area.

By using GeoNode, custom spatial analytics can be easily developed, **extending the base implementation to handle location-based data** efficiently.

{% hint style="info" %}
The GeoNode class provides a generic data structure for spatial entities to be store and searched through the use of a spatial index
{% endhint %}

## Key features

1. <mark style="color:green;">**Latitude and longitude support**</mark>\
   Stores spatial data using geographic coordinates.
2. <mark style="color:green;">**Container for spatial entities**</mark>\
   Designed to hold and query spatial entities.
3. <mark style="color:green;">**Customisable for entity types**</mark>\
   Can be extended for use with different types of reference data.

## Build with the SDK

{% content-ref url="../../what-is-joule/data-types/geonode.md" %}
[geonode.md](../../what-is-joule/data-types/geonode.md)
{% endcontent-ref %}
