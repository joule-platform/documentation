---
icon: arrows-to-circle
description: Collection of concepts for the effective utilisation of Joule
---

# Concepts

## Overview

From the outset Joule has been designed to be loosely coupled system, low weight , with a non-opinionated approach to integrations. This enables the best of breed solutions to be leverage along with new innovative solutions.&#x20;

## Core

Learn what key design choices were made to make Joule unique

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Core concepts</strong></mark></td><td></td><td></td><td><a href="../what-is-joule/core-concepts.md">core-concepts.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Low code development</strong></mark></td><td></td><td></td><td><a href="low-code-development.md">low-code-development.md</a></td></tr></tbody></table>

## Processing model

A simplified processing model that focuses more on the "what" not the "how".

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Unified execution engine</strong></mark></td><td></td><td></td><td><a href="../what-is-joule/unified-execution-engine.md">unified-execution-engine.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Batch and stream processing</strong></mark></td><td></td><td></td><td><a href="../what-is-joule/batch-processing-explainer.md">batch-processing-explainer.md</a></td></tr></tbody></table>

## Analytics

At Joule's core analytics provides the use case enabler using standard aggregates, continuous metrics to ML inferencing.&#x20;

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Continuous metrics</strong></mark></td><td></td><td></td><td><a href="../what-is-joule/continuous-metrics-applications.md">continuous-metrics-applications.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Flexible analytics</strong></mark></td><td></td><td></td><td></td></tr></tbody></table>

## Data

Standard data type applied across the platform to achieve a simplified method of interfacing and management&#x20;

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Stream event</strong></mark></td><td></td><td></td><td><a href="key-joule-data-types/streamevent-object.md">streamevent-object.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Contextual data</strong></mark></td><td></td><td></td><td><a href="key-joule-data-types/referencedataobject.md">referencedataobject.md</a></td></tr><tr><td><mark style="color:orange;"><strong>Geospatial</strong></mark></td><td></td><td></td><td><a href="key-joule-data-types/geonode.md">geonode.md</a></td></tr></tbody></table>







