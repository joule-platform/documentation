---
description: Everything you need to know about Joule.
hidden: true
icon: file
---

# Documentation guide

## How this documentation is structure

Although the Joule documentation might seem overwhelming at first, we've organised it into two sections to help you get up to speed more quickly. Here's a guide to help you navigate through it:

### LEARN JOULE

This group is designed to help you understand everything related to Joule and guide you in mastering its concepts and features.

* [Tutorials](joules-tutorials/) serve as your foundation for learning how to work with Joule, guiding you through the basics. No prior knowledge of Joule is assumed, and we will support you every step of the way.
* [Concepts](https://app.gitbook.com/o/GXzsLF1Yj5IM5nJZKy16/s/UU6FZlV07ZD90OzbzGww/\~/changes/762/learn-joule/joules-concepts) will help you see the bigger picture by connecting the various elements together, enhancing your understanding of your use cases.

### APPLY JOULE

This group is tailored to help you solve your real world challenges and understand the technicalities of Joule much deeper.

* [How-to’s](https://app.gitbook.com/o/GXzsLF1Yj5IM5nJZKy16/s/UU6FZlV07ZD90OzbzGww/\~/changes/762/apply-joule/joules-how-tos) function as cheat sheets that assist in troubleshooting and resolving issues. These resources are geared towards those who already possess some knowledge of Joule, as they cover more advanced topics.
* [Recipes](https://app.gitbook.com/o/GXzsLF1Yj5IM5nJZKy16/s/UU6FZlV07ZD90OzbzGww/\~/changes/762/apply-joule/joules-recipes) consist of technical guides that clarify how the APIs, SDK, and other platform features operate. Familiarity with Joule is expected to get the most out of these materials.

## Get help

Finding challenges with Joule? We want to help!

* Try the [FAQ](faq.md)
* Found a bug? Reach out to&#x20;

## Want to contribute?

...
