---
icon: door-open
description: >-
  The low-code multi-purpose platform for stream analytics builders designed to
  accelerate ideation to business impact
cover: .gitbook/assets/logo_dark_doc_joule.png
coverY: 0
layout:
  cover:
    visible: true
    size: full
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: false
---

# Welcome to Joule's Docs

Joule is a platform that enables high throughput data and analytics at scale.

<table data-card-size="large" data-column-title-hidden data-view="cards"><thead><tr><th></th><th data-hidden></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><p><mark style="color:orange;"><strong>New to Joule?</strong></mark></p><p>Build your first Joule</p></td><td></td><td></td><td><a href="joules-tutorials/getting-started.md">getting-started.md</a></td></tr><tr><td><p><mark style="color:orange;"><strong>Already have a Joule running?</strong></mark></p><p>Explore the data pipeline</p></td><td></td><td></td><td><a href="components/pipelines/">pipelines</a></td></tr></tbody></table>

## Discover Joule

Joule is a low-code platform designed to handle a wide range of use cases, optimised for modern streaming analytics in hybrid environments and Internet of Things (IoT). It offers a modular architecture, ensuring consistent data processing through flexible pipelines tailored to your use cases.

<table data-card-size="large" data-column-title-hidden data-view="cards" data-full-width="false"><thead><tr><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><strong>Understand </strong><mark style="color:orange;"><strong>Joule's benefits</strong></mark></td><td><a href="why-joule/">why-joule</a></td></tr><tr><td><strong>Get a high level view on </strong><mark style="color:orange;"><strong>What is Joule</strong></mark></td><td><a href="what-is-joule/">what-is-joule</a></td></tr><tr><td><strong>Learn about </strong><mark style="color:orange;"><strong>Joule’s features</strong></mark></td><td><a href="what-is-joule/key-features.md">key-features.md</a></td></tr></tbody></table>

{% hint style="info" %}
Stable current Joule release v1.2.0 [release notes](product-updates/release-notes/v1.2.0-join-streams-with-stateful-analytics.md), July-2024&#x20;
{% endhint %}
