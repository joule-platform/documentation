# Glossary

#### <mark style="color:green;">Alert</mark>

Ability to send downstream systems an alert that is based upon a logical having expression.

#### <mark style="color:green;">Analytics</mark>

Generate stream based [analytics](components/analytics/) using a suite of features such as predictive scores, time based windows, aggregates, user define functions, scripts and expressions.

#### <mark style="color:green;">API Endpoint</mark>

Query Joule runtime internal database using API [rest endpoints](components/connectors/sources/rest-endpoints.md) with pre-defined and registered  SQL queries.

#### <mark style="color:green;">Connector</mark>

A Joule connector provides the ability to subscribe and publish events using a stream based processing paradigm. See [connectors](components/connectors/) section for more information.

#### <mark style="color:green;">Data Sink</mark>

Data sink provide the integration fabric to downstream event consumers using custom data formats such as AVRO, parquet, JSON and CSV.

#### <mark style="color:green;">Data Source</mark>

Data sources enable Joule to consume real-time streaming events and files using pre-defined data structures.

#### <mark style="color:green;">Emit</mark>

Events are [emitted](components/pipelines/emit-computed-events.md) from the Joule process to connected consumers using a select based projection.

#### <mark style="color:green;">Low Code</mark>

Joule defines stream processing use cases using a [low-code](concepts/low-code-development.md) iterative development approach coupled with a SDK that enables developers to extend platform capabilities.

#### <mark style="color:green;">Microbatch</mark>

To improve throughput performance Joule applies small batches of events within a discrete processing cycle. This improves JVM memory management while optimising processing throughput.&#x20;

#### <mark style="color:green;">OOTB</mark>

Out Of The Box features provided by the Joule platform.

#### <mark style="color:green;">Pipeline</mark>

Pipelines consist of one or more processors which invoked in the sequence of which they have been defined.

#### <mark style="color:green;">Processors</mark>

Processors perform a single function of work on a single event. Functions such as analytics, filters, enrichment and transformation are examples of [processors](components/processors/).

#### <mark style="color:green;">SDK</mark>

To extend the platform capabilities beyond the OOTB analytics, data connectors and processors a [Software Development Kit](developer-guides/builder-sdk/) is provided.

#### <mark style="color:green;">StreamEvent</mark>

The key data structure within Joule is the [StreamEvent](concepts/key-joule-data-types/streamevent-object.md). This abstract data type provides the ability to provide a ubiquitous computation and development model for all platform components.&#x20;

#### <mark style="color:green;">Use case</mark>

Either part of or complete function of tasks that form an automatable digital business function.
